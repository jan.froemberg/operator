// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package novacompute

import (
	"strings"
	"yaook.cloud/nova_template"
)

novacompute_conf_spec: nova_template.nova_template_conf_spec
novacompute_conf_spec: {
	cinder: {
		auth_url:     string
		"auth-url":   auth_url
		cafile:       keystone_authtoken.cafile
		catalog_info: "volumev3:cinderv3:internalURL"
	}
	DEFAULT: {
		#transport_url_hosts:    *["rabbitmq-nova"] | [...string]
		#transport_url_port:     *5671 | int
		#transport_url_username: *"admin" | string
		#transport_url_password: string
		#transport_url_vhost:    *"" | string
		#transport_url_parts: [ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}]
		transport_url:  "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
		use_stderr:     *true | bool
		use_json:       *true | bool
		compute_driver: "libvirt.LibvirtDriver"
		state_path:     "/var/lib/nova"
	}
	keystone_authtoken: {
		os_region_name:      *"RegionOne" | string
		auth_url:            *"https://keystone:5000/v3" | string
		"auth-url":          auth_url
		cafile:              "/etc/ssl/certs/ca-bundle.crt"
		auth_type:           *"password" | string
		project_domain_name: *"default" | string
		user_domain_name:    *"default" | string
		project_name:        *"service" | string
		username:            string
		password:            string
	}
	neutron: {
		auth_url:   string
		"auth-url": auth_url
		cafile:     keystone_authtoken.cafile
	}
	placement: {
		auth_url:   string
		"auth-url": auth_url
		cafile:     keystone_authtoken.cafile
	}
	service_user: {
		send_service_user_token: *true | bool
		keystone_authtoken
	}
	libvirt: {
		live_migration_uri:    "qemu+ssh://nova@%s/system"
		live_migration_scheme: "ssh"
		swtpm_enabled:         *false | bool
		if libvirt.swtpm_enabled == true {
			swtpm_user:  *"nova" | string
			swtpm_group: *"libvirt" | string
		}
	}
	nova_sys_admin: {
		helper_command: "sudo /usr/local/bin/nova-rootwrap /usr/local/etc/nova/rootwrap.conf privsep-helper --config-file /etc/nova/nova.conf"
	}
	privsep_osbrick: {
		helper_command: nova_sys_admin.helper_command
	}
	upgrade_levels: {
		compute: "auto"
	}
	os_vif_ovs: {
		ovsdb_connection: "unix:/run/openvswitch/db.sock"
	}
	vif_plug_ovs_privileged: {
		helper_command: nova_sys_admin.helper_command
	}
	vnc: {
		enabled:       true
		server_listen: "0.0.0.0"
	}
	oslo_messaging_rabbit: {
		ssl:                 true
		ssl_ca_file:         "/etc/ssl/certs/ca-bundle.crt"
		amqp_durable_queues: true
		rabbit_quorum_queue: true
	}
	barbican: {
		barbican_endpoint_type: *"internal" | string
	}
}
