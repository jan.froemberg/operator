// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ceilometer

import (
	"strings"
	"yaook.cloud/ceilometer_template"
)

ceilometer_conf_spec: ceilometer_template.ceilometer_template_conf_spec
ceilometer_conf_spec: {
	service_credentials: {
		auth_url:            *"https://keystone:5000/v3" | string
		cafile:              "/etc/ssl/certs/ca-bundle.crt"
		interface:           "internal"
		auth_type:           *"password" | string
		project_domain_name: *"default" | string
		user_domain_name:    *"default" | string
		project_name:        *"service" | string
		username:            *"ceilometer" | string
		password:            *"" | string
		"auth-url":          auth_url
	}
	DEFAULT: {
		#transport_url_hosts:    *["rabbitmq-ceilometer"] | [...string]
		#transport_url_port:     *5671 | int
		#transport_url_username: *"admin" | string
		#transport_url_password: string
		#transport_url_vhost:    *"" | string
		#transport_url_parts: [ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}]
		transport_url: "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
		use_stderr:    *true | bool
		use_json:      *true | bool
	}
	notification: {
		messaging_urls: [...string]
		messaging_urls: [DEFAULT.transport_url, ...]
	}
	oslo_messaging_rabbit: {
		ssl:                 true
		ssl_ca_file:         "/etc/ssl/certs/ca-bundle.crt"
		amqp_durable_queues: true
		// as we may want to run ceilometer in higher versions than the other
		// services, we need to be able to disable quorum, so we can also run e.g.
		// ceilometer yoga with nova queens
		rabbit_quorum_queue: *true | bool
	}
	polling: {
		cfg_file: "/usr/local/etc/ceilometer/polling.yaml"
	}
}
