// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gnocchi

gnocchi_conf_spec: {
	keystone_authtoken: {
		auth_url:            *"https://keystone:5000/v3" | string
		cafile:              "/etc/pki/tls/certs/ca-bundle.crt"
		interface:           *"internal" | string
		auth_type:           *"password" | string
		project_domain_name: *"default" | string
		user_domain_name:    *"default" | string
		project_name:        *"service" | string
		username:            *"gnocchi" | string
		password:            string
		"auth-url":          auth_url
	}
	indexer: {
		#connection_host:        *"mysql-gnocchi-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"gnocchi" | string
		#connection_password:    string
		#connection_database:    *"gnocchi" | string
		#connection_ssl_ca_path: *"/etc/pki/tls/certs/ca-bundle.crt" | string
		url:                     *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string
	}
	api: {
		auth_mode: *"keystone" | string
		host:      *"localhost" | string
		port:      *8080 | int
	}
	oslo_policy: {
		policy_file: "/etc/gnocchi/policy.json"
	}
}

// Template to be included in "storage" if we want to use ceph
gnocchi_backend_ceph: {
	driver:        *"ceph" | string
	ceph_pool:     *"gnocchi-pool" | string
	ceph_username: *"gnocchi" | string
}

// Template to be included in "storage" if we want to use s3
gnocchi_backend_s3: {
	driver:               *"s3" | string
	s3_endpoint_url:      string
	s3_access_key_id:     string
	s3_secret_access_key: string
	s3_bucket_prefix:     string
}
