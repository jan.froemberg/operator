// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mariadb

import (
	"yaook.cloud/mariadb_template"
)

mariadb_conf_spec: mariadb_template.mariadb_template_conf_spec
mariadb_conf_spec: {
	#ssl_ca_filename:      *"ca.crt" | string
	#ssl_key_filename:     *"tls.key" | string
	#ssl_cert_filename:    *"tls.crt" | string
	#ssl_dir:              *"/bitnami/mariadb/certs/frontend" | string
	#replication_ssl_dir:  *"/bitnami/mariadb/certs/replication" | string
	#ssl_ca:               *"\(#ssl_dir)/\(#ssl_ca_filename)" | string
	#ssl_cert:             *"\(#ssl_dir)/\(#ssl_cert_filename)" | string
	#ssl_key:              *"\(#ssl_dir)/\(#ssl_key_filename)" | string
	#replication_ssl_ca:   *"\(#replication_ssl_dir)/\(#ssl_ca_filename)" | string
	#replication_ssl_cert: *"\(#replication_ssl_dir)/\(#ssl_cert_filename)" | string
	#replication_ssl_key:  *"\(#replication_ssl_dir)/\(#ssl_key_filename)" | string

	"client-server": {
		"port":       3306
		"socket":     "/opt/bitnami/mariadb/tmp/mysql.sock"
		"plugin_dir": "/opt/bitnami/mariadb/plugin"
	}
	"mysqld": {
		"default_storage_engine": "InnoDB"
		"basedir":                "/opt/bitnami/mariadb"
		"datadir":                "/bitnami/mariadb/data"
		"tmpdir":                 "/opt/bitnami/mariadb/tmp"
		"pid_file":               "/opt/bitnami/mariadb/tmp/mysqld.pid"
		"bind_address":           "0.0.0.0"

		"collation_server":     "utf8_unicode_ci"
		"init_connect":         "SET NAMES utf8"
		"character_set_server": "utf8"

		"key_buffer_size":        *"32M" | string
		"myisam_recover_options": *"FORCE,BACKUP" | string

		"skip_host_cache":    null
		"skip_name_resolve":  null
		"max_allowed_packet": *"16M" | string
		"max_connect_errors": *1000000 | int
		"sql_mode":           *"STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_AUTO_VALUE_ON_ZERO,NO_ENGINE_SUBSTITUTION,NO_ZERO_DATE,NO_ZERO_IN_DATE,ONLY_FULL_GROUP_BY" | string
		"sysdate_is_now":     *true | bool

		"log_bin":          "mysql-bin"
		"expire_logs_days": *14 | int
		"sync_binlog":      *false | bool

		"tmp_table_size":         *"32M" | string
		"max_heap_table_size":    *"32M" | string
		"query_cache_type":       *1 | int
		"query_cache_limit":      *"4M" | string
		"query_cache_size":       *"256M" | string
		"max_connections":        *500 | int
		"thread_cache_size":      *50 | int
		"open_files_limit":       *65535 | int
		"table_definition_cache": *4096 | int
		"table_open_cache":       *4096 | int
		"max_statement_time":     *10 | int

		"optimizer_switch": {
			"derived_merge": *false | bool
		}

		"innodb":             "force"
		"innodb_strict_mode": true
		// Mandatory per https://github.com/codership/documentation/issues/25
		"innodb_autoinc_lock_mode": 2
		// Per https://www.percona.com/blog/2006/08/04/innodb-double-write/
		"innodb_doublewrite":             *true | bool
		"innodb_flush_method":            *"O_DIRECT" | string
		"innodb_log_files_in_group":      *2 | int
		"innodb_flush_log_at_trx_commit": *1 | int
		"innodb_file_per_table":          *true | bool
		"innodb_buffer_pool_size":        *"2G" | string
		"innodb_file_format":             "Barracuda"

		// Disable writing to local disk without logrotation!
		"slow_query_log": *false | int

		"ssl":      null
		"ssl_ca":   *"\(#ssl_ca)" | string
		"ssl_cert": *"\(#ssl_cert)" | string
		"ssl_key":  *"\(#ssl_key)" | string
	}
	"galera": {
		"wsrep_on":                       *true | bool
		"wsrep_provider":                 "/opt/bitnami/mariadb/lib/libgalera_smm.so"
		"wsrep_sst_method":               *"mariabackup" | string
		"wsrep_slave_threads":            *4 | int
		"wsrep_cluster_address":          *"gcomm://" | string
		"wsrep_cluster_name":             *"galera" | string
		"wsrep_sst_auth":                 *"root:" | string
		"innodb_flush_log_at_trx_commit": *2 | int
		// row is required for galera
		"binlog_format": "row"
		"wsrep_provider_options": {
			"socket.ssl":        "yes"
			"socket.ssl_ca":     *"\(#replication_ssl_ca)" | string
			"socket.ssl_cert":   *"\(#replication_ssl_cert)" | string
			"socket.ssl_key":    *"\(#replication_ssl_key)" | string
			"gcomm.thread_prio": *"rr:2" | string
			"gcs.fc_limit":      *160 | int
			"gcs.fc_factor":     *0.8 | float
			"gcache.size":       *"2G" | string
		}
	}
	"sst": {
		"encrypt": *3 | 2 | 3 | 4
		"tkey":    *"\(#replication_ssl_key)" | string
		"tcert":   *"\(#replication_ssl_cert)" | string
		"tca":     *"\(#replication_ssl_ca)" | string
	}
}
