#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import yaook.common.config
import yaook.op.common
import yaook.statemachine as sm
import yaook.statemachine.resources
import yaook.statemachine.registry


class MemcachedService(sm.CustomResource):
    API_GROUP = "infra.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "memcachedservices"
    KIND = "MemcachedService"

    service = sm.TemplatedService(
        template="memcached-service.yaml",
        params={
            "memcached_component": "memcached",
        },
    )

    memcached_exporter_image = sm.VersionedDockerImage(
        'quay.io/prometheus/memcached-exporter',
        sm.SemVerSelector(prefix="v"),
    )

    exporter_certificate_secret = sm.EmptyTlsSecret(
        metadata=lambda ctx: (f"{ctx.parent_name}-memcached-exporter-", True),
    )

    exporter_certificate = sm.TemplatedCertificate(
        template="memcached-exporter-certificate.yaml",
        add_dependencies=[exporter_certificate_secret, service],
    )

    ready_exporter_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=exporter_certificate,
    )
    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )
    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload/service-reload',
        sm.YaookSemVerSelector(),
    )
    memcache_service_monitor = sm.GeneratedStatefulsetServiceMonitor(
        metadata=lambda ctx: (f"{ctx.parent_name}-memcache-service-monitor-",
                              True),
        service=service,
        certificate=ready_exporter_certificate_secret,
        endpoints=["metrics"],
    )

    memcached = sm.TemplatedStatefulSet(
        template="memcached-statefulset.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.INFRA_CACHING.value,
            yaook.op.common.SchedulingKey.ANY_INFRA.value,
        ],
        add_dependencies=[
            service,
            ready_exporter_certificate_secret,
        ],
        component=yaook.op.common.MEMCACHED_STATEFUL_COMPONENT,
        versioned_dependencies=[
            ssl_terminator_image,
            service_reload_image,
            memcached_exporter_image,
        ],
    )

    memcached_pdb = sm.QuorumPodDisruptionBudget(
         metadata=lambda ctx: f"{ctx.parent_name}-cache-pdb",
         replicated=memcached,
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


yaook.statemachine.registry.register(MemcachedService)
