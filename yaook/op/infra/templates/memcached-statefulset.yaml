##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ "%s-cache" | format(labels["state.yaook.cloud/parent-name"]) }}
spec:
  replicas: {{ crd_spec.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  podManagementPolicy: OrderedReady
  serviceName: {{ dependencies['service'].resource_name() }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
      containers:
        - name: memcached
          image: {{ crd_spec.imageRef }}
          imagePullPolicy: IfNotPresent
          env:
            - name: MEMCACHED_CACHE_SIZE
              value: {{ crd_spec.memory | string }}
            - name: MEMCACHED_MAX_CONNECTIONS
              value: {{ crd_spec.connections | string }}
          ports:
            - name: memcached
              containerPort: 11211
              protocol: TCP
          livenessProbe:
            tcpSocket:
              port: 11211
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 3
          readinessProbe:
            exec:
              command:
                - bash
                - -ec
                - |
                  { printf >&3 'stats\nquit\n'; cat <&3; } 3<>/dev/tcp/127.0.0.1/11211 | grep uptime
            periodSeconds: 10
            timeoutSeconds: 1
            successThreshold: 1
            failureThreshold: 3
          resources: {{ crd_spec | resources('memcached') }}
        - name: memecached-exporter
          image: {{ versioned_dependencies['memcached_exporter_image'] }}
          imagePullPolicy: Always
          args:
            - "--memcached.address=localhost:11211"
          ports:
            - name: prometheus
              containerPort: 9150
              protocol: TCP
          resources: {{ crd_spec | resources('memcached-exporter') }}
        - name: ssl-terminator
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "9337"
            - name: METRICS_PORT
              value: "8002"
            - name: LOCAL_PORT
              value: "9150"
          ports:
            - name: metrics
              containerPort: 9337
              protocol: TCP
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8002
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8002
              scheme: HTTPS
          resources: {{ crd_spec | resources('ssl-terminator') }}
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('service-reload') }}
      volumes:
        - name: ssl-terminator-config
          emptyDir: {}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_exporter_certificate_secret'].resource_name() }}

