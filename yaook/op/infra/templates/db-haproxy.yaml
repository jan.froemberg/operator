##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ "%s-prx" | format(labels['state.yaook.cloud/parent-name']) }}
spec:
  replicas: {{ crd_spec.proxy.replicas }}
  selector:
    matchLabels: {{ labels }}
  template:
    metadata:
      labels: {{ labels }}
      annotations:
        config-timestamp: {{ dependencies['haproxy_config'].last_update_timestamp() }}
    spec:
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ labels }}
      automountServiceAccountToken: false
      shareProcessNamespace: true
      terminationGracePeriodSeconds: {{ crd_spec.proxy.timeoutClient }}
      # TODO: add a container which watches the configuration file for changes
      # and reloads haproxy automatically, so that we can do zero-downtime
      # in-place updates.
      initContainers:
      - name: create-ca-bundle
        image: {{ versioned_dependencies['service_reload_image'] }}
        # Please note: Service Reloader will also recreate the bundle at runtime
        command: ['sh', '-c', "cat /cert/tls.key /cert/tls.crt /cert/ca.crt > /cert-bundle/bundle.pem"]
        volumeMounts:
        - name: metric-cert
          mountPath: /cert/
        - name: metric-cert-bundle
          mountPath: /cert-bundle/
        resources: {{ crd_spec | resources('proxy.create-ca-bundle') }}
      containers:
      - name: haproxy
        image: {{ versioned_dependencies['haproxy_image'] }}
        volumeMounts:
        - name: config
          mountPath: /usr/local/etc/haproxy/
        - name: metric-cert-bundle
          mountPath: /usr/local/etc/haproxy/metric-cert-bundle/
        resources: {{ crd_spec | resources('proxy.haproxy') }}
      - name: "service-reload"
        image: {{ versioned_dependencies['service_reload_image'] }}
        imagePullPolicy: Always
        volumeMounts:
        - name: metric-cert
          mountPath: /cert/
        - name: metric-cert-bundle
          mountPath: /cert-bundle/
        env:
          - name: YAOOK_SERVICE_RELOAD_MODULE
            value: haproxy
        args:
          - /cert/
        resources: {{ crd_spec | resources('proxy.service-reload') }}
      volumes:
      - name: metric-cert-bundle
        emptyDir: {}
      - name: config
        configMap:
          name: {{ dependencies['haproxy_config'].resource_name() }}
      - name: metric-cert
        secret:
          secretName: {{ dependencies['ready_haproxy_metrics_certificate_secret'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
