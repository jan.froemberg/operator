##
## Copyright (c) 2023 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
{% set _ = pod_labels.update(labels) %}
{% set _ = pod_labels.update({'state.yaook.cloud/ovsdb-access': "access-service" }) %}

{% if crd_spec.dbSchema == "northbound" %}
{%   set _port = 6641 %}

{%   set _stanza = "nb" %}
{%   set _dbname = "OVN_Northbound" %}
{% endif %}

{% if crd_spec.dbSchema == "southbound" %}
{%   set _port = 6642 %}

{%   set _stanza = "sb" %}
{%   set _dbname = "OVN_Southbound" %}
{% endif %}

{% set _statefulset_name = "%s-ovn-relay" | format(labels["state.yaook.cloud/parent-name"]) %}

{% set _dbfile = '/etc/ovn/ovn' + _stanza + '_db.db' %}
{% set _dbsock = 'punix:/var/run/ovn/ovn' + _stanza + '_db.sock' %}
{% set _dbctl = '/var/run/ovn/ovn' + _stanza + '_db.ctl' %}
{% set _pidfile = '/var/run/ovn/ovn' + _stanza + '_db.pid' %}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ _statefulset_name }}
spec:
  replicas: {{ crd_spec.ovnRelay.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  podManagementPolicy: OrderedReady
  serviceName: {{ dependencies['headless_relay_service'].resource_name() }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
      securityContext:
        fsGroup: 2500016
      volumes:
        - name: etc-ovn
          emptyDir: {}
        - name: ssl-terminator-config
          emptyDir: {}
        - name: var-run-ovn
          emptyDir: {}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_certificate_secret'].resource_name() }}
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
      containers:
        - name: ovn-relay
          volumeMounts:
            - name: etc-ovn
              mountPath: /etc/ovn
            - name: var-run-ovn
              mountPath: /var/run/ovn
            - name: tls-secret
              mountPath: /etc/ssl/private
          resources: {{ crd_spec.ovnRelay | resources('ovn-relay') }}
          image: {{ crd_spec.imageRef }}
          imagePullPolicy: Always
          securityContext:
            runAsUser: 2500016
          env:
            - name: DBCTL
              value: {{ _dbctl }}
            - name: DBFILE
              value: {{ _dbfile }}
            - name: DBNAME
              value: {{ _dbname }}
            - name: DBSOCK
              value: {{ _dbsock }}
            - name: MY_POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: MY_POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: OVSDB_SERVERS
              value: {{ ovsdb_servers }}
            - name: PORT
              value: "6632"
            - name: PIDFILE
              value: {{ _pidfile }}
          command:
            - bash
            - -ec
            - |
              set -euo pipefail
              ovsdb-tool create "${DBFILE}"
              ovsdb-server \
                --log-file=/dev/stdout \
                --ssl-protocols="db:${DBNAME},SSL,ssl_protocols" \
                --ssl-ciphers="db:${DBNAME},SSL,ssl_ciphers" \
                --private-key=/etc/ssl/private/tls.key \
                --certificate=/etc/ssl/private/tls.crt \
                --ca-cert=/etc/ssl/private/ca.crt \
                --remote="$DBSOCK" \
                --pidfile="${PIDFILE}" \
                --unixctl="${DBCTL}" \
                --remote=ptcp:"${PORT}":127.0.0.1 \
                "${DBFILE}" \
                relay:${DBNAME}:${OVSDB_SERVERS}
          startupProbe:
            exec:
              command:
                - bash
                - -ec
                - |
                  set -euo pipefail
                  ovsdb-client \
                      list-dbs "tcp:127.0.0.1:${PORT}"
            periodSeconds: 10
            timeoutSeconds: 5
            successThreshold: 1
            failureThreshold: 4
          livenessProbe:
            exec:
              command:
                - bash
                - -ec
                - |
                  set -euo pipefail
                  ovs-appctl -t "${DBCTL}" ovsdb-server/sync-status
            periodSeconds: 10
            timeoutSeconds: 5
            successThreshold: 1
            failureThreshold: 4
          readinessProbe:
            exec:
              command:
                - bash
                - -ec
                - |
                  set -euo pipefail

                  P="/var/run/ovn/readinessProbe.txt"
                  ovs-appctl -t "${DBCTL}" ovsdb-server/sync-status >"$P"

                  if grep -E 'state: active' "$P"; then
                    exit 0
                  fi

                  exit 1
            periodSeconds: 10
            timeoutSeconds: 5
            successThreshold: 1
            failureThreshold: 4
        - name: ssl-terminator
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: {{ _port | string }}
            - name: METRICS_PORT
              value: "8004"
            - name: LOCAL_PORT
              value: "6632"
            - name: TCP_MODE
              value: "true"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          ports:
            - name: metrics
              containerPort: 8004
              protocol: TCP
            - name: ovn-relay
              containerPort: {{ _port }}
              protocol: TCP
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            tcpSocket:
              port: {{ _port }}
          readinessProbe:
            tcpSocket:
              port: {{ _port }}
          resources: {{ crd_spec.ovnRelay | resources('ssl-terminator') }}
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec.ovnRelay | resources('service-reload') }}
