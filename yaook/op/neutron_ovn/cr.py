#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import environ
import typing

import yaook.common
import yaook.common.config
import yaook.op.common
import yaook.statemachine as sm
import yaook.statemachine.resources.openstack as resource
from . import resources


class BridgeMapping(typing.NamedTuple):
    bridge_name: str
    uplink_device: str
    openstack_physnet: str


class OVNConfigState(sm.CueSecret):
    def _extract_bridge_mappings(
            self,
            mappings: typing.List[typing.Dict],
    ) -> typing.List[BridgeMapping]:
        return [
            BridgeMapping(
                bridge_name=mapping["bridgeName"],
                uplink_device=mapping["uplinkDevice"],
                openstack_physnet=mapping["openstackPhysicalNetwork"],
            )
            for mapping in mappings
        ]

    async def _render_cue_config(
            self,
            ctx: sm.Context,
    ) -> sm.ResourceBody:
        config = await super()._render_cue_config(ctx)
        bridge_mappings = self._extract_bridge_mappings(
            ctx.parent_spec.get("bridgeConfig", [])
        )
        bridge_config = []
        for mapping in bridge_mappings:
            bridge_config.append(
                f'{mapping.bridge_name};{mapping.uplink_device};'
                f'{mapping.openstack_physnet}'
            )
        config["bridge_mappings"] = '\n'.join(bridge_config) + '\n'
        return config


class OvnMetadataSpecSequenceLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        southbound_servers = ','.join(
                            ctx.parent_spec.get("southboundServers", []))
        return {
            "neutron_ovn_metadata_agent":
                yaook.common.config.OSLO_CONFIG.declare(
                    ctx.parent_spec.get("neutronMetadataAgentConfig", []) +
                    [{
                        "ovn": {
                            "ovn_sb_connection": southbound_servers
                        }
                    }]
                ),
        }


@environ.config(prefix="YAOOK_NEUTRON_OVN_AGENT_OP")
class OpOVNAgentConfig:
    interface = environ.var("internal")


class NeutronOVNAgent(sm.ReleaseAwareCustomResource):
    API_GROUP = "network.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "neutronovnagents"
    KIND = "NeutronOVNAgent"
    ADDITIONAL_PERMISSIONS = (
        (False, "network.yaook.cloud", "neutronovnagents/status",
         {"get", "patch", "replace", "update"}),
        # Required to read the node state for judging the next steps in
        # OVNStateResource
        (True, "", "nodes", {"get", "patch", "watch"}),
    )
    RELEASES = [
        "yoga",
    ]

    VALID_UPGRADE_TARGETS: typing.List[str] = []

    ovn_controller_docker_image = sm.ConfigurableVersionedDockerImage(
        "ovn/ovn",
        sm.OVNVersionSelector(),
    )

    neutron_ovn_agent_docker_image = yaook.op.common.image_dependencies(
        "neutron-ovn-agent/neutron-ovn-agent-{release}",
        RELEASES,
    )

    openvswitch_docker_image = sm.ConfigurableVersionedDockerImage(
        "openvswitch/openvswitch",
        sm.SoftwareBuildVersionSelector(),
    )

    ovsdb_service = sm.TemplatedService(
        template="ovn-service.yaml",
        params={
            "generate_name": "neutron-ovn-ovsdb-server",
        },
    )

    keystone = sm.KeystoneReference()
    keystone_operator_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )

    nova = sm.NovaReference()
    nova_metadata_proxy_secret = sm.ForeignResourceDependency(
        resource_interface_factory=sm.secret_interface,
        foreign_resource=nova,
        foreign_component=yaook.op.common.NOVA_METADATA_SECRET_COMPONENT,
    )

    user = sm.StaticKeystoneUserWithParent(
        username="neutron-ovn",
        keystone=keystone
    )
    user_password = yaook.op.common.keystone_user_credentials_reference(
        user
    )

    ovsdb_server = resource.TemplatedRecreatingStatefulSet(
        template="ovsdb-server-statefulset.yaml",
        add_dependencies=[
            ovsdb_service,
        ],
        versioned_dependencies=[
            openvswitch_docker_image,
        ],
        params={
            "tolerations": list(map(
                sm.api_utils.make_toleration,
                yaook.op.common.OVN_SCHEDULING_KEYS,
            )),
        },
    )

    ovsdb_server_pdb = sm.DisallowedPodDisruptionBudget(
        metadata=("ovn-ovsdb-server-pdb-", True),
        replicated=ovsdb_server,
    )

    ovs_vswitchd_service = sm.TemplatedService(
        template="ovn-service.yaml",
        params={
            "generate_name": "neutron-ovs-vswitchd",
        },
    )

    ovs_vswitchd = resource.TemplatedRecreatingStatefulSet(
        template="ovs-vswitchd-statefulset.yaml",
        add_dependencies=[
            ovs_vswitchd_service,
        ],
        versioned_dependencies=[
            openvswitch_docker_image,
        ],
        params={
            "tolerations": list(map(
                sm.api_utils.make_toleration,
                yaook.op.common.OVN_SCHEDULING_KEYS,
            )),
        },
    )

    ovs_vswitchd_pdb = sm.DisallowedPodDisruptionBudget(
        metadata=("ovn-statefulset-vswitchd-pdb-", True),
        replicated=ovs_vswitchd,
    )

    ovn_config = OVNConfigState(
        metadata=("neutron-ovn-config-", True),
        copy_on_write=True,
    )

    ovn_metadata_config = sm.CueSecret(
        metadata=("neutron-ovn-metadata-", True),
        copy_on_write=True,
        add_cue_layers=[
            OvnMetadataSpecSequenceLayer(),
            sm.MetadataSecretLayer(
                metadata_key="neutron_ovn_metadata_agent",
                proxy_secret=nova_metadata_proxy_secret,
            ),
        ]
    )

    ovn_service = sm.TemplatedService(
        template="ovn-service.yaml",
        params={
            "generate_name": "neutron-ovn-controller",
        },
    )

    certificate_secret = sm.EmptyTlsSecret(
        metadata=("ovn-controller-certificate-", True),
    )

    certificate = sm.TemplatedCertificate(
        template="ovn-controller-certificate.yaml",
        add_dependencies=[
            certificate_secret,
            ovn_service,
            ],
    )

    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )

    ovn_controller = resource.TemplatedRecreatingStatefulSet(
        template='ovn-controller-statefulset.yaml',
        add_dependencies=[
            ovn_service,
            ready_certificate_secret,
            ovn_config,
            ovn_metadata_config,
        ],
        versioned_dependencies=[
            ovn_controller_docker_image,
            neutron_ovn_agent_docker_image,
        ],
        params={
            "tolerations": list(map(
                sm.api_utils.make_toleration,
                yaook.op.common.OVN_SCHEDULING_KEYS,
            )),
        },
    )

    ovn_controller_pdb = sm.DisallowedPodDisruptionBudget(
        metadata=("neutron-ovn-agent-pdb-", True),
        replicated=ovn_controller,
    )

    api_state = resources.OVNStateResource(
        finalizer="network.yaook.cloud/neutron-ovn-agent",
        endpoint_config=keystone_operator_api,
        credentials_secret=user_password,
        # scheduling_keys are needed for other APIStateResources, but without
        # a backgroundjob, we don't need to define them.
        scheduling_keys=[],
        add_dependencies=[ovn_controller]
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)
        config = environ.to_config(OpOVNAgentConfig)
        self.keystone_operator_api._foreign_component = {
            "internal": yaook.op.common.KEYSTONE_INTERNAL_API_COMPONENT,
            "public": yaook.op.common.KEYSTONE_PUBLIC_API_COMPONENT,
        }[config.interface]


sm.register(NeutronOVNAgent)
