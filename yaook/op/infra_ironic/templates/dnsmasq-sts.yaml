##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ "%s-dnsmasq" | format(labels['state.yaook.cloud/parent-name']) }}
  annotations:
    test.yaook.cloud/skip-ca-certs: "true"
spec:
  replicas: 1
  serviceName: {{ dependencies['dnsmasq_service'].resource_name() }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      hostNetwork: true
      containers:
      - name: dnsmasq
        image: {{ versioned_dependencies["dnsmasq_image"] }}
        command:
        - bash
        - -euo
        - pipefail
        - -c
        - |
          dhcp_listen_ip=$(ip a | grepcidr "$DHCP_LISTEN_CIDR" | head -n1 | awk '{print $2}' | cut -d '/' -f1)
          exec dnsmasq -d --listen-address="$dhcp_listen_ip"
        env:
        - name: DHCP_LISTEN_CIDR
          value: {{ crd_spec.pxe.listenNetwork }}
        volumeMounts:
        - name: config
          mountPath: /etc/dnsmasq.d
        - name: state
          mountPath: /var/lib/dnsmasq
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
        resources: {{ crd_spec | resources('dnsmasq.dnsmasq') }}
      volumes:
      - name: config
        projected:
          sources:
          - configMap:
              name: {{ dependencies['dnsmasq_config'].resource_name() }}
              items:
              - key: dnsmasq.conf
                path: config.conf
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
  volumeClaimTemplates:
  - metadata:
      name: state
    spec:
      accessModes:
      - ReadWriteOnce
      storageClassName: {{ crd_spec.dnsmasq.storageClassName }}
      resources:
        requests:
          storage: {{ crd_spec.dnsmasq.storageSize }}
