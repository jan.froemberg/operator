#!/usr/bin/env python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import typing

import yaook.common
import yaook.common.config
import yaook.op.common
import yaook.statemachine as sm
from yaook.statemachine import version_utils
import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.resources as sm_resources

from . import resources as neutron_resources


JOB_SCHEDULING_KEYS = [
    yaook.op.common.SchedulingKey.OPERATOR_NEUTRON.value,
    yaook.op.common.SchedulingKey.OPERATOR_ANY.value,
]


def _internal_endpoint_configured(ctx: sm.Context) -> bool:
    return ctx.parent_spec["api"].get("internal", {}) != {}


SERVICE_NAME = "neutron"
API_SVC_USERNAME = "api"


class OVNNeutronLayer(sm.CueLayer):
    """
    Cue layer for a OVN neutron settings.

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """
    async def get_layer(self, ctx: sm.Context) -> \
            yaook.common.config.MutableConfigSpec:
        return {
            "neutron": yaook.common.config.OSLO_CONFIG.declare([
                {
                    "DEFAULT": {
                        # this is a ugly fix, as we need the setup key here
                        # so we can add the right options to configfile
                        # at CueLayer.
                        "#setup": setup for setup in ctx.parent_spec["setup"]
                    }
                }
            ]),
        }


def is_ovs_setup(ctx):
    return 'ovs' in ctx.parent_spec['setup']


def is_ovn_setup(ctx):
    return 'ovn' in ctx.parent_spec['setup']


class OVSML2Layer(sm.CueLayer):
    """
    Cue layer for a OVS ML2 settings.
    """
    async def get_layer(
            self,
            ctx: sm.Context,
            ) -> yaook.common.config.MutableConfigSpec:
        if not is_ovs_setup(ctx):
            return {}

        return {
            "neutron_ml2": yaook.common.config.OSLO_CONFIG.declare([
                yaook.common.config.CueConfigReference(
                    "neutron_ml2.ml2_config_ovs"
                ),
            ]),
        }


class OVNML2Layer(sm.CueLayer):
    """
    Cue layer for a OVN ML2 settings.

    This is a :class:`CueLayer` for use with
    :ref:`yaook.statemachine.cue.ConfigurationClasses`.
    """
    def __init__(
            self,
            *,
            ovsdb_nb_service:
            sm_resources.orchestration.OptionalKubernetesReference,
            ovsdb_sb_service:
            sm_resources.orchestration.OptionalKubernetesReference,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(ovsdb_nb_service, ovsdb_sb_service)
        self._ovsdb_nb_service = ovsdb_nb_service
        self._ovsdb_sb_service = ovsdb_sb_service

    async def _get_ovn_db_ips(self, ctx: sm.Context) -> typing.Dict:
        connection_dict = {}
        for db_schema in ['nb', 'sb']:
            if db_schema == 'nb':
                port = yaook.op.common.OVSDB_NORTHBOUND_PORT
                service_ref = await self._ovsdb_nb_service.get_all(ctx)
            elif db_schema == 'sb':
                port = yaook.op.common.OVSDB_SOUTHBOUND_PORT
                service_ref = await self._ovsdb_sb_service.get_all(ctx)
            ovndb_service_list = \
                await yaook.op.common.get_ovn_db_servers(
                    ctx, service_ref, port)
            connection_dict[f"ovn_{db_schema}_connection"] = \
                ",".join(ovndb_service_list)
        return connection_dict

    async def get_layer(
            self,
            ctx: sm.Context,
            ) -> yaook.common.config.MutableConfigSpec:
        if not is_ovn_setup(ctx):
            return {}

        return {
            "neutron_ml2": yaook.common.config.OSLO_CONFIG.declare([
                yaook.common.config.CueConfigReference(
                    "neutron_ml2.ml2_config_ovn"
                ),
                {
                    "ovn": await self._get_ovn_db_ips(ctx,)
                },
            ]),
        }


class Neutron(sm.ReleaseAwareCustomResource):
    WATCH_NODES = True
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "neutrondeployments"
    KIND = "NeutronDeployment"
    ADDITIONAL_PERMISSIONS = (
        # The disruption budgets are hidden behind the InstancedResources
        (False, "yaook.cloud", "yaookdisruptionbudgets", {
            "list", "watch", "get"}),
        (False, "yaook.cloud", "yaookdisruptionbudgets/status", {
            "get", "patch"}),
    )
    RELEASES = [
        "queens",
        "train",
        "yoga",
    ]
    SETUP_RELEASE_MAP = {
        "ovs": ["queens", "train", ],
        "ovn": ["yoga", ],
    }
    VALID_UPGRADE_TARGETS: typing.List[str] = []

    neutron_docker_image = yaook.op.common.image_dependencies(
        "neutron/neutron-{release}",
        RELEASES,
    )
    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )
    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload/service-reload',
        sm.YaookSemVerSelector(),
    )
    mariadb_version = sm.MappedVersionedDependency(
        mapping={
            **{release: "10.2" for release in ["queens", "rocky", "stein"]},
            **{
                release: "10.6"
                for release in [
                    "train",
                    "ussuri",
                    "victoria",
                    "wallaby",
                    "xena",
                    "yoga",
                ]
            },
        },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx),
    )
    rabbitmq_image = sm.VersionedDockerImage(
        "library/rabbitmq",
        sm.SemVerSelector([">=3.8.0", "<4.0.0"], suffix="-management"),
    )
    ovn_image = sm.ConfigurableVersionedDockerImage(
        "ovn/ovn",
        sm.OVNVersionSelector(),
    )
    memcached_image = sm.VersionedDockerImage(
        "bitnami/memcached",
        sm.BitnamiVersionSelector([
            ([">=1.6.10", "<2.0.0"], []),
        ]),
    )

    keystone = sm.KeystoneReference()
    keystone_internal_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )

    neutron_policy = sm.Orphan(
        resource_interface_factory=sm.interfaces.config_map_interface
    )

    new_neutron_policy = sm.ReleaseAwarePolicyConfigMap(
        metadata=("neutron-policy-", True),
        component=yaook.op.common.POLICY_CONFIGMAP_COPMONENT,
        copy_on_write=True,
        versioned_dependencies=[
            neutron_docker_image,
        ]
    )

    ready_neutron_policy = sm.ReadyPolicyConfigMapReference(
        configmap_reference=new_neutron_policy
    )

    policy_validation_management_role = sm.TemplatedRole(
        template="common-policy-validation-role.yaml",
        params={
            "name": SERVICE_NAME,
        },
        add_dependencies=[
            new_neutron_policy
        ]
    )
    policy_validation_management_service_account = \
        sm.TemplatedServiceAccount(
            template="common-policy-validation-serviceaccount.yaml",
            component=yaook.op.common.SERVICE_ACCOUNT_COPMONENT,
            params={
                "name": SERVICE_NAME,
            },
        )
    policy_validation_management_role_binding = \
        sm.TemplatedRoleBinding(
            template="common-policy-validation-role-binding.yaml",
            params={
                "name": SERVICE_NAME,
            },
            add_dependencies=[
                policy_validation_management_role,
                policy_validation_management_service_account,
            ]
        )

    script = sm.PolicyValidationScriptConfigMap(
        metadata=("neutron-policy-validation-script-", True),
    )

    policy_validation = sm.PolicyValidator(
        template="common-policy-validator.yaml",
        params={
            "name": SERVICE_NAME,
        },
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[
            new_neutron_policy,
            script,
            policy_validation_management_service_account,
        ],
        versioned_dependencies=[
            neutron_docker_image,
        ]
    )

    db = sm.TemplatedMySQLService(
        template="database.yaml",
        params={
            "database_name": SERVICE_NAME,
        },
        versioned_dependencies=[mariadb_version],
    )
    db_api_user_password = sm.AutoGeneratedPassword(
        metadata=("neutron-db-api-user-", True),
    )
    db_api_user = sm.SimpleMySQLUser(
        metadata=("neutron-api-", True),
        database=db,
        username=API_SVC_USERNAME,
        password_secret=db_api_user_password,
    )
    db_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=db,
        foreign_component=yaook.op.common.MYSQL_DATABASE_SERVICE_COMPONENT,
    )

    mq = sm.TemplatedAMQPServer(
        template="amqp.yaml",
        versioned_dependencies=[rabbitmq_image],
    )
    mq_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=mq,
        foreign_component=yaook.op.common.AMQP_SERVER_SERVICE_COMPONENT,
    )

    mq_api_user_password = sm.AutoGeneratedPassword(
        metadata=("neutron-mq-api-user-", True),
        copy_on_write=True,
    )
    mq_api_user = sm.SimpleAMQPUser(
        metadata=("neutron-api-", True),
        server=mq,
        username_format=API_SVC_USERNAME,
        password_secret=mq_api_user_password,
    )

    memcached = sm.TemplatedMemcachedService(
        template="memcached.yaml",
        versioned_dependencies=[memcached_image],
    )
    memcached_statefulset = sm.ForeignResourceDependency(
        resource_interface_factory=sm.stateful_set_interface,
        foreign_resource=memcached,
        foreign_component=yaook.op.common.MEMCACHED_STATEFUL_COMPONENT,
    )

    nova = sm.NovaReference()

    certificate_secret = sm.EmptyTlsSecret(
        metadata=("neutron-api-certificate-", True),
    )
    certificate = sm.TemplatedCertificate(
        template="neutron-api-certificate.yaml",
        add_dependencies=[certificate_secret],
    )
    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )
    ca_certs = sm.CAConfigMap(
        metadata=("neutron-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            ready_certificate_secret,
        ],
    )

    ovn_central_ca_certificate_secret = sm.Optional(
        condition=is_ovn_setup,
        wrapped_state=sm.EmptyTlsSecret(
            metadata=lambda ctx: (f"{ctx.parent_name}-ovn-central-ca-", True),
        )
    )

    ovn_central_ca_certificate = sm.OptionalKubernetesReference(
        condition=is_ovn_setup,
        wrapped_state=sm.TemplatedCertificate(
            template="ovn-central-ca-certificate.yaml",
            add_dependencies=[ovn_central_ca_certificate_secret],
        )
    )

    ready_ovn_central_ca_certificate_secret = sm.Optional(
        condition=is_ovn_setup,
        wrapped_state=sm.ReadyCertificateSecretReference(
            certificate_reference=ovn_central_ca_certificate,
        )
    )

    ovn_central_ca = sm.Optional(
        condition=is_ovn_setup,
        wrapped_state=sm.TemplatedIssuer(
            template="ovn-central-ca.yaml",
            add_dependencies=[ready_ovn_central_ca_certificate_secret],
        )
    )

    ovsdb_nb = sm.OptionalKubernetesReference(
        condition=is_ovn_setup,
        wrapped_state=sm.TemplatedOVSDBService(
            template="northbound-ovsdb.yaml",
            add_dependencies=[ovn_central_ca],
            params={
                "db_schema": 'northbound',
            },
            versioned_dependencies=[ovn_image],
        )
    )
    ovsdb_nb_service = sm.OptionalKubernetesReference(
        condition=is_ovn_setup,
        wrapped_state=sm.ForeignResourceDependency(
            resource_interface_factory=sm.service_interface,
            foreign_resource=ovsdb_nb,
            foreign_component=yaook.op.common.OVSDB_DATABASE_SERVICE_COMPONENT,
        )
    )
    ovsdb_sb = sm.OptionalKubernetesReference(
        condition=is_ovn_setup,
        wrapped_state=sm.TemplatedOVSDBService(
            template="southbound-ovsdb.yaml",
            add_dependencies=[ovn_central_ca],
            params={
                "db_schema": 'southbound',
            },
            versioned_dependencies=[ovn_image],
        )
    )
    ovsdb_sb_service = sm.OptionalKubernetesReference(
        condition=is_ovn_setup,
        wrapped_state=sm.ForeignResourceDependency(
            resource_interface_factory=sm.service_interface,
            foreign_resource=ovsdb_sb,
            foreign_component=yaook.op.common.OVSDB_DATABASE_SERVICE_COMPONENT,
        )
    )
    # The access service points either to the ovn-relay or ovsdb, if no relay
    # is deployed.
    southbound_ovsdb_access_service = sm.OptionalKubernetesReference(
        condition=is_ovn_setup,
        wrapped_state=sm.ForeignResourceDependency(
            resource_interface_factory=sm.service_interface,
            foreign_resource=ovsdb_sb,
            foreign_component=yaook.op.common.OVN_ACCESS_SERVICE_COMPONENT,
        )
    )

    northd_certificate_secret = sm.Optional(
        condition=is_ovn_setup,
        wrapped_state=sm.EmptyTlsSecret(
            metadata=("northd-certificate-", True),
        )
    )

    northd_certificate = sm.OptionalKubernetesReference(
        condition=is_ovn_setup,
        wrapped_state=sm.TemplatedCertificate(
            template="northd-certificate.yaml",
            add_dependencies=[
                northd_certificate_secret,
                ovn_central_ca
            ],
        )
    )

    ready_northd_certificate_secret = sm.Optional(
        condition=is_ovn_setup,
        wrapped_state=sm.ReadyCertificateSecretReference(
            certificate_reference=northd_certificate,
        )
    )

    northd = sm.OptionalKubernetesReference(
        condition=is_ovn_setup,
        wrapped_state=neutron_resources.OVSDBAwareDeployment(
            template="northd-deployment.yaml",
            scheduling_keys=[
                yaook.op.common.SchedulingKey.NETWORK_NEUTRON_NORTHD.value,
            ],
            add_dependencies=[
                # northd needs to use the direct ovsdb services, not relay.
                # so not use access service here.
                ovsdb_nb_service,
                ovsdb_sb_service,
                # I actually think, we don't need this, but it makes the
                # pipeline happy and don't hurt, I guess
                ca_certs,
                ready_northd_certificate_secret,
            ],
            versioned_dependencies=[
                ovn_image,
            ],
        )
    )
    northd_deployment_pdb = sm.Optional(
        condition=is_ovn_setup,
        wrapped_state=sm.QuorumPodDisruptionBudget(
            metadata=("northd-pdb-", True),
            replicated=northd,
        )
    )

    api_keystone_user = sm.StaticKeystoneUser(
        username="neutron",
        keystone=keystone,
    )
    api_keystone_user_credentials = \
        yaook.op.common.keystone_user_credentials_reference(api_keystone_user)
    keystone_endpoint = sm.Optional(
        condition=yaook.op.common.publish_endpoint,
        wrapped_state=sm.TemplatedKeystoneEndpoint(
            template="neutron-keystone-endpoint.yaml",
            add_dependencies=[keystone],
        )
    )
    config = sm.CueSecret(
        metadata=("neutron-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="neutron",
                accessor="neutronConfig",
            ),
            OVNNeutronLayer(),
            sm.SecretInjectionLayer(
                target="neutron",
                accessor=lambda ctx: ctx.parent_spec.get("neutronSecrets", []),
            ),
            sm.SpecLayer(
                target="neutron_ml2",
                accessor="neutronML2Config",
            ),
            OVSML2Layer(),
            OVNML2Layer(
                # neutron-api needs direct ovsdb access
                # so not use access service here.
                ovsdb_nb_service=ovsdb_nb_service,
                ovsdb_sb_service=ovsdb_sb_service,
            ),
            sm.DatabaseConnectionLayer(
                target="neutron",
                service=db_service,
                database_name=SERVICE_NAME,
                username=API_SVC_USERNAME,
                password_secret=db_api_user_password,
                config_section="database",
            ),
            sm.AMQPTransportLayer(
                target="neutron",
                service=mq_service,
                username=API_SVC_USERNAME,
                password_secret=mq_api_user_password,
            ),
            sm.KeystoneAuthLayer(
                target="neutron",
                credentials_secret=api_keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.KeystoneAuthLayer(
                target="neutron",
                config_section="nova",
                credentials_secret=api_keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.KeystoneAuthLayer(
                target="neutron",
                config_section="placement",
                credentials_secret=api_keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.MemcachedConnectionLayer(
                target="neutron",
                memcached_sfs=memcached_statefulset,
            ),
            sm.RegionNameConfigLayer(
                target="neutron",
            ),
            sm.JSONPolicyLayer(target="neutron"),
        ],
    )
    db_sync = sm.TemplatedJob(
        template="neutron-job-db-sync.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[config, ca_certs],
        versioned_dependencies=[neutron_docker_image],
    )

    external_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx:
            ctx.parent_spec["api"]["ingress"]
            .get("externalCertificateSecretRef", {}).get("name"),
        secret_reference=ready_certificate_secret
    )

    internal_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx:
            ctx.parent_spec["api"]["internal"]["ingress"]
            .get("externalCertificateSecretRef", {}).get("name"),
        secret_reference=ready_certificate_secret
    )

    ml2_plugin_certificate_secret = sm.Optional(
        condition=is_ovn_setup,
        wrapped_state=sm.EmptyTlsSecret(
            metadata=("ml2-plugin-certificate-", True),
        )
    )

    ml2_plugin_certificate = sm.OptionalKubernetesReference(
        condition=is_ovn_setup,
        wrapped_state=sm.TemplatedCertificate(
            template="ml2-plugin-certificate.yaml",
            add_dependencies=[
                ml2_plugin_certificate_secret,
                ovn_central_ca
            ],
        )
    )

    ready_ml2_plugin_certificate_secret = sm.OptionalKubernetesReference(
        condition=is_ovn_setup,
        wrapped_state=sm.ReadyCertificateSecretReference(
            certificate_reference=ml2_plugin_certificate,
        )
    )

    api_deployment = sm.TemplatedDeployment(
        template="neutron-deployment-api.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.NETWORK_API.value,
            yaook.op.common.SchedulingKey.ANY_API.value,
        ],
        add_dependencies=[
            config,
            db_sync,
            ca_certs,
            ready_certificate_secret,
            ready_neutron_policy,
            external_certificate_secret,
            internal_certificate_secret,
            ready_ml2_plugin_certificate_secret,
        ],
        versioned_dependencies=[
            neutron_docker_image,
            ssl_terminator_image,
            service_reload_image,
        ],
    )
    api_deployment_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("neutron-api-pdb-", True),
         replicated=api_deployment,
    )

    api_service = sm.TemplatedService(
        template="neutron-api-service.yaml",
        add_dependencies=[api_deployment],
    )

    internal_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-internal-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=ready_certificate_secret,
        endpoints=["internal-ssl-terminator-prometheus"],
    )

    external_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-external-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=external_certificate_secret,
        server_name_provider=lambda ctx: (
            ctx.parent_spec["api"]["ingress"]["fqdn"]
        ),
        endpoints=["external-ssl-terminator-prometheus"],
    )

    internal_ingress_ssl_service_monitor = sm.Optional(
        condition=_internal_endpoint_configured,
        wrapped_state=sm.GeneratedServiceMonitor(
           metadata=lambda ctx: (
               f"{ctx.parent_name}-internal-ingress-ssl-service-monitor-",
               True),
           service=api_service,
           certificate=internal_certificate_secret,
           server_name_provider=lambda ctx: (
               ctx.parent_spec["api"]["internal"]["ingress"]["fqdn"]
           ),
           endpoints=["internal-ingress-ssl-terminator-prometheus"],
        )
    )

    api_ingress = sm.TemplatedIngress(
        template="neutron-api-ingress.yaml",
        add_dependencies=[api_service],
    )

    internal_api_ingress = sm.Optional(
        condition=_internal_endpoint_configured,
        wrapped_state=sm.TemplatedIngress(
            template="neutron-internal-api-ingress.yaml",
            add_dependencies=[api_service],
        )
    )

    l2_agents = sm.Optional(
        condition=is_ovs_setup,
        wrapped_state=neutron_resources.NeutronL2Agents(
            scheduling_keys=yaook.op.common.L2_SCHEDULING_KEYS,
            wrapped_state=neutron_resources.TemplatedNeutronL2Agent(
                template="neutron-l2-agent.yaml",
            ),
            add_dependencies=[
                ca_certs,
                keystone,
                mq,
            ],
        )
    )

    ovn_agents = sm.Optional(
        condition=is_ovn_setup,
        wrapped_state=neutron_resources.NeutronOVNAgents(
            scheduling_keys=yaook.op.common.OVN_SCHEDULING_KEYS,
            wrapped_state=neutron_resources.TemplatedNeutronOVNAgent(
                template="neutron-ovn-agent.yaml",
            ),
            add_dependencies=[
                ca_certs,
                keystone,
                nova,
                ovn_central_ca,
                southbound_ovsdb_access_service,
            ],
        )
    )

    dhcp_agents = sm.Optional(
        condition=is_ovs_setup,
        wrapped_state=neutron_resources.NeutronDHCPAgents(
            scheduling_keys=yaook.op.common.DHCP_SCHEDULING_KEYS,
            wrapped_state=neutron_resources.TemplatedNeutronDHCPAgent(
                template="neutron-dhcp-agent.yaml",
            ),
            add_dependencies=[
                ca_certs,
                keystone,
                mq,
                nova,
            ],
        )
    )

    l3_agents = sm.Optional(
        condition=is_ovs_setup,
        wrapped_state=neutron_resources.NeutronL3Agents(
            scheduling_keys=yaook.op.common.L3_SCHEDULING_KEYS,
            wrapped_state=neutron_resources.TemplatedNeutronL3Agent(
                template="neutron-l3-agent.yaml",
            ),
            add_dependencies=[
                ca_certs,
                keystone,
                mq,
                nova,
            ],
        )
    )

    bgp_dragents = sm.Optional(
        condition=is_ovs_setup,
        wrapped_state=neutron_resources.NeutronBGPAgents(
            setup_key="ovs",
            scheduling_keys=yaook.op.common.BGP_SCHEDULING_KEYS,
            wrapped_state=neutron_resources.TemplatedNeutronBGPDRAgent(
                template="neutron-bgp-dragent.yaml",
            ),
            add_dependencies=[
                ca_certs,
                keystone,
                mq,
                nova,
            ],
        )
    )

    ovn_bgp_agents = sm.Optional(
        condition=is_ovn_setup,
        wrapped_state=neutron_resources.NeutronBGPAgents(
            setup_key="ovn",
            scheduling_keys=yaook.op.common.OVN_BGP_SCHEDULING_KEYS,
            wrapped_state=neutron_resources.TemplatedNeutronOVNBGPAgent(
                template="neutron-ovn-bgp-agent.yaml",
            ),
            add_dependencies=[
                ovn_central_ca,
            ],
        )
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)

    async def validate_setup_and_release(self, ctx: sm.Context) -> None:
        release = version_utils.get_target_release(ctx)
        setup, = ctx.parent_spec["setup"].keys()
        if release not in self.SETUP_RELEASE_MAP[setup]:
            raise exceptions.ConfigurationInvalid(
                f"{setup} is not supported for openstack release {release}."
            )

    async def _validate_config(self, ctx: sm.Context) -> None:
        await self.validate_setup_and_release(ctx)
        await super()._validate_config(ctx)
        await self._validate_bgp_config(ctx)

    async def _validate_bgp_config(self, ctx: sm.Context) -> None:
        keys = ctx.parent_spec["setup"].get("ovs", {}).get("bgp", {}).keys()
        for configkey in keys:
            if configkey == configkey.lower():
                pass
            else:
                ctx.logger.error(
                    "Configuration for BGPDRAgents is invalid."
                    " Configuration keys for the different BGPDRAgents need "
                    "to be lowercase."
                )
                raise exceptions.ConfigurationInvalid(
                    "Configkeys for the BGPDRAgents need to be lowercase")


sm.register(Neutron)
