#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from __future__ import annotations

import dataclasses
import enum
import random
import typing
import asyncio
import kubernetes_asyncio
import kubernetes_asyncio.stream as kstream
import kubernetes_asyncio.client
from yaook.statemachine.resources.orchestration import kubernetes_run
import os
import time
import logging
import shlex


import openstack
from novaclient.client import Client

import yaook.statemachine.commoneviction as eviction
import yaook.statemachine.resources.openstack as os_resource
import yaook.statemachine.interfaces as interfaces

POLL_MIGRATION_RELEASES = ["queens", "rocky", "stein", "train"]


@dataclasses.dataclass(frozen=True)
class ComputeServiceInfo:
    id_: str
    host: str
    binary: str
    is_up: bool
    is_enabled: bool
    disable_reason: typing.Optional[str]
    is_forced_down: bool

    @classmethod
    def get(cls,
            compute_client: openstack.compute.v2._proxy.Proxy,
            *,
            host: str, binary: str) -> ComputeServiceInfo:
        services = [
            svc_info
            for svc_info in compute_client.get(
                "/os-services",
            ).json()["services"]
            if svc_info["host"] == host and svc_info["binary"] == binary
        ]
        if not services:
            raise LookupError(f"{binary} on {host} not found")

        svc_info = services[0]
        return cls(
            id_=svc_info["id"],
            host=svc_info["host"],
            binary=svc_info["binary"],
            is_up=svc_info["state"] == "up",
            is_enabled=svc_info["status"] == "enabled",
            disable_reason=svc_info["disabled_reason"],
            is_forced_down=svc_info["forced_down"],
        )

    def disable(self,
                compute_client: openstack.compute.v2._proxy.Proxy,
                reason: typing.Optional[str]) -> None:
        os_resource.raise_response_error_if_any(
            compute_client.put(
                f"/os-services/{self.id_}",
                json={
                    "status": "disabled",
                    "disabled_reason": reason,
                }
            )
        )


@dataclasses.dataclass(frozen=True)
class EvictionStatus:
    migratable_instances: int
    actions_in_progress: int
    pending_instances: int
    unhandleable_instances: int

    def as_json_object(self) -> typing.Mapping[str, typing.Any]:
        return {
            "migratable": self.migratable_instances,
            "in_progress": self.actions_in_progress,
            "pending": self.pending_instances,
            "unhandleable": self.unhandleable_instances,
        }


class ServerStatusClass(enum.Enum):
    MIGRATABLE = "migratable"
    OFFLINE_MIGRATABLE = "offline_migratable"
    CONFIRMABLE = "confirmable"
    OFFLOADABLE = "offloadable"
    FORCE_DELETABLE = "force-deletable"
    PENDING = "pending"
    MIGRATING = "migrating"
    UNHANDLEABLE = "unhandleable"
    IGNORABLE = "ignorable"
    ACTIVATABLE = "activatable"
    EVACUATABLE = "evacuatable"


ALL_OPENSTACK_STATUSES = [
    "ACTIVE",
    "BUILD",
    "DELETED",
    "ERROR",
    "HARD_REBOOT",
    "MIGARTING",
    "PASSWORD",
    "PAUSED",
    "REBOOT",
    "REBUILD",
    "RESCUE",
    "RESIZE",
    "REVERT_RESIZE",
    "SHELVED",
    "SHELVED_OFFLOADED",
    "SHUTOFF",
    "SOFT_DELETED",
    "SUSPENDED",
    "UNKNOWN",
    "VERIFY_RESIZE",
]


STATUS_MAP = {
    # obvious, do a live migration
    "ACTIVE": ServerStatusClass.MIGRATABLE,
    # wait until it has completed building
    "BUILD": ServerStatusClass.PENDING,
    # obvious, it’s gone anyway
    "DELETED": ServerStatusClass.IGNORABLE,
    # This is tricky: we cannot migrate a server which is in ERROR state, but
    # at the same time, it might be running and only in ERROR because of other
    # reasons.
    # TODO: consider automatically overriding the status to ACTIVE if the
    # power_state is Running; for now, we log it as unhandleable.
    "ERROR": ServerStatusClass.UNHANDLEABLE,
    # Wait until it’s finished rebooting.
    "HARD_REBOOT": ServerStatusClass.PENDING,
    # obvious.
    "MIGRATING": ServerStatusClass.MIGRATING,
    # Wait until the password reset operation is done.
    "PASSWORD": ServerStatusClass.PENDING,
    # Just trigger a live migration (works, tested)
    "PAUSED": ServerStatusClass.MIGRATABLE,
    # Wait until reboot is complete.
    "REBOOT": ServerStatusClass.PENDING,
    # Wait until rebuild is complete.
    "REBUILD": ServerStatusClass.PENDING,
    # This one is tricky. One cannot live-migrate while the instance is in
    # RESCUE mode. We thus treat it as unhandleable and thus alert the operator
    # about the issue.
    "RESCUE": ServerStatusClass.UNHANDLEABLE,
    # Offline migrating or real resize, wait until it’s over. Note that we need
    # to keep track of those we offline-migrated because we don’t see them in
    # our host’s listing anymore afterward.
    "RESIZE": ServerStatusClass.MIGRATING,
    # Wait until it’s over...
    "REVERT_RESIZE": ServerStatusClass.PENDING,
    # Force offloading immediately.
    "SHELVED": ServerStatusClass.OFFLOADABLE,
    # Can appear in a host listing while the server is unshelving(!). We need
    # to wait for it.
    "SHELVED_OFFLOADED": ServerStatusClass.PENDING,
    # Do an offline migration.
    "SHUTOFF": ServerStatusClass.OFFLINE_MIGRATABLE,
    # The intent is clear… delete it.
    "SOFT_DELETED": ServerStatusClass.FORCE_DELETABLE,
    # This is a very tricky one. SUSPENDED cannot be migrated or live-migrated.
    # We can resume it, pause it immediately, trigger a live-migration, unpause
    # and suspend.
    # However, this is tricky to orchestrate, especially as filtering by tags
    # is not reliable in the nova version we are using (it simply returns all
    # the servers).
    # That means we have to keep state in the job itself, which is dangerous
    # (it could be killed).
    # Marking it as unhandleable for now.
    "SUSPENDED": ServerStatusClass.UNHANDLEABLE,
    # Confirm.
    "VERIFY_RESIZE": ServerStatusClass.CONFIRMABLE,
}


# the default in _collect_server_status is UNHANDLEABLE
DOWN_STATUS_MAP: typing.Dict[str, ServerStatusClass] = {}


FORCED_DOWN_STATUS_MAP = {
    # Classic standard case.
    "ACTIVE": ServerStatusClass.EVACUATABLE,
    # Under construction on this host? Bad luck.
    # NOTE: this is also special-cased so that we don’t re-activate a BUILD
    # server which is still in BUILD because it is being evacuated.
    "BUILD": ServerStatusClass.ACTIVATABLE,
    # Don’t care.
    "DELETED": ServerStatusClass.IGNORABLE,
    # Evacuation should work
    "ERROR": ServerStatusClass.EVACUATABLE,
    # First bring to active, then evacuate it.
    "HARD_REBOOT": ServerStatusClass.ACTIVATABLE,
    # Let us be careful here: The instance could be either inbound or outbound
    # and the edge cases are probably megafugly here.
    # Hence, we do not handle it and instead let the operator take care.
    "MIGRATING": ServerStatusClass.UNHANDLEABLE,
    # These are a bunch of "tough luck" cases.
    "PASSWORD": ServerStatusClass.ACTIVATABLE,
    # We cannot force it to respawn as SHUTOFF, thus we treat it as
    # unhandleable.
    "PAUSED": ServerStatusClass.UNHANDLEABLE,
    "REBOOT": ServerStatusClass.ACTIVATABLE,
    # We need to activate it first, nova doesn’t support migrating from rescue
    "RESCUE": ServerStatusClass.ACTIVATABLE,
    # Unclear whether this is inbound or outbound again, so we treat it as
    # unhandleable.
    "RESIZE": ServerStatusClass.UNHANDLEABLE,
    # Wait until it’s gone.
    "REVERT_RESIZE": ServerStatusClass.PENDING,
    # Doesn’t work either, and we cannot force it to respawn in SHUTOFF state,
    # thus we have to treat it as UNHANDLEABLE
    "SHELVED": ServerStatusClass.UNHANDLEABLE,
    # It’s gone. (hopefully)
    "SHELVED_OFFLOADED": ServerStatusClass.IGNORABLE,
    # The intent is clear.
    "SOFT_DELETED": ServerStatusClass.FORCE_DELETABLE,
    # Same problem as PAUSED and SHELVED.
    "SUSPENDED": ServerStatusClass.UNHANDLEABLE,
    # While reverting would be smart, it does, unfortunately, not really work.
    # So we have to confirm and then move on.
    "VERIFY_RESIZE": ServerStatusClass.CONFIRMABLE,
}


ServerStatusMap = typing.Mapping[
    ServerStatusClass,
    typing.Collection[openstack.compute.v2.server.Server],
]


class ActionProtocol(typing.Hashable, typing.Protocol):
    __name__: str

    def __call__(
            self,
            client: openstack.connection.Connection,
            server_id: str,
            release: str,
            k8s_client: kubernetes_asyncio.client.ApiClient
            ) -> None:
        ...


def action(f: typing.Callable[
        [
            openstack.connection.Connection, str,
            str,
            kubernetes_asyncio.client.ApiClient
        ], None]) -> ActionProtocol:
    return typing.cast(ActionProtocol, f)


# Fun fact: While most of these have helpers, the helpers oftentimes do *not*
# raise HTTP errors as exceptions, which is kind of not what we want.


async def set_migration_speed(
        api_client: kubernetes_asyncio.client.ApiClient,
        namespace: str,
        node: str,
        instance_name: str,
        speed: int) -> None:
    """
    Execs into the novacompute pod on the given node and runs the virsh
    migrate-setspeed command based on the given instance and speed.
    """
    pods = interfaces.pod_interface(api_client)
    pod, = await pods.list_(
            namespace,
            label_selector={
                "state.yaook.cloud/parent-name": node,
                "state.yaook.cloud/parent-plural": "novacomputenodes",
                "state.yaook.cloud/component": "compute"
            },
        )
    async with kstream.WsApiClient(api_client.configuration) as ws_client:
        await kubernetes_run(
            ws_client,
            namespace=namespace,
            pod=pod.metadata.name,
            container="libvirtd",
            command=["/bin/bash", "-c",
                     f"virsh migrate-setspeed --bandwidth {speed} "
                     f"{instance_name}"],
        )


async def check_virsh_is_emtpy(
        logger: logging.Logger,
        api_client: kubernetes_asyncio.client.ApiClient,
        namespace: str,
        node: str) -> typing.Optional[bool]:
    """
    Execs into the novacompute pod on the given node and runs virsh list. Only
    returns True, when there is no VM running. Returns None if it was not able
    to run virsh list.
    """
    try:
        pods = interfaces.pod_interface(api_client)
        pod, = await pods.list_(
                namespace,
                label_selector={
                    "state.yaook.cloud/parent-name": node,
                    "state.yaook.cloud/parent-plural": "novacomputenodes",
                    "state.yaook.cloud/component": "compute"
                },
            )
        async with kstream.WsApiClient(api_client.configuration) as ws_client:
            result = await kubernetes_run(
                ws_client,
                namespace=namespace,
                pod=pod.metadata.name,
                container="libvirtd",
                command=["/bin/bash", "-c", "virsh list --all | grep instance"]
            )

            if result.returncode == 0:
                return False
            return True
    except Exception as e:
        logger.exception(e)
        return None


async def check_ovs_flows_present(
        api_client: kubernetes_asyncio.client.api_client.ApiClient,
        namespace: str,
        node: str,
        mac_devices: typing.List[str]) -> bool:
    """
    Connects to the l2 agent pod on the given node and checks if the flows
    contain the given mac addresses. Only returns true, if all mac addresses
    are present.
    """
    pods = interfaces.pod_interface(api_client)
    pod, = await pods.list_(
            namespace,
            label_selector={
                "state.yaook.cloud/parent-name": node,
                "state.yaook.cloud/component": "l2_agent"
            },
        )
    async with kstream.WsApiClient(api_client.configuration) as ws_client:
        flows_present = True
        grep_command = "grep -Fo"
        for mac_device in mac_devices:
            grep_command += f" -e {shlex.quote(mac_device)}"
        result = await kubernetes_run(
            ws_client,
            namespace=namespace,
            pod=pod.metadata.name,
            container="neutron-openvswitch-agent",
            command=["/bin/bash", "-c",
                     f'test $(ovs-ofctl dump-flows br-int | {grep_command} | '
                     f'sort -u | wc -l) -eq {len(mac_devices)}']
        )
        if result.returncode != 0:
            flows_present = False
        return flows_present


def check_root_device_is_local(
        openstack_client: openstack.connection.Connection,
        server: openstack.compute.v2.server.Server) -> bool:
    """
    As there is no obvious flag in the server object, that indicates if it is
    based on a local disk, this has to be evaluated based on the attached
    volumes. If there are no attached volumes the root disk must be local.
    Otherwise you have to compare if the mount of the volumes match the name of
    the root_device of the server.
    """
    if not server.attached_volumes:
        return True
    root_device = server.root_device_name
    for server_volume in server.attached_volumes:
        volume = openstack_client.volume.get_volume(server_volume["id"])
        root_device_attachments = [
                attachment
                for attachment in volume.attachments
                if attachment["device"] == root_device
            ]
        if root_device_attachments:
            return False
    return True


def get_migration_speed(
        openstack_client: openstack.connection.Connection,
        server: openstack.compute.v2.server.Server) -> int:
    """
    If the server does not use a local disk, we can limit the migration speed
    to the smallest possible value, as we are able to increase the ram copy
    afterwards. This is not possible when using a local disk, as there is no
    way to increase the speed of the local disk copy after starting it.
    So the speed for a vm with a local disk is normally higher but depends of
    the average size of the local disk in the environment. It can be configured
    using the YAOOK_NOVA_COMPUTE_EVICT_POLL_MIGRATION_SPEED_LOCAL_DISK env var.
    The speed is defined in MiB/s. Set 30 MiB/s, based on the experience in
    our production environment.
    """
    if check_root_device_is_local(openstack_client, server):
        return int(os.getenv(
            "YAOOK_NOVA_COMPUTE_EVICT_POLL_MIGRATION_SPEED_LOCAL_DISK", "30"))
    else:
        return 1


async def poll_migration(
        openstack_client: openstack.connection.Connection,
        k8s_client: kubernetes_asyncio.client.ApiClient,
        server_id: str,
        src_host: str,
        namespace: str) -> typing.Optional[float]:
    logger = logging.getLogger(
            __name__ + "." + namespace + "." + src_host + "." + server_id,
        )
    server = openstack_client.compute.get_server(server_id)
    # When the source hypervisor does not match the current hypervisor of the
    # server the migration has already succeeded.
    if server.hypervisor_hostname != src_host:
        return None

    async with kubernetes_asyncio.client.ApiClient(
            configuration=k8s_client.configuration) as api_client:
        # Need to access the current migrations on order to get the destination
        # hypervisor later on.
        nova_client = None
        if os.getenv("OS_REGION_NAME") is not None:
            nova_client = Client(version="2.60", cacert=os.getenv("OS_CACERT"),
                                 endpoint_type=os.getenv("OS_INTERFACE"),
                                 region_name=os.getenv("OS_REGION_NAME"),
                                 **openstack_client.auth)
        else:
            nova_client = Client(version="2.60", cacert=os.getenv("OS_CACERT"),
                                 endpoint_type=os.getenv("OS_INTERFACE"),
                                 **openstack_client.auth)
        migrations = nova_client.server_migrations.list(server_id)
        if not migrations:
            speed = get_migration_speed(openstack_client, server)
            logger.debug(f"Reducing migration speed to {speed} MiB/s")
            await set_migration_speed(
                api_client, namespace, src_host, server.instance_name, speed
            )
            logger.debug("Start migration")
            openstack_client.compute.live_migrate_server(
                server_id,
                host=None,
                force=False,)
            return 5.0
        dest_host = migrations[0].dest_node
        mac_devices = [port.mac_address for port
                       in openstack_client.network.ports(
                            device_id=server_id)
                       if port.is_port_security_enabled]
        if mac_devices:
            flows_present = await check_ovs_flows_present(
                api_client, namespace, dest_host, mac_devices)
        else:
            flows_present = True
        if flows_present:
            # During local disk migrations, virsh cannot change the migration
            # speed. So we should wait for it.
            if migrations[0].disk_remaining_bytes != 0:
                logger.debug("Flows are present, but disk migration is not"
                             " finished yet, checking again in 10s.")
                return 10.0
            logger.debug("Flows are present and local disks are migrated,"
                         " restoring full migration speed")
            # set_migration_speed must be called after the memory migration
            # has been started. If we set the migration speed before the actual
            # memory-sync happens (memory_processed_bytes increases), virsh
            # will accept it but ignore it for the memory migration.
            # Since we don't reliably know the exact beginning,
            # we will set the migration speed on each iteration until
            # memory_processed_bytes is not 0.
            await set_migration_speed(
                api_client, namespace, src_host, server.instance_name, 0
            )
            if migrations[0].memory_processed_bytes != 0:
                return None
        else:
            logger.debug("Flows are not present.")
        logger.debug("Migration still in progress, checking again in 10s.")
        return 10.0


@action
def do_live_migrate_server(
        openstack_client: openstack.connection.Connection,
        server_id: str,
        release: str,
        k8s_client: kubernetes_asyncio.client.ApiClient
        ) -> None:
    """
    As there is a bug in the l2 agent, it can take a long time to plug the
    ports in the l2 agent. This leads to the vm being unreachable for from the
    time the migration is completed until the port is plugged. If an affected
    version of openstack is used, the poll_migration function is used that
    prevents the downtime.
    However this whole issue only applies to ports that have port-security
    enabled. If a vm has no such ports we can safely use the normal migration
    """
    if release in POLL_MIGRATION_RELEASES and any(
            [port.is_port_security_enabled for port
                in openstack_client.network.ports(device_id=server_id)]):
        server = openstack_client.compute.get_server(server_id)
        src_host = server.hypervisor_hostname

        while True:
            result = asyncio.run(poll_migration(openstack_client, k8s_client,
                                                server_id, src_host, "yaook"))
            if result is None:
                return None
            time.sleep(result)
    else:
        # This one raises errors correctly.
        openstack_client.compute.live_migrate_server(
            server_id,
            host=None,
            force=False)


@action
def do_offline_migrate_server(
        client: openstack.connection.Connection,
        server_id: str,
        _release: str,
        _k8s_client: kubernetes_asyncio.client.ApiClient
        ) -> None:
    # This one also raises correctly.

    client.compute.migrate_server(server_id)


@action
def do_confirm_server(
        client: openstack.connection.Connection,
        server_id: str,
        _release: str,
        _k8s_client: kubernetes_asyncio.client.ApiClient
        ) -> None:
    os_resource.raise_response_error_if_any(
        client.compute.post(
            f"/servers/{server_id}/action",
            json={"confirmResize": None},
        )
    )


@action
def do_offload_server(
        client: openstack.connection.Connection,
        server_id: str,
        _release: str,
        _k8s_client: kubernetes_asyncio.client.ApiClient
        ) -> None:
    os_resource.raise_response_error_if_any(
        client.compute.post(
            f"/servers/{server_id}/action",
            json={"shelveOffload": None},
        )
    )


@action
def do_force_delete_server(
        client: openstack.connection.Connection,
        server_id: str,
        _release: str,
        _k8s_client: kubernetes_asyncio.client.ApiClient
        ) -> None:
    client.compute.delete_server(server_id, force=True)


@action
def do_evacuate_server(
        client: openstack.connection.Connection,
        server_id: str,
        _release: str,
        _k8s_client: kubernetes_asyncio.client.ApiClient
        ) -> None:
    os_resource.raise_response_error_if_any(
        client.compute.post(
            f"/servers/{server_id}/action",
            json={"evacuate": {}},
        )
    )


@action
def do_activate_server(
        client: openstack.connection.Connection,
        server_id: str,
        _release: str,
        _k8s_client: kubernetes_asyncio.client.ApiClient
        ) -> None:
    os_resource.raise_response_error_if_any(
        client.compute.post(
            f"/servers/{server_id}/action",
            json={"os-resetState": {"state": "active"}},
        )
    )


@action
def do_revert_server(
        client: openstack.connection.Connection,
        server_id: str,
        _release: str,
        _k8s_client: kubernetes_asyncio.client.ApiClient
        ) -> None:
    os_resource.raise_response_error_if_any(
        client.compute.post(
            f"/servers/{server_id}/action",
            json={"revertResize": None},
        )
    )


class ActionSpec(typing.NamedTuple):
    server_id: str
    action: ActionProtocol
    cost: float
    needs_confirm: bool = False
    causes_local_rebuild: bool = False


class Evictor(eviction.Evictor[EvictionStatus]):
    """
    Clear out a compute node completely and without loss of data.

    An eviction can happen in three different ways:

    - Live migration: This is preferred, because it happens nearly without
      disruption of the data plane.

    - Offline migration: This is used for cases where live migration is not
      possible, for example instances which are shut off.

    - Evacuation: This can only be done if the compute node has been "fenced"
      ("forced-down" in OpenStack API speak), otherwise the API will also
      refuse doing this.

      Quote from the OS API:

        forced_down is a manual override to tell nova that the service in
        question has been fenced manually by the operations team (either hard
        powered off, or network unplugged). That signals that it is safe to
        proceed with evacuate or other operations that nova has safety checks
        to prevent for hosts that are up.

        .. warning:: Warning

            Setting a service forced down without completely fencing it will
            likely result in the corruption of VMs on that host.

      The fencing needs to be done by a cloud operator or by the nova operator
      based on external knowledge.

    The key point of the eviction is that it MUST NOT complete before all
    instances have been moved away. At the same time, it MUST NOT cause data
    loss.

    This means that there are situations where the evictor has to enter a
    loop which requires external intervention in order to avoid violating
    either of those criteria: If an instance cannot be migrated away (for
    example because there are not enough resources available on other enabled
    compute nodes), it cannot complete.

    The evictor will operate based on the following logic:

    - Run in a loop. On each iteration:

      - Mark the compute service as disabled.
      - Enumerate all instances on the service.
      - If there are no instances, exit with success.
      - If the compute service is `up`, live and offline migrate all instances
        as appropriate.
      - If the compute service is `down` or deleted(!), evacuate all instances.

      Errors during migration attempts are logged, but otherwise ignored and
      retried during the next execution.

    As evacuation is blocked by the OpenStack API unless the service has been
    set to be forced-down by an operator, this process is safe; to unblock
    evacuation, a conscious (by a human operator) or backed-up-by-facts (by
    the nova operator process) decision has had to be made.

    One caveat: The evictor will execute changes against instances if they are
    absolutely required in order to complete the migration. The most notable
    example is the VERIFY_RESIZE state which requires action in order to
    proceed. This action is taken unconditionally.
    """
    def __init__(
            self,
            *,
            max_parallel_migrations: int,
            k8s_api_client: kubernetes_asyncio.client.ApiClient,
            release: str,
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._max_parallel_migrations = max_parallel_migrations

        self._final_warning: typing.MutableSet[str] = set()
        self._confirm_needed: typing.MutableSet[str] = set()
        self._protected_rebuild: typing.MutableSet[str] = set()
        self.k8s_client = k8s_api_client
        self.release = release

    def _connect(self) -> openstack.connection.Connection:
        client = openstack.connect(**self._connection_info)
        client.compute.default_microversion = "2.60"
        return client

    def _collect_server_status(
            self,
            client: openstack.connection.Connection,
            status_map: typing.Mapping[str, ServerStatusClass],
            ) -> ServerStatusMap:
        result: typing.Dict[
            ServerStatusClass,
            typing.List[openstack.compute.v2.server.Server]
        ] = {
            class_: []
            for class_ in ServerStatusClass
        }

        for server in client.compute.servers(host=self._node_name,
                                             all_tenants=True):
            class_ = status_map.get(server.status,
                                    ServerStatusClass.UNHANDLEABLE)
            result[class_].append(server)

        return result

    def _compute_actions(
            self,
            status: ServerStatusMap,
            ) -> typing.List[ActionSpec]:
        result = [
            ActionSpec(server.id, do_live_migrate_server, 1.0)
            for server in status[ServerStatusClass.MIGRATABLE]
        ]
        result.extend(
            ActionSpec(server.id, do_offline_migrate_server, 1.0,
                       needs_confirm=True)
            for server in status[ServerStatusClass.OFFLINE_MIGRATABLE]
        )
        result.extend(
            ActionSpec(server.id, do_force_delete_server, 0.5)
            for server in status[ServerStatusClass.FORCE_DELETABLE]
        )
        result.extend(
            ActionSpec(server.id, do_offload_server, 0.75)
            for server in status[ServerStatusClass.OFFLOADABLE]
        )
        result.extend(
            ActionSpec(server.id, do_confirm_server, 0.25)
            for server in status[ServerStatusClass.CONFIRMABLE]
        )
        result.extend(
            ActionSpec(server.id, do_activate_server, 0.1)
            for server in status[ServerStatusClass.ACTIVATABLE]
            if server.status != "REBUILD" or
            server.id not in self._protected_rebuild
        )
        result.extend(
            ActionSpec(server.id, do_evacuate_server, 0.2,
                       causes_local_rebuild=True)
            for server in status[ServerStatusClass.EVACUATABLE]
        )
        return result

    def _start_actions(
            self,
            client: openstack.connection.Connection,
            actions: typing.Sequence[ActionSpec],
            max_cost: float,
            ) -> int:
        started = 0
        cost = 0.0
        for action in actions:
            if cost + action.cost > max_cost:
                continue
            self._logger.debug("starting %s (cost=%.2f, budget_left=%.2f) for "
                               "server %s",
                               action.action.__name__,
                               action.cost,
                               max_cost - cost,
                               action.server_id)
            attempt_key = action.server_id, action.action
            self._attempt_counter[attempt_key] += 1
            nattempt = self._attempt_counter[attempt_key]
            if nattempt % 3 == 0:
                self._logger.warning(
                    "%s: is failing to be moved (this is attempt #%d)",
                    action.server_id,
                    nattempt,
                )
            try:
                action.action(client, action.server_id,
                              self.release, self.k8s_client)
            except openstack.exceptions.HttpException as exc:
                self._logger.warning(
                    "failed to start action %s for instance %s: %s",
                    action.action.__name__, action.server_id, exc,
                )
                continue
            if action.needs_confirm:
                self._confirm_needed.add(action.server_id)
            if action.causes_local_rebuild:
                self._protected_rebuild.add(action.server_id)
            cost += action.cost
            started += 1
        return started

    def _confirm_resize(
            self,
            client: openstack.connection.Connection,
            ) -> None:
        for server_id in list(self._confirm_needed):
            server = client.compute.get_server(server_id)
            status = STATUS_MAP.get(server.status)
            if status == ServerStatusClass.CONFIRMABLE:
                # Note that we do not discard right away. I do not trust this
                # API to return all errors. We only discard the server if it
                # enters a non-confirmable and non-migrating state :).
                self._logger.info(
                    "confirming %s after offline migration",
                    server_id,
                )
                try:
                    client.compute.confirm_server_resize(server_id)
                except openstack.exceptions.HttpException as exc:
                    self._logger.warning(
                        "failed to confirm %s after offline migration (will "
                        "retry): %s",
                        server_id, str(exc),
                    )
            elif status == ServerStatusClass.MIGRATING:
                self._logger.debug(
                    "waiting for %s to complete offline migration",
                    server_id,
                )
                continue
            else:
                self._logger.info(
                    "%s completed offline migration (status=%s), removing "
                    "from watchlist",
                    server_id, server.status,
                )
                self._confirm_needed.discard(server_id)

    def _dump_status(
            self,
            status: ServerStatusMap,
            ) -> None:
        if not any(status.values()):
            self._logger.debug("no servers in server status map")
            return

        self._logger.debug("server status map (empty sets omitted):")
        for k, servers in status.items():
            if not servers:
                continue
            self._logger.debug(
                "  %s: %s",
                k.name,
                ", ".join(f"{server.id} ({server.status})"
                          for server in servers)
            )

    def _iterate(
            self,
            client: openstack.connection.Connection,
            status_map: typing.Mapping[str, ServerStatusClass],
            ) -> EvictionStatus:
        status = self._collect_server_status(client, status_map)
        self._confirm_resize(client)
        self._dump_status(status)

        actions = self._compute_actions(status)
        random.shuffle(actions)
        in_progress = len(status[ServerStatusClass.MIGRATING])
        max_new = self._max_parallel_migrations - in_progress
        in_progress += self._start_actions(client, actions,
                                           max_new)

        unhandleable = status[ServerStatusClass.UNHANDLEABLE]
        if unhandleable:
            self._logger.warning(
                "%d server(s) are not handleable: %s",
                len(unhandleable),
                ", ".join(f"{server.id} ({server.status})"
                          for server in unhandleable),
            )

        return EvictionStatus(
            actions_in_progress=in_progress + len(self._confirm_needed),
            migratable_instances=(
                len(status[ServerStatusClass.MIGRATABLE]) +
                len(status[ServerStatusClass.OFFLINE_MIGRATABLE]) +
                len(status[ServerStatusClass.MIGRATING]) +
                len(status[ServerStatusClass.ACTIVATABLE]) +
                len(status[ServerStatusClass.EVACUATABLE]) +
                len(status[ServerStatusClass.CONFIRMABLE]) +
                len(status[ServerStatusClass.OFFLOADABLE])
            ),
            pending_instances=len(status[ServerStatusClass.PENDING]),
            unhandleable_instances=len(status[ServerStatusClass.UNHANDLEABLE]),
        )

    async def _is_node_down(self, node_name: str) -> bool:
        is_node_down = False
        async with kubernetes_asyncio.client.ApiClient(
                configuration=self.k8s_client.configuration) as api_client:
            try:
                self._logger.info(
                    "check if node is alive"
                )
                v1 = kubernetes_asyncio.client.CoreV1Api(api_client)
                node = await v1.read_node(node_name)
                status = ''
                for condition in node.status.conditions:
                    if condition.type == 'Ready':
                        status = condition.status
                        break
                if status != 'True':
                    self._logger.warning(
                        "node %s not in Ready state. Actual State: %s",
                        node_name, status
                        )
                    ping_result = os.system("ping -c 5 " + node_name)
                    if ping_result != 0:
                        self._logger.error(
                            "node %s not pingable",
                            node_name,
                        )
                        is_node_down = True
                    else:
                        self._logger.info(
                            "node %s is pingable",
                            node_name
                        )
            except kubernetes_asyncio.client.ApiException as exc:
                self._logger.error(
                    "Kubernets API Exception: %s",
                    exc.status,
                )
                if exc.status == 404:
                    is_node_down = True
                else:
                    raise

        return is_node_down

    def _os_iterate(self) -> EvictionStatus:
        client = self._connect()

        self.is_forced_down: bool
        try:
            service = ComputeServiceInfo.get(
                client.compute,
                host=self._node_name,
                binary="nova-compute",
            )
            is_node_down = asyncio.run(
                self._is_node_down(self._node_name)
            )

        except LookupError:
            self._logger.warning(
                "compute service %s has vanished ... treating as forced down",
                self._node_name,
            )
            # This is a very unpleasant situation; it is not clear if we can
            # salvage anything, but we still have to try.
            self.is_forced_down = True
            is_down = True
        else:
            service.disable(
                client.compute,
                f"nova-compute-operator: {self._reason}",
            )
            if is_node_down and not service.is_up:
                self.is_forced_down = True
            else:
                self.is_forced_down = service.is_forced_down
            is_down = not service.is_up
            self._logger.info(
                "compute service %s: is_down=%s, is_forced_down=%s",
                service.id_, is_down, self.is_forced_down,
            )

        if self.is_forced_down:
            status_map = FORCED_DOWN_STATUS_MAP
        elif is_down:
            self._logger.warning(
                "compute service %s is down, but not forced down. to prevent "
                "corruption of instances, we do not do anything!",
                service.id_,
            )
            status_map = DOWN_STATUS_MAP
        else:
            status_map = STATUS_MAP

        return self._iterate(client, status_map)

    async def is_migration_done(
            self,
            status: EvictionStatus,
            ) -> bool:

        migration_finished = (status.migratable_instances +
                              status.pending_instances +
                              status.actions_in_progress +
                              status.pending_instances +
                              status.unhandleable_instances) == 0

        if migration_finished:
            if not self.is_forced_down:
                virsh_status = await check_virsh_is_emtpy(
                    self._logger,
                    self.k8s_client,
                    self._namespace,
                    self._node_name)
            else:
                virsh_status = True

            if virsh_status is False:
                self._logger.warning(
                    "Virsh says: Nova api is a liar, migration is NOT "
                    "finished. There are still VMs on the hypervisor! This "
                    "can happen, when a cellX DB is not working. The job will "
                    "not terminate until virsh matches the state of the nova "
                    "api")
                return False
            if virsh_status is None:
                self._logger.warning(
                    "Unable to check via virsh if the all VMs are gone. To "
                    "make sure that the NovaComputeNode wont't be deleted "
                    "until the node is actually empty, the job won't "
                    "terminate.")
                return False
        return migration_finished

    def start_eviction_log(
            self,
            poll_interval: int,
            ) -> str:
        return (f"initiating eviction of node {self._node_name} with reason "
                f"{self._reason}. up to {self._max_parallel_migrations} in "
                f"parallel, poll interval is {poll_interval}s")
