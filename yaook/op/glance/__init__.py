#!/usr/bin/env python3
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import yaook
import yaook.common.config
import yaook.op.common
import yaook.op.tasks
import yaook.statemachine as sm
from yaook.statemachine import resources


def _internal_endpoint_configured(ctx: sm.Context) -> bool:
    return ctx.parent_spec["api"].get("internal", {}) != {}


def _use_ceph(ctx: sm.Context) -> bool:
    return "ceph" in ctx.parent_spec.get("backends", {})


def _use_file(ctx: sm.Context) -> bool:
    return "file" in ctx.parent_spec.get("backends", {})


def _use_s3(ctx: sm.Context) -> bool:
    return "s3" in ctx.parent_spec.get("backends", {})


class CephConfigLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        if not _use_ceph(ctx):
            return {}

        if _use_file(ctx) or _use_s3(ctx):
            raise sm.ConfigurationInvalid(
                "Choose either 'ceph', 'file' or s3 as a storage backend. "
                "Using more than a single backend is currently not supported.")

        cephconfig = ctx.parent_spec["backends"]["ceph"]
        return {
            "glance": yaook.common.config.OSLO_CONFIG.declare([
                {
                    "glance_store": yaook.common.config.CueConfigReference(
                        "glance.glance_store_ceph_spec"
                    ),
                },
                {
                    "glance_store": {
                        "rbd_store_user": cephconfig["keyringUsername"],
                        "rbd_store_pool": cephconfig["keyringPoolname"],
                    },
                },
            ]),
            "ceph": yaook.common.config.CEPH_CONFIG.declare([
                cephconfig.get("cephConfig", {}),
                {
                    f"client.{cephconfig['keyringUsername']}": {
                        "keyfile": "/etc/ceph/keyfile"
                    },
                },
            ]),
        }


class FileConfigLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        if not _use_file(ctx):
            return {}

        if _use_ceph(ctx) or _use_s3(ctx):
            raise sm.ConfigurationInvalid(
                "Choose either 'ceph', 'file' or s3 as a storage backend. "
                "Using more than a single backend is currently not supported.")

        return {
            "glance": yaook.common.config.OSLO_CONFIG.declare([
                {
                    "glance_store": yaook.common.config.CueConfigReference(
                        "glance.glance_store_file_spec"
                    ),
                },
            ]),
        }


class S3ConfigLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        if not _use_s3(ctx):
            return {}

        if _use_ceph(ctx) or _use_file(ctx):
            raise sm.ConfigurationInvalid(
                "Choose either 'ceph', 'file' or s3 as a storage backend. "
                "Using more than a single backend is currently not supported.")

        secrets = sm.interfaces.secret_interface(ctx.api_client)

        s3config = ctx.parent_spec["backends"]["s3"]

        credentials = sm.api_utils.decode_secret_data(
            (await secrets.read(ctx.namespace,
                                s3config["credentialRef"]["name"])).data
        )

        return {
            "glance": yaook.common.config.OSLO_CONFIG.declare([
                {
                    "glance_store": yaook.common.config.CueConfigReference(
                        "glance.glance_store_s3_spec"
                    ),
                },
                {
                    "glance_store": {
                        "s3_store_host": s3config["endpoint"],
                        "s3_store_bucket": s3config["bucket"],
                        "s3_store_bucket_url_format": s3config["addressingStyle"],  # noqa 501
                        "s3_store_access_key": credentials["access"],
                        "s3_store_secret_key": credentials["secret"],
                    },
                },
            ]),
        }


class Deployment(sm.TemplatedDeployment):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap
            ) -> resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        param["use_ceph"] = _use_ceph(ctx)
        param["use_file"] = _use_file(ctx)
        return param


JOB_SCHEDULING_KEYS = [
    yaook.op.common.SchedulingKey.OPERATOR_GLANCE.value,
    yaook.op.common.SchedulingKey.OPERATOR_ANY.value,
]


DATABASE_NAME = "glance"
API_SVC_USER_NAME = "api"


class Glance(sm.ReleaseAwareCustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "glancedeployments"
    KIND = "GlanceDeployment"
    RELEASES = [
        "train",
        "ussuri",
        "victoria",
        "wallaby",
        "xena",
        "yoga"
    ]
    VALID_UPGRADE_TARGETS = [
        "ussuri",
        "victoria",
        "wallaby",
        "xena",
        "yoga"
    ]

    glance_docker_image = yaook.op.common.image_dependencies(
        "glance/glance-{release}",
        RELEASES,
    )
    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )
    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload/service-reload',
        sm.YaookSemVerSelector(),
    )
    mariadb_version = sm.MappedVersionedDependency(
        mapping={
            release: "10.6"
            for release in [
                "train",
                "ussuri",
                "victoria",
                "wallaby",
                "xena",
                "yoga",
            ]
        },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx),
    )
    memcached_image = sm.VersionedDockerImage(
        "bitnami/memcached",
        sm.BitnamiVersionSelector([
            ([">=1.6.10", "<2.0.0"], []),
        ]),
    )

    keystone = sm.KeystoneReference()
    keystone_internal_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )

    db = sm.TemplatedMySQLService(
        template="database.yaml",
        params={
            "db_name": DATABASE_NAME,
        },
        versioned_dependencies=[mariadb_version],
    )
    db_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=db,
        foreign_component=yaook.op.common.MYSQL_DATABASE_SERVICE_COMPONENT,
    )

    db_api_user_password = sm.AutoGeneratedPassword(
        metadata=("glance-db-api-user-", True),
        copy_on_write=True,
    )
    db_api_user = sm.SimpleMySQLUser(
        metadata=("glance-api-", True),
        database=db,
        username=API_SVC_USER_NAME,
        password_secret=db_api_user_password,
    )

    memcached = sm.TemplatedMemcachedService(
        template="memcached.yaml",
        versioned_dependencies=[memcached_image],
    )
    memcached_statefulset = sm.ForeignResourceDependency(
        resource_interface_factory=sm.stateful_set_interface,
        foreign_resource=memcached,
        foreign_component=yaook.op.common.MEMCACHED_STATEFUL_COMPONENT,
    )

    keystone_user = sm.StaticKeystoneUser(
        keystone=keystone,
        username="glance",
    )
    keystone_user_credentials = \
        yaook.op.common.keystone_user_credentials_reference(
            keystone_user,
        )
    keystone_endpoint = sm.Optional(
        condition=yaook.op.common.publish_endpoint,
        wrapped_state=sm.TemplatedKeystoneEndpoint(
            template="glance-keystone-endpoint.yaml",
            add_dependencies=[keystone],
        )
    )

    certificate_secret = sm.EmptyTlsSecret(
        metadata=("glance-api-certificate-", True),
    )
    certificate = sm.TemplatedCertificate(
        template="glance-api-certificate.yaml",
        add_dependencies=[certificate_secret],
    )
    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )
    ca_certs = sm.CAConfigMap(
        metadata=("glance-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            ready_certificate_secret,
        ],
    )

    config = sm.CueSecret(
        metadata=("glance-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="glance",
                accessor="glanceConfig",
            ),
            sm.SecretInjectionLayer(
                target="glance",
                accessor=lambda ctx: ctx.parent_spec.get("glanceSecrets", []),
            ),
            sm.KeystoneAuthLayer(
                target="glance",
                credentials_secret=keystone_user_credentials,
                endpoint_config=keystone_internal_api,
            ),
            sm.MemcachedConnectionLayer(
                target="glance",
                memcached_sfs=memcached_statefulset,
            ),
            sm.DatabaseConnectionLayer(
                target="glance",
                service=db_service,
                database_name=DATABASE_NAME,
                username=API_SVC_USER_NAME,
                password_secret=db_api_user_password,
                config_section="database",
            ),
            CephConfigLayer(),
            FileConfigLayer(),
            S3ConfigLayer(),
        ],
    )

    image_volume = sm.Optional(
        condition=_use_file,
        wrapped_state=sm.TemplatedPersistentVolumeClaim(
            template="glance-pvc-image-volume.yaml"
        )
    )

    db_sync = sm.Optional(
        condition=sm.optional_non_upgrade(),
        wrapped_state=sm.TemplatedJob(
            template="glance-job-db-sync.yaml",
            scheduling_keys=JOB_SCHEDULING_KEYS,
            add_dependencies=[config, ca_certs],
            versioned_dependencies=[glance_docker_image],
        ),
    )
    db_load_metadefs = sm.Optional(
        condition=sm.optional_non_upgrade(),
        wrapped_state=sm.TemplatedJob(
            template="glance-job-db-load-metadefs.yaml",
            scheduling_keys=JOB_SCHEDULING_KEYS,
            add_dependencies=[config, db_sync, ca_certs],
            versioned_dependencies=[glance_docker_image],
        ),
    )
    db_upgrade_pre = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.TemplatedJob(
            template="glance-job-db-upgrade-pre.yaml",
            scheduling_keys=JOB_SCHEDULING_KEYS,
            add_dependencies=[config, ca_certs],
            versioned_dependencies=[glance_docker_image],
        ),
    )

    glance_policy = sm.PolicyConfigMap(
        metadata=("glance-policy-", True),
        policy_spec_key="policy",
        copy_on_write=True,
    )

    internal_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx:
        ctx.parent_spec["api"].get("internal", {}).get("ingress", {}).get(
            "externalCertificateSecretRef", {}).get("name"),
        secret_reference=ready_certificate_secret
    )

    external_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx:
        ctx.parent_spec["api"]["ingress"].get("externalCertificateSecretRef",
                                              {}).get("name"),
        secret_reference=ready_certificate_secret
    )

    api_deployment = Deployment(
        template="glance-deployment-api.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.IMAGE_API.value,
            yaook.op.common.SchedulingKey.ANY_API.value,
        ],
        add_dependencies=[
            config, db_sync, db_load_metadefs, ca_certs,
            ready_certificate_secret, image_volume,
            glance_policy, db_upgrade_pre,
            external_certificate_secret,
            internal_certificate_secret,
        ],
        versioned_dependencies=[
            glance_docker_image,
            ssl_terminator_image,
            service_reload_image,
        ],
    )
    api_deployment_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("glance-api-pdb-", True),
         replicated=api_deployment,
    )

    api_service = sm.TemplatedService(
        template="glance-api-service.yaml",
        add_dependencies=[api_deployment],
    )

    internal_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-internal-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=ready_certificate_secret,
        endpoints=["internal-ssl-terminator-prometheus"],
    )

    external_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-external-ssl-service-monitor-",
            True),
        service=api_service,
        certificate=external_certificate_secret,
        server_name_provider=lambda ctx: (
            ctx.parent_spec["api"]["ingress"]["fqdn"]
        ),
        endpoints=["external-ssl-terminator-prometheus"],
    )

    internal_ingress_ssl_service_monitor = sm.Optional(
        condition=_internal_endpoint_configured,
        wrapped_state=sm.GeneratedServiceMonitor(
            metadata=lambda ctx: (
                f"{ctx.parent_name}-internal-ingress-ssl-service-monitor-",
                True),
            service=api_service,
            certificate=internal_certificate_secret,
            server_name_provider=lambda ctx: (
                ctx.parent_spec["api"]["internal"]["ingress"]["fqdn"]
            ),
            endpoints=["internal-ingress-ssl-terminator-prometheus"],
        )
    )

    api_ingress = sm.TemplatedIngress(
        template="glance-api-ingress.yaml",
        add_dependencies=[api_service],
    )

    internal_api_ingress = sm.Optional(
        condition=_internal_endpoint_configured,
        wrapped_state=sm.TemplatedIngress(
            template="glance-internal-api-ingress.yaml",
            add_dependencies=[api_service],
        )
    )

    db_upgrade_post = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.TemplatedJob(
            template="glance-job-db-upgrade-post.yaml",
            scheduling_keys=JOB_SCHEDULING_KEYS,
            add_dependencies=[api_deployment, config, ca_certs],
            versioned_dependencies=[glance_docker_image],
        ),
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


sm.register(Glance)
