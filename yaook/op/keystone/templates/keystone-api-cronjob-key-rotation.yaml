##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  generateName: "keystone-key-rotation-"
spec:
  schedule: {{ crd_spec.keyRotationSchedule }}
  concurrencyPolicy: Forbid
  jobTemplate:
    spec:
      template:
        spec:
          restartPolicy: Never
          serviceAccountName: {{ dependencies['credential_management_service_account'].resource_name() }}
          containers:
            - name: fernet-rotation
              image:  {{ versioned_dependencies['keystone_docker_image'] }}
              command: ["/opt/secretsmanage.py", "fernet_rotate", "keystone"]
              env:
                - name: KUBERNETES_NAMESPACE
                  value: {{namespace}}
                - name: SECRET_NAME
                  value: {{ dependencies['fernet_keys'].resource_name() }}
                - name: KEYSTONE_KEYS_REPOSITORY
                  value: /etc/keystone/fernet-keys/
                - name: REQUESTS_CA_BUNDLE
                  value: /etc/pki/tls/certs/ca-bundle.crt
              volumeMounts:
                - name: keystone-config-volume
                  mountPath: /etc/keystone
                - name: fernet-keys
                  mountPath: /etc/keystone/fernet-keys
                - name: ca-certs
                  mountPath: /etc/pki/tls/certs
              resources: {{ crd_spec | resources('job.keystone-key-rotation-fernet-cronjob') }}
            - name: credential-rotation
              image:  {{ versioned_dependencies['keystone_docker_image'] }}
              command: ["/opt/secretsmanage.py", "credential_rotate", "keystone"]
              env:
                - name: KUBERNETES_NAMESPACE
                  value: {{namespace}}
                - name: SECRET_NAME
                  value: {{ dependencies['credential_keys'].resource_name() }}
                - name: KEYSTONE_KEYS_REPOSITORY
                  value: /etc/keystone/credential-keys/
                - name: REQUESTS_CA_BUNDLE
                  value: /etc/pki/tls/certs/ca-bundle.crt
              volumeMounts:
                - name: keystone-config-volume
                  mountPath: /etc/keystone
                - name: credential-keys
                  mountPath: /etc/keystone/credential-keys
                - name: fernet-keys-read-only
                  mountPath: /etc/keystone/fernet-keys
                - name: ca-certs
                  mountPath: /etc/pki/tls/certs
              resources: {{ crd_spec | resources('job.keystone-key-rotation-credential-cronjob') }}
          volumes:
            - name: keystone-config-volume
              secret:
                secretName: {{ dependencies['config'].resource_name() }}
                items:
                  - key: keystone.conf
                    path: keystone.conf
            - name: fernet-keys
              emptyDir: {}
            # The Keys from the Secret are needed, for the credential key rotation to be successfull
            - name: fernet-keys-read-only
              secret:
                secretName: {{ dependencies['fernet_keys'].resource_name() }}
            - name: credential-keys
              emptyDir: {}
            - name: ca-certs
              configMap:
                name: {{ dependencies['ca_certs'].resource_name() }}
    {% if crd_spec.imagePullSecrets | default(False) %}
          imagePullSecrets: {{ crd_spec.imagePullSecrets }}
    {% endif %}
