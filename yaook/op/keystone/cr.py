#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import typing
import secrets

import kubernetes_asyncio.client as kclient

import yaook.op.common
import yaook.statemachine as sm
import yaook.statemachine.resources


def _internal_endpoint_configured(ctx):
    return ctx.parent_spec["api"].get("internal", {}) != {}


class EmptySecret(sm.Secret):
    def __init__(self, *, name_prefix=None, **kwargs):
        super().__init__(**kwargs)
        self._name_prefix = name_prefix

    def __set_name__(self, owner, name):
        super().__set_name__(owner, name)
        if self._name_prefix is None:
            self._name_prefix = self.component.lower().replace("_", "-") + "-"

    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> sm.ResourceBody:
        return {
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": {
                "generateName": self._name_prefix,
            },
            "type": "Opaque",
            "data": {}
        }

    def _needs_update(self,
                      current: kclient.V1Secret,
                      new: typing.Mapping) -> bool:
        return False


class AdminCredential(sm.Secret):
    def __init__(
            self,
            metadata: sm.MetadataProvider,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata_provider = metadata

    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> sm.ResourceBody:
        return {
            "apiVersion": "v1",
            "kind": "Secret",
            "type": "Opaque",
            "metadata": sm.evaluate_metadata(ctx, self._metadata_provider),
            "data": sm.api_utils.encode_secret_data({
                "OS_PASSWORD": secrets.token_urlsafe(32),
                "OS_USERNAME": "yaook-sys-maint",
                "OS_PROJECT_NAME": "admin",
                "OS_USER_DOMAIN_NAME": "Default",
                "OS_PROJECT_DOMAIN_NAME": "Default",
                "OS_AUTH_TYPE": "password",
            }),
        }

    def _needs_update(
            self,
            current: kclient.V1Secret,
            new: typing.Mapping) -> bool:
        # We need this here because else the operator will rewrite the
        # password constantly
        return False


class ExternalAdminCredential(sm.Secret):
    def __init__(
            self,
            metadata: sm.MetadataProvider,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata_provider = metadata

    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> sm.ResourceBody:
        password = await sm.extract_password(
            ctx,
            ctx.parent_spec["password"]["name"]
        )
        return {
            "apiVersion": "v1",
            "kind": "Secret",
            "type": "Opaque",
            "metadata": sm.evaluate_metadata(ctx, self._metadata_provider),
            "data": sm.api_utils.encode_secret_data({
                "OS_PASSWORD": password,
                "OS_USERNAME": ctx.parent_spec["username"],
                "OS_PROJECT_NAME": ctx.parent_spec["projectName"],
                "OS_USER_DOMAIN_NAME": ctx.parent_spec["userDomainName"],
                "OS_PROJECT_DOMAIN_NAME": ctx.parent_spec["projectDomainName"],
                "OS_AUTH_TYPE": ctx.parent_spec["authType"],
            }),
        }


class APIConfigmapTemplate(sm.TemplatedConfigMap):
    def __init__(
            self,
            **kwargs: typing.Any):
        super().__init__(**kwargs)

    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap
            ) -> yaook.statemachine.resources.TemplateParameters:
        params = await super()._get_template_parameters(ctx, dependencies)
        return params


class TemplatedKeystoneConfigMap(sm.TemplatedConfigMap):
    """
    Manage a jinja2-templated KeystoneConfigMap.

    :param memcached_sfs: Reference to the Kubernetes StatefulSet under which
        the memcached is reachable.

    .. seealso::

        :class:`~.TemplatedConfigMapState`:
            for arguments related to templating.
    """
    def __init__(
            self,
            *,
            memcached_sfs: sm.resources.KubernetesReference[
                kclient.V1StatefulSet],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(memcached_sfs)
        self._memcached_sfs = memcached_sfs

    async def _get_template_parameters(
        self,
        ctx: sm.Context,
        dependencies: sm.DependencyMap
    ) -> sm.resources.TemplateParameters:
        memcached_sfs = await self._memcached_sfs.get(ctx)
        memcached_port = 11211
        memcached_servers = sm.MemcachedConnectionLayer.\
            _generate_memcached_addresses(
                memcached_sfs.name,
                memcached_port,
                ctx.parent_spec["memcached"]["replicas"],
                ctx.namespace,
            )

        template_parameters = await super()._get_template_parameters(
            ctx,
            dependencies
        )

        template_parameters.update(
            {'memcached_servers': ','.join(memcached_servers)}
        )
        return template_parameters


JOB_SCHEDULING_KEYS = [
    yaook.op.common.SchedulingKey.OPERATOR_KEYSTONE.value,
    yaook.op.common.SchedulingKey.OPERATOR_ANY.value,
]

SERVICE_NAME = "keystone"
API_SVC_USERNAME = "api"


class KeystoneDeployment(sm.ReleaseAwareCustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "keystonedeployments"
    KIND = "KeystoneDeployment"
    RELEASES = [
        "train",
        "ussuri",
        "victoria",
        "wallaby",
        "xena",
        "yoga",
    ]
    VALID_UPGRADE_TARGETS = [
        "ussuri",
        "victoria",
        "wallaby",
        "xena",
        "yoga",
    ]

    keystone_docker_image = yaook.op.common.image_dependencies(
        "keystone/keystone-{release}",
        RELEASES,
    )
    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload/service-reload',
        sm.YaookSemVerSelector(),
    )
    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator/ssl-terminator',
        sm.YaookSemVerSelector(),
    )
    mariadb_version = sm.MappedVersionedDependency(
        mapping={
            release: "10.6"
            for release in [
                "train",
                "ussuri",
                "victoria",
                "wallaby",
                "xena",
                "yoga",
            ]
        },
        targetfn=lambda ctx: sm.version_utils.get_target_release(ctx),
    )

    memcached_image = sm.VersionedDockerImage(
        "bitnami/memcached",
        sm.BitnamiVersionSelector([
            ([">=1.6.10", "<2.0.0"], []),
        ]),
    )

    keystone_policy = sm.Orphan(
        resource_interface_factory=sm.interfaces.config_map_interface,
    )

    new_keystone_policy = sm.ReleaseAwarePolicyConfigMap(
        metadata=("keystone-policy-", True),
        component=yaook.op.common.POLICY_CONFIGMAP_COPMONENT,
        copy_on_write=True,
        versioned_dependencies=[
            keystone_docker_image,
        ]
    )

    ready_keystone_policy = sm.ReadyPolicyConfigMapReference(
        configmap_reference=new_keystone_policy
    )

    policy_validation_management_role = sm.TemplatedRole(
        template="common-policy-validation-role.yaml",
        params={
            "name": SERVICE_NAME,
        },
        add_dependencies=[
            new_keystone_policy
        ]
    )
    policy_validation_management_service_account = \
        sm.TemplatedServiceAccount(
            template="common-policy-validation-serviceaccount.yaml",
            component=yaook.op.common.SERVICE_ACCOUNT_COPMONENT,
            params={
                "name": SERVICE_NAME,
            },
        )
    policy_validation_management_role_binding = \
        sm.TemplatedRoleBinding(
            template="common-policy-validation-role-binding.yaml",
            params={
                "name": SERVICE_NAME,
            },
            add_dependencies=[
                policy_validation_management_role,
                policy_validation_management_service_account,
            ]
        )

    script = sm.PolicyValidationScriptConfigMap(
        metadata=("keystone-policy-validation-script-", True),
    )

    policy_validation = sm.PolicyValidator(
        template="common-policy-validator.yaml",
        params={
            "name": SERVICE_NAME,
        },
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[
            new_keystone_policy,
            script,
            policy_validation_management_service_account,
        ],
        versioned_dependencies=[
            keystone_docker_image,
        ]
    )

    db = sm.TemplatedMySQLService(
        template="database.yaml",
        params={
            "db_name": SERVICE_NAME,
        },
        versioned_dependencies=[mariadb_version],
    )

    db_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=db,
        foreign_component=yaook.op.common.MYSQL_DATABASE_SERVICE_COMPONENT,
    )

    db_api_user_password = sm.AutoGeneratedPassword(
        metadata=("keystone-api-db-user-", True),
        copy_on_write=True,
    )
    db_api_user = sm.SimpleMySQLUser(
        metadata=("keystone-api-", True),
        database=db,
        username=API_SVC_USERNAME,
        password_secret=db_api_user_password,
    )

    memcached = sm.TemplatedMemcachedService(
        template="memcached.yaml",
        versioned_dependencies=[memcached_image],
    )

    memcached_statefulset = sm.ForeignResourceDependency(
        resource_interface_factory=sm.stateful_set_interface,
        foreign_resource=memcached,
        foreign_component=yaook.op.common.MEMCACHED_STATEFUL_COMPONENT,
    )

    certificate_secret = sm.EmptyTlsSecret(
        metadata=("keystone-api-certificate-", True),
    )
    certificate = sm.TemplatedCertificate(
        template="keystone-api-certificate.yaml",
        add_dependencies=[certificate_secret],
    )
    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )
    ca_certs = sm.CAConfigMap(
        metadata=("keystone-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            ready_certificate_secret,
        ],
        component=yaook.op.common.KEYSTONE_CA_CERTIFICATES_COMPONENT,
    )

    admin_credentials = AdminCredential(
        metadata=lambda ctx: f"{ctx.parent_name}-admin",
        component=yaook.op.common.KEYSTONE_ADMIN_CREDENTIALS_COMPONENT,
    )
    config = sm.CueSecret(
        metadata=("keystone-config-", True),
        copy_on_write=True,
        add_cue_layers=[
            sm.SpecLayer(
                target="keystone",
                accessor="keystoneConfig",
            ),
            sm.SecretInjectionLayer(
                target="keystone",
                accessor=lambda ctx: ctx.parent_spec.get(
                    "keystoneSecrets", []),
            ),
            sm.DatabaseConnectionLayer(
                target="keystone",
                service=db_service,
                database_name=SERVICE_NAME,
                username=API_SVC_USERNAME,
                password_secret=db_api_user_password,
                config_section="database",
            ),
            sm.MemcachedConnectionLayer(
                target="keystone",
                config_sections=["cache"],
                memcached_sfs=memcached_statefulset
            )
        ],
    )

    credential_management_role = sm.TemplatedRole(
        template="keystone-api-role-credential-management.yaml",
    )
    credential_management_service_account = \
        sm.TemplatedServiceAccount(
            template="keystone-api-serviceaccount-credential-management.yaml",
        )
    credential_management_role_binding = \
        sm.TemplatedRoleBinding(
            template="keystone-api-role-binding-credential-management.yaml",
            add_dependencies=[
                credential_management_role,
                credential_management_service_account,
            ]
        )
    fernet_keys = EmptySecret()
    credential_keys = EmptySecret()

    db_sync = sm.Optional(
        condition=sm.optional_non_upgrade(),
        wrapped_state=sm.TemplatedJob(
            template="keystone-api-job-db-sync.yaml",
            scheduling_keys=JOB_SCHEDULING_KEYS,
            add_dependencies=[config, ca_certs],
            versioned_dependencies=[keystone_docker_image],
        ),
    )
    db_upgrade_pre = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.TemplatedJob(
            template="keystone-api-job-db-upgrade-pre.yaml",
            scheduling_keys=JOB_SCHEDULING_KEYS,
            add_dependencies=[config, ca_certs],
            versioned_dependencies=[keystone_docker_image],
        ),
    )

    fernet_setup = sm.TemplatedJob(
        template="keystone-api-job-fernet-setup.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[
            fernet_keys,
            credential_management_role_binding,
            credential_management_service_account,
            config,
            ca_certs,
        ],
        versioned_dependencies=[keystone_docker_image],
    )
    credential_setup = sm.TemplatedJob(
        template="keystone-api-job-credential-setup.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[
            credential_keys,
            credential_management_role_binding,
            credential_management_service_account,
            config,
            ca_certs,
        ],
        versioned_dependencies=[keystone_docker_image],
    )
    bootstrap = sm.TemplatedJob(
        template="keystone-api-job-bootstrap.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[
            db_sync,
            db_upgrade_pre,
            fernet_setup,
            credential_setup,
            credential_keys,
            fernet_keys,
            admin_credentials,
            config,
            ca_certs,
        ],
        versioned_dependencies=[keystone_docker_image],
    )

    external_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx:
            ctx.parent_spec["api"]["ingress"]
            .get("externalCertificateSecretRef", {}).get("name"),
        secret_reference=ready_certificate_secret
    )

    internal_certificate_secret = sm.ExternalSecretReference(
        external_secret=lambda ctx:
            ctx.parent_spec["api"]["internal"]["ingress"]
            .get("externalCertificateSecretRef", {}).get("name"),
        secret_reference=ready_certificate_secret
    )

    api = sm.TemplatedDeployment(
        template="keystone-api-deployment.yaml",
        scheduling_keys=[
            yaook.op.common.SchedulingKey.IDENTITY_API.value,
            yaook.op.common.SchedulingKey.ANY_API.value,
        ],
        add_dependencies=[
            db_sync,
            db_upgrade_pre,
            bootstrap,
            config,
            credential_keys,
            fernet_keys,
            ca_certs,
            ready_certificate_secret,
            internal_certificate_secret,
            external_certificate_secret,
            ready_keystone_policy,
        ],
        versioned_dependencies=[
            keystone_docker_image,
            service_reload_image,
            ssl_terminator_image,
        ],
    )
    api_pdb = sm.QuorumPodDisruptionBudget(
         metadata=("keystone-api-pdb-", True),
         replicated=api,
    )

    service = sm.TemplatedService(
        template="keystone-api-service.yaml",
        add_dependencies=[api],
    )

    internal_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-internal-ssl-service-monitor-",
            True),
        service=service,
        certificate=ready_certificate_secret,
        endpoints=["internal-ssl-terminator-prometheus"],
    )

    internal_ingress_ssl_service_monitor = sm.Optional(
        condition=_internal_endpoint_configured,
        wrapped_state=sm.GeneratedServiceMonitor(
            metadata=lambda ctx: (
                f"{ctx.parent_name}-internal-ingress-ssl-service-monitor-",
                True),
            service=service,
            certificate=internal_certificate_secret,
            server_name_provider=lambda ctx: (
                ctx.parent_spec["api"]["internal"]["ingress"]["fqdn"]
            ),
            endpoints=["internal-ingress-ssl-terminator-prometheus"],
        )
    )

    external_ssl_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-external-ssl-service-monitor-",
            True),
        service=service,
        certificate=external_certificate_secret,
        server_name_provider=lambda ctx: (
            ctx.parent_spec["api"]["ingress"]["fqdn"]
        ),
        endpoints=["external-ssl-terminator-prometheus"],
    )

    db_upgrade_post = sm.Optional(
        condition=sm.optional_only_upgrade(),
        wrapped_state=sm.TemplatedJob(
            template="keystone-api-job-db-upgrade-post.yaml",
            scheduling_keys=JOB_SCHEDULING_KEYS,
            add_dependencies=[api, config, ca_certs],
            versioned_dependencies=[keystone_docker_image],
        ),
    )

    internal_config = TemplatedKeystoneConfigMap(
        memcached_sfs=memcached_statefulset,
        component=yaook.op.common.KEYSTONE_INTERNAL_API_COMPONENT,
        template="internal-api.yaml",
        add_dependencies=[service],
    )
    public_config = TemplatedKeystoneConfigMap(
        memcached_sfs=memcached_statefulset,
        component=yaook.op.common.KEYSTONE_PUBLIC_API_COMPONENT,
        template="public-api.yaml",
        add_dependencies=[service],
    )
    ingress = sm.TemplatedIngress(
        template="keystone-api-ingress.yaml",
        add_dependencies=[service]
    )

    internal_ingress = sm.Optional(
        condition=_internal_endpoint_configured,
        wrapped_state=sm.TemplatedIngress(
            template="keystone-internal-api-ingress.yaml",
            add_dependencies=[service]
        )
    )

    key_rotation = sm.TemplatedCronJob(
        template="keystone-api-cronjob-key-rotation.yaml",
        scheduling_keys=JOB_SCHEDULING_KEYS,
        add_dependencies=[
            api,
            ca_certs,
            config,
            credential_keys,
            credential_management_service_account,
            credential_setup,
            db_sync,
            fernet_keys,
            fernet_setup,
        ],
        versioned_dependencies=[keystone_docker_image],
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


sm.register(KeystoneDeployment)


class ExternalKeystoneDeployment(sm.CustomResource):
    API_GROUP = "yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "externalkeystonedeployments"
    KIND = "ExternalKeystoneDeployment"

    ca_certs = sm.CAConfigMap(
        metadata=("external-keystone-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        component=yaook.op.common.KEYSTONE_CA_CERTIFICATES_COMPONENT,
    )

    admin_credentials = ExternalAdminCredential(
        metadata=("external-keystone-admin-", True),
        component=yaook.op.common.KEYSTONE_ADMIN_CREDENTIALS_COMPONENT,
    )

    internal_config = APIConfigmapTemplate(
        component=yaook.op.common.KEYSTONE_INTERNAL_API_COMPONENT,
        template="external-api.yaml",
    )
    public_config = APIConfigmapTemplate(
        component=yaook.op.common.KEYSTONE_PUBLIC_API_COMPONENT,
        template="external-api.yaml",
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


sm.register(ExternalKeystoneDeployment)
