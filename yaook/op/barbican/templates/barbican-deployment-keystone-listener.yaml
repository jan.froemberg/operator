##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "barbican-keystone-listener"
  labels:
    app: "barbican-keystone-listener"
spec:
  replicas:  {{ crd_spec.keystoneListener.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "barbican-keystone-listener"
          image:  {{ versioned_dependencies['barbican_docker_image'] }}
          imagePullPolicy: Always
          command: ["barbican-keystone-listener"]
          volumeMounts:
            - name: barbican-config-volume
              mountPath: /etc/barbican/barbican.conf
              subPath: barbican.conf
            - name: barbican-policy-volume
              mountPath: /etc/barbican/policy.json
              subPath: policy.json
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          resources: {{ crd_spec | resources('keystoneListener.barbican-keystone-listener') }}
      volumes:
        - name: barbican-config-volume
          secret:
            secretName: {{ dependencies['config'].resource_name() }}
            items:
              - key: barbican.conf
                path: barbican.conf
        - name: barbican-policy-volume
          configMap:
            name: {{ dependencies['barbican_policy'].resource_name() }}
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
