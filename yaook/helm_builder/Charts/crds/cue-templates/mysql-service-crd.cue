// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#cacertificatecrd
crd.#imagepullsecretcrd
crd.#releaseawarecrd
crd.#operatorcrd
{
	#group:    "infra.yaook.cloud"
	#kind:     "MySQLService"
	#plural:   "mysqlservices"
	#singular: "mysqlservice"
	#shortnames: ["mysqlsvcs", "mysqlsvc"]
	#releases: ["10.2", "10.3", "10.4", "10.5", "10.6"]
	#schema: properties: spec: {
		crd.replicated
		crd.storageconfig
		required: [
			"database",
			"proxy",
			"frontendIssuerRef",
			"backendCAIssuerRef",
			"storageSize",
			"backup",
			"targetRelease",
		]
		properties: {
			database: {
				type:    "string"
				pattern: "[a-zA-Z$_]([0-9a-zA-Z$_]+)?"
			}
			mysqlConfig:    crd.#mysqlconfig
			serviceMonitor: crd.#servicemonitor
			proxy: {
				crd.replicated
				properties: {
					replicas: default: 1
					timeoutClient: {
						type:    "integer"
						default: 60
					}
					resources: {
						type: "object"
						properties: {
							"create-ca-bundle": crd.#containerresources
							"haproxy":          crd.#containerresources
							"service-reload":   crd.#containerresources
						}
					}
				}
			}

			implementation: {
				type:    "string"
				default: "MariaDB"
				enum: ["MariaDB"]
			}

			backup: crd.backupspec
			backup: properties: {
				mysqldump: {
					type:    "boolean"
					default: false
				}
			}

			frontendIssuerRef:  crd.#ref
			backendCAIssuerRef: crd.#ref

			resources: {
				type: "object"
				properties: {
					"mariadb-galera":  crd.#containerresources
					"backup-creator":  crd.#containerresources
					"backup-shifter":  crd.#containerresources
					"mysqld-exporter": crd.#containerresources
				}
			}
		}
	}
	#schema: properties: status: properties: replicas: type: "integer"
}
