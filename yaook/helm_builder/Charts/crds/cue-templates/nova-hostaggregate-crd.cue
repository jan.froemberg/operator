import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
{
	#group:    "compute.yaook.cloud"
	#kind:     "NovaHostAggregate"
	#plural:   "novahostaggregates"
	#singular: "novahostaggregate"
	#schema: properties: spec: {
		type: "object"
		required: ["novaRef", "keystoneRef"]
		properties: {
			novaRef:     crd.#ref
			keystoneRef: crd.#keystoneref
			zone: {
				type:    "string"
				pattern: "^[^:]*$"
			}
			properties: {
				type:                                   "object"
				"x-kubernetes-preserve-unknown-fields": true
			}
		}
	}
}
