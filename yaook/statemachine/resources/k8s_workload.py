#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Kubernetes Workload resources
#############################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: Deployment

.. autoclass:: Job

.. autoclass:: CronJob

.. autoclass:: StatefulSet

.. autoclass:: TemplatedDeployment

.. autoclass:: TemplatedJob

.. autoclass:: TemplatedCronJob

.. autoclass:: FinalTemplatedJob

.. autoclass:: TemplatedStatefulSet


"""
import asyncio
import copy
import typing

import jsonpatch

import kubernetes_asyncio.client as kclient

from .. import api_utils, context, exceptions, interfaces, watcher
from .base import ResourceBody
from .k8s import BodyTemplateMixin, SingleObject


T = typing.TypeVar("T")


async def patch_restart(instance:
                        typing.Union[
                            kclient.models.v1_stateful_set.V1StatefulSet,
                            kclient.models.v1_deployment.V1Deployment
                        ],
                        resource_interface:
                            interfaces.ResourceInterface,
                        restart_id: str,
                        ) -> None:
    if instance.spec.template.metadata.annotations is None:
        await resource_interface.patch(
            instance.metadata.namespace,
            instance.metadata.name,
            [
                {
                    "op": "add",
                    "path": jsonpatch.JsonPointer.from_parts([
                        "spec", "template", "metadata", "annotations",
                    ]).path,
                    "value": {context.ANNOTATION_RESTART_ID: restart_id},
                },
            ],
        )
    else:
        await resource_interface.patch(
            instance.metadata.namespace,
            instance.metadata.name,
            [
                {
                    "op": "add",
                    "path": jsonpatch.JsonPointer.from_parts([
                        "spec", "template", "metadata", "annotations",
                        context.ANNOTATION_RESTART_ID,
                    ]).path,
                    "value": restart_id,
                },
            ],
        )


class CronJob(SingleObject[kclient.V1beta1CronJob]):
    """
    Manage a CronJobState resource.
    """

    def __init__(self, *,
                 scheduling_keys: typing.Collection[str],
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._scheduling_keys = scheduling_keys

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[
                kclient.V1beta1CronJob]:
        return interfaces.cronjob_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1beta1CronJob,
                      new: typing.Mapping) -> bool:
        """
        Return True if the spec or the schedule changed.
        """
        cyaml = api_utils.k8s_obj_to_yaml_data(current.spec)
        if api_utils.deep_has_changes(
                cyaml["jobTemplate"],
                new["spec"]["jobTemplate"]):
            return True

        if new["spec"]["schedule"] != cyaml["schedule"]:
            return True

        return super()._needs_update(current, new)

    async def adopt_object(self,
                           ctx: context.Context,
                           body: ResourceBody) -> None:
        await super().adopt_object(ctx, body)
        if self._scheduling_keys:
            pod_spec = body.setdefault("spec", {}).setdefault(
                "jobTemplate", {}).setdefault("spec", {}).setdefault(
                    "template", {}).setdefault("spec", {})
            api_utils.inject_scheduling_keys(
                pod_spec,
                self._scheduling_keys,
            )


class Deployment(SingleObject[kclient.V1Deployment]):
    """
    :param scheduling_keys: Scheduling keys to use.
    :type scheduling_keys: collection of :class:`str`

    When configuring the Deployment resource, the Pods will gain a nodeAffinity
    for only those nodes matching any of the scheduling keys.

    A node matches a scheduling key if it has a label with that key; the value
    of that label is ignored.

    In addition, tolerations for all the scheduling keys (for any taint effect)
    are added to the pods.
    """

    def __init__(
            self, *,
            scheduling_keys: typing.Collection[str],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._scheduling_keys = list(scheduling_keys)

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[kclient.V1Deployment]:
        return interfaces.deployment_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1Deployment,
                      new: typing.Mapping) -> bool:
        cyaml = api_utils.k8s_obj_to_yaml_data(current.spec)
        cyaml.get("template", {}).get("metadata", {})\
            .get("annotations", {}).pop(context.ANNOTATION_RESTART_ID, None)
        new_copy = copy.deepcopy(new["spec"])
        new_copy.get("template", {}).get("metadata", {})\
            .get("annotations", {}).pop(context.ANNOTATION_RESTART_ID, None)
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml, new_copy))

    async def rolling_restart(
            self,
            ctx: context.Context,
            restart_id: str,
    ) -> None:
        """
        Trigger a rolling restart by adding an annotation to the pod template.

        :param restart_id: A string uniquely identifying the cause of the
            restart.
        :type restart_id: :class:`str`

        The `restart_id` is set as the value of the annotation. That means that
        the rolling restart is effectively only executed if the `restart_id` is
        different from whatever value was set on the annotation before. To
        always trigger a restart, :func:`api_utils.generate_update_timestamp`
        can be used to generate a unique ID.

        This is compatible with kubectl rollout restart in the sense that it
        uses the same annotation. The annotation is ignored by
        :meth:`reconcile` for the purposes of detecting a diff, so using this
        method or kubectl will not cause a spurious reconcile.
        """
        instance = await self._get_current(ctx)
        await patch_restart(
            instance,
            self.get_resource_interface(ctx),
            restart_id
        )

    async def scale(
            self,
            ctx: context.Context,
            replicas: int,
    ) -> None:
        """
        Trigger a scaling by replacing a replicas value with a different value.

        :param replicas: An integer, which defines a number of the replicas.
        :type replicas: :class:`int`

        The `replicas` is set as the value of the replicas in a deployment
        spec. That means that the scaling is effectively only executed if
        the number of `replicas` is different from whatever value was set
        on the replicas before.
        """
        instance = await self._get_current(ctx)
        await self.get_resource_interface(ctx).patch(
            instance.metadata.namespace,
            instance.metadata.name,
            [
                {
                    "op": "replace",
                    "path": jsonpatch.JsonPointer.from_parts([
                        "spec", "replicas",
                    ]).path,
                    "value": replicas,
                },
            ],
        )

    async def adopt_object(self,
                           ctx: context.Context,
                           body: ResourceBody) -> None:
        await super().adopt_object(ctx, body)
        if self._scheduling_keys:
            pod_spec = body.setdefault("spec", {}).setdefault(
                "template", {}
            ).setdefault("spec", {})
            api_utils.inject_scheduling_keys(
                pod_spec,
                self._scheduling_keys,
            )

    async def get_used_resources(
            self,
            ctx: context.Context,
            ) -> typing.Collection[api_utils.ResourceReference]:
        try:
            current = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return []

        result = set(api_utils.extract_pod_references(
            current.spec.template.spec,
            ctx.namespace,
        ))

        selector = api_utils.LabelSelector.from_api_object(
            current.spec.selector
        )

        pods = interfaces.pod_interface(ctx.api_client)
        for pod in await pods.list_(ctx.namespace,
                                    label_selector=selector.as_api_selector()):
            result.update(api_utils.extract_pod_references(
                pod.spec,
                ctx.namespace,
            ))

        return result

    def _is_ready(self, current: kclient.V1Deployment) -> bool:
        status: kclient.V1DeploymentStatus = current.status
        if not status:
            return False
        required_replicas = status.replicas
        return (required_replicas == status.updated_replicas) and \
            (required_replicas == status.available_replicas) and \
            (current.metadata.generation == status.observed_generation)

    async def is_ready(self, ctx: context.Context) -> bool:
        try:
            current = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return False
        return self._is_ready(current)

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + [
            context.KubernetesListener[kclient.V1Deployment](
                'apps', 'v1', 'deployments', self._handle_deployment_event,
                component=self.component,
            ),
        ]

    def _handle_deployment_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[
                kclient.V1Deployment],
            ) -> bool:
        if event.type_ == watcher.EventType.DELETED:
            return True

        deployment = event.object_
        if not deployment.status or \
                event.old_object is None or not event.old_object.status:
            return False

        if not self._is_ready(deployment):
            return False

        status = deployment.status
        oldstatus = event.old_object.status
        return (
            (oldstatus.updated_replicas != status.updated_replicas) or
            (oldstatus.available_replicas != status.available_replicas) or
            (oldstatus.observed_generation != status.observed_generation)
        )

    async def deployment_has_zero_replicas(
            self,
            ctx: context.Context
            ) -> bool:
        current = await self._get_current(ctx)
        status: kclient.V1DeploymentStatus = current.status
        if not status:
            return False
        return (status.replicas or 0) == 0 and current.spec.replicas == 0


class Job(SingleObject[kclient.V1Job]):
    """
    Manage a Job resource.

    :param scheduling_keys: Scheduling keys to use.
    :type scheduling_keys: collection of :class:`str`

    In contrast to other resources, the Job resource will only report as
    ready (via :meth:`is_ready`) after the Job has had at least one successful
    run.

    When configuring the Job resource, the Pods will gain a nodeAffinity for
    only those nodes matching any of the scheduling keys.

    A node matches a scheduling key if it has a label with that key; the value
    of that label is ignored.

    In addition, tolerations for all the scheduling keys (for any taint effect)
    are added to the pods.

    Jobs are automatically killled and recreated if `_needs_update` returns
    true. During that it is ensured that the old job completely terminates
    before the new one starts.
    This also means that setting `copy_on_write` for jobs is invalid.
    """

    _OBSERVATION_STATE_ANNOTATION = \
        "statemachine.yaook.cloud/completion-observed"

    def __init__(self, *,
                 scheduling_keys: typing.Collection[str],
                 **kwargs: typing.Any):
        if "copy_on_write" in kwargs:
            raise ValueError("Setting copy_on_write for jobs is not allowed")
        super().__init__(ignore_deleted_resources=False, **kwargs)
        self._scheduling_keys = scheduling_keys

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + [
            context.KubernetesListener[kclient.V1Job](
                'batch', 'v1', 'jobs', self._handle_job_event,
                component=self.component,
            ),
        ]

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[kclient.V1Job]:
        return interfaces.job_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1Job,
                      new: typing.Mapping) -> bool:
        cyaml = api_utils.k8s_obj_to_yaml_data(current)
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml["spec"], new["spec"]))

    def _handle_job_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[kclient.V1Job],
            ) -> bool:
        if event.type_ == watcher.EventType.DELETED:
            return True

        job = event.object_
        succeeded = job.status and job.status.succeeded
        if not succeeded or succeeded <= 0:
            return False

        obj_annotations = job.metadata.annotations or {}
        if not obj_annotations.get(self._OBSERVATION_STATE_ANNOTATION):
            return True

        return False

    async def _update_resource(self,
                               ctx: context.Context,
                               current: T,
                               new_body: ResourceBody) -> bool:
        metadata = api_utils.extract_metadata(current)
        interface = self.get_resource_interface(ctx)
        # The propagation_policy is critical here as it ensures that the job
        # is only deleted from k8s once all its pods are deleted.
        # In addition to setting ignore_deleted_resources to false in the
        # constructor, we also do not orphan the Job so that spurious
        # reconciles do not recreate the job before its really gone.
        await interface.delete(
            ctx.namespace, metadata["name"],
            interfaces.DeletionPropagationPolicy.FOREGROUND)
        await asyncio.sleep(0)
        try:
            await interface.read(ctx.namespace, metadata["name"])
            # If it did not raise, the resource is still there. So we need
            # to wait for the deleted event before we can continue
            return True
        except kclient.ApiException as e:
            if e.status == 404:
                # The resource is already gone, so no need to wait for the
                # event
                return False
            raise e

    async def _add_observed_annotation(self,
                                       ctx: context.Context,
                                       namespace: str,
                                       name: str) -> None:
        interface = self.get_resource_interface(ctx)
        # NOTE: we use a JSONPatch here because that is the safer way to add
        # the annotation.
        await interface.patch(
            namespace, name,
            [
                {
                    "op": "add",
                    "path": jsonpatch.JsonPointer.from_parts([
                        "metadata", "annotations",
                        self._OBSERVATION_STATE_ANNOTATION,
                    ]).path,
                    "value": "true"
                }
            ],
        )

    async def is_ready(self, ctx: context.Context) -> bool:
        """
        Return true if and only if the Job exists and has had at least one
        successful Pod.
        """
        try:
            instance = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return False

        # Explicitly check if the job is being deleted as for jobs we
        # explicitly disable the check for _get_current
        metadata = api_utils.extract_metadata(instance)
        if metadata.get("deletionTimestamp") is not None:
            return False

        status = instance.status
        if status is None:
            return False

        is_ok = status.succeeded is not None and status.succeeded > 0
        if not is_ok:
            return False

        obj_annotations = instance.metadata.annotations or {}
        if obj_annotations.get(self._OBSERVATION_STATE_ANNOTATION):
            return True

        await self._add_observed_annotation(
            ctx, instance.metadata.namespace, instance.metadata.name)
        return True

    async def adopt_object(self,
                           ctx: context.Context,
                           body: ResourceBody) -> None:
        await super().adopt_object(ctx, body)
        if self._scheduling_keys:
            pod_spec = body.setdefault("spec", {}).setdefault(
                "template", {},
            ).setdefault("spec", {})
            api_utils.inject_scheduling_keys(
                pod_spec,
                self._scheduling_keys,
            )

    async def get_used_resources(
            self,
            ctx: context.Context,
            ) -> typing.Collection[api_utils.ResourceReference]:
        try:
            current = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return []

        if (current.status is not None and
                current.status.succeeded is not None and
                current.status.succeeded > 0):
            return []

        result = set(api_utils.extract_pod_references(
            current.spec.template.spec,
            ctx.namespace,
        ))

        selector = api_utils.LabelSelector.from_api_object(
            current.spec.selector
        )

        pods = interfaces.pod_interface(ctx.api_client)
        for pod in await pods.list_(ctx.namespace,
                                    label_selector=selector.as_api_selector()):
            result.update(api_utils.extract_pod_references(
                pod.spec,
                ctx.namespace,
            ))

        return result


class StatefulSet(SingleObject[kclient.V1StatefulSet]):
    def __init__(
            self, *,
            scheduling_keys: typing.Optional[typing.Collection[str]] = None,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        if not scheduling_keys:
            scheduling_keys = []
        self._scheduling_keys = list(scheduling_keys)

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[kclient.V1StatefulSet]:
        return interfaces.stateful_set_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1StatefulSet,
                      new: typing.Mapping) -> bool:
        cyaml = api_utils.k8s_obj_to_yaml_data(current.spec)
        cyaml.get("template", {}).get("metadata", {})\
            .get("annotations", {}).pop(context.ANNOTATION_RESTART_ID, None)
        new_copy = copy.deepcopy(new["spec"])
        new_copy.get("template", {}).get("metadata", {})\
            .get("annotations", {}).pop(context.ANNOTATION_RESTART_ID, None)
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml, new_copy))

    async def rolling_restart(
            self,
            ctx: context.Context,
            restart_id: str,
    ) -> None:
        """
        Trigger a rolling restart by adding an annotation to the pod template.

        :param restart_id: A string uniquely identifying the cause of the
            restart.
        :type restart_id: :class:`str`

        The `restart_id` is set as the value of the annotation. That means that
        the rolling restart is effectively only executed if the `restart_id` is
        different from whatever value was set on the annotation before. To
        always trigger a restart, :func:`api_utils.generate_update_timestamp`
        can be used to generate a unique ID.

        This is compatible with kubectl rollout restart in the sense that it
        uses the same annotation. The annotation is ignored by
        :meth:`reconcile` for the purposes of detecting a diff, so using this
        method or kubectl will not cause a spurious reconcile.
        """
        instance = await self._get_current(ctx)
        await patch_restart(
            instance,
            self.get_resource_interface(ctx),
            restart_id
        )

    async def adopt_object(self,
                           ctx: context.Context,
                           body: ResourceBody) -> None:
        await super().adopt_object(ctx, body)
        if self._scheduling_keys:
            pod_spec = body.setdefault("spec", {}).setdefault(
                "template", {}
            ).setdefault("spec", {})
            api_utils.inject_scheduling_keys(
                pod_spec,
                self._scheduling_keys,
            )

    async def get_used_resources(
            self,
            ctx: context.Context,
            ) -> typing.Collection[api_utils.ResourceReference]:
        try:
            current = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return []

        result = set(api_utils.extract_pod_references(
            current.spec.template.spec,
            ctx.namespace,
        ))

        selector = api_utils.LabelSelector.from_api_object(
            current.spec.selector
        )

        pods = interfaces.pod_interface(ctx.api_client)
        for pod in await pods.list_(ctx.namespace,
                                    label_selector=selector.as_api_selector()):
            result.update(api_utils.extract_pod_references(
                pod.spec,
                ctx.namespace,
            ))

        return result

    def _is_ready(self, sts: kclient.V1StatefulSet) -> bool:
        if not sts.status:
            # the kube-controller hasn't seen this statefulset at all yet.
            return False
        if sts.status.observed_generation != sts.metadata.generation:
            # the kube-controller hasn't seen the most recent version of the
            # resource yet, so it may need to do things.
            return False
        if sts.status.current_revision != sts.status.update_revision:
            # the statefulset is in the process of rolling out an update.
            return False
        if (sts.status.ready_replicas is None or
                sts.status.ready_replicas < sts.status.replicas):
            # either it has never seen ready replicas yet, or the number of
            # ready replicas is less than the number of intended replicas
            return False
        # everything green.
        return True

    async def is_ready(self, ctx: context.Context) -> bool:
        try:
            sts = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return False
        return self._is_ready(sts)

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + [
            context.KubernetesListener[kclient.V1StatefulSet](
                'apps', 'v1', 'statefulsets', self._handle_sts_event,
                component=self.component,
            ),
        ]

    def _handle_sts_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[
                kclient.V1StatefulSet],
            ) -> bool:
        if event.type_ == watcher.EventType.DELETED:
            return True
        # We only trigger on a rising edge of readiness, because we do not base
        # any decisions on the readiness except whether to continue
        # reconciliation or not. A rising edge thus allows reconciliation to
        # proceed where it previously could not, so we need to watch for that
        # and trigger a reconcile when it happens.
        new_ready = self._is_ready(event.object_)
        if event.old_object is not None:
            old_ready = self._is_ready(event.old_object)
        else:
            old_ready = False
        return new_ready and not old_ready


class TemplatedDeployment(BodyTemplateMixin, Deployment):
    """
    Manage a jinja2-templated Deployment.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.DeploymentState`:
            for arguments specific to Deployments, such as the
            `scheduling_keys`.
    """


class TemplatedCronJob(BodyTemplateMixin, CronJob):
    """
    Manage a jinja2-templated CronJob.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
        :class:`~.CronJobState`:
            for specific information about managing Jobs and specific
            arguments, such as the `scheduling_keys`.
    """


class TemplatedJob(BodyTemplateMixin, Job):
    """
    Manage a jinja2-templated Job.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
        :class:`~.JobState`:
            for specific information about managing Jobs and specific
            arguments, such as the `scheduling_keys`.
    """


class TemplatedStatefulSet(BodyTemplateMixin, StatefulSet):
    """
    Manage a jinja2-templated StatefulSet.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
        :class:`~.StatefulSetState`:
            for specific information about managing StatefulSets.
    """


class FinalTemplatedJob(BodyTemplateMixin, Job):
    """
    Manage a jinja2-templated Job which is the final state of a oneshot.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
        :class:`~.JobState`:
            for specific information about managing Jobs and specific
            arguments, such as the `scheduling_keys`.
    """

    def _handle_job_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[kclient.V1Job],
            ) -> bool:
        job = event.object_
        failed = job.status and job.status.failed
        if failed and failed > 0:
            return True
        return super()._handle_job_event(ctx, event)

    async def is_ready(self, ctx: context.Context) -> bool:
        try:
            instance = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return False

        status = instance.status
        if status is None:
            return False

        is_finished = (
            (status.succeeded is not None and status.succeeded > 0) or
            (status.failed is not None and status.failed > 0)
            )
        if not is_finished:
            return False

        obj_annotations = instance.metadata.annotations or {}
        if obj_annotations.get(self._OBSERVATION_STATE_ANNOTATION):
            return True

        await self._add_observed_annotation(
            ctx, instance.metadata.namespace, instance.metadata.name)
        return True

    async def is_successful(self,
                            ctx: context.Context,
                            ) -> bool:
        try:
            instance = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            return False

        status = instance.status
        if status is None:
            return False

        return status.succeeded is not None and status.succeeded > 0
