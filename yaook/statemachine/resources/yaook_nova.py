import typing

import kubernetes_asyncio.client as kclient

from .. import interfaces
from .k8s import SingleObject


class SSHIdentity(SingleObject[typing.Mapping]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.sshidentity_interface(api_client)

    # _needs_update is omitted here because we didn't need a concrete
    # implementation at the time of adding it; SSHIdentity objects are only
    # created as empty objects by the nova_compute operator (which always has
    # to return False from `_needs_update` for that reason), hence no time was
    # invested in writing a generically correct `_needs_update` for
    # SSHIdentity objects in general.
