#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Base classes and mixins for Kubernetes resources
################################################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: BodyTemplateMixin

.. autoclass:: KubernetesReference

.. autoclass:: KubernetesResource

.. autoclass:: SingleObject

.. currentmodule:: yaook.statemachine.resources.k8s

.. autoclass:: DependencyTemplateWrapper

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: Orphan

"""
import abc
import dataclasses
import json
import typing

import jsonpatch

import kubernetes_asyncio.client as kclient
from lru import LRU

from .. import (
    api_utils,
    context,
    interfaces,
    exceptions,
    versioneddependencies,
    version_utils,
)
from .base import (
    DependencyMap,
    KubernetesReference,
    ResourceBody,
    TemplateMixin,
    TemplateParameters,
)


T = typing.TypeVar("T")
cache = LRU(32)


def _encode_body(body: ResourceBody) -> bytes:
    return json.dumps(body).encode("utf-8")


class DependencyTemplateWrapper:
    """
    A wrapper object used for templating.

    .. automethod:: resource_name

    .. automethod:: instances

    .. automethod:: instance_name_map

    .. automethod:: last_update_timestamp
    """

    def __init__(self,
                 ctx: context.Context,
                 obj: KubernetesReference):
        super().__init__()
        self._object = obj
        self._ctx = ctx

    async def _get(self) -> kclient.V1ObjectReference:
        """
        The way that the cache is implemented in this function could lead to a
        race condition when two tasks call this function for the same resource.
        Within the current implementation it will only cause an additional
        request to the k8s api that is not necessary. Keep this in mind when
        you modify the behavior of this function in the future.
        """
        cache_key_instance = hash((self._object, self._ctx.without_instance(),
                                   self._ctx.instance))
        cache_key = hash((self._object, self._ctx.without_instance()))
        object_ref = cache.get(cache_key)
        if object_ref is not None:
            return object_ref
        object_ref = cache.get(cache_key_instance)
        if object_ref is not None:
            return cache[cache_key_instance]
        object_ref, is_instanced = await self._object.get_with_instanced(
            self._ctx)
        if is_instanced:
            cache[cache_key_instance] = object_ref
        else:
            cache[cache_key] = object_ref
        return object_ref

    async def resource_name(self) -> str:
        """
        Return the Kubernetes name of the resource.
        """
        result = await self._get()
        return result.name

    @api_utils.cached
    async def instances(self) -> typing.Mapping[
            typing.Union[str, None],
            kclient.V1ObjectReference]:
        """
        Return a dictionary mapping all instance names to their Kubernetes
        object references (i.e. to objects with ``namespace`` and ``name``
        attributes).
        """
        return await self._object.get_all(self._ctx)

    @api_utils.cached
    async def instance_name_map(self) -> typing.Mapping[
            typing.Union[str, None], str]:
        """
        Return a dictionary mapping all instance names to their Kubernetes
        object names.
        """
        return {
            k: v.name
            for k, v in (await self.instances()).items()
        }

    @api_utils.cached
    async def last_update_timestamp(self) -> str:
        """
        Return the last update timestamp as contained in the object’s
        annotations.
        """
        interface = self._object.get_resource_interface(self._ctx)
        name = await self.resource_name()
        instance = await interface.read(self._ctx.namespace, name)
        metadata = api_utils.extract_metadata(instance)
        return metadata.get("annotations", {}).get(
            context.ANNOTATION_LAST_UPDATE
        )

    def labels(self) -> typing.Mapping[str, str]:
        if not isinstance(self._object, KubernetesResource):
            raise TypeError("labels() not supported for bare references")
        return self._object.labels(self._ctx)


class KubernetesResource(KubernetesReference[T], metaclass=abc.ABCMeta):
    """
    Manage the state of a Kubernetes resource.

    This is a specialisation of :class:`~.Resource`, but still abstract. It
    manages one or more Kubernetes resources of the same type.

    This is also a :class:`KubernetesReference` and can thus be passed to
    functions which expect a reference to the specific resource.

    .. seealso::

        TODO(resource-refactor): class hierarchy thing

    The Kubernetes resources associated with state managers have specifc labels
    in order to make them easily selectable through the Kubernetes API. The
    following labels are defined. Please see
    :ref:`yaook.statemachine.context.LabelsAndAnnotations`.

    **Public interface:**

    .. autoattribute:: component

    .. automethod:: cleanup_orphans

    .. automethod:: get

    .. automethod:: get_all

    .. automethod:: get_listeners

    .. automethod:: get_resource_interface

    .. automethod:: is_ready

    .. automethod:: reconcile

    **Subclass interface:**

    .. automethod:: _create_resource_interface

    **Utilities for subclasses:**

    .. automethod:: labels

    .. automethod:: adopt_object
    """

    def labels(self, ctx: context.Context) -> typing.Mapping[str, str]:
        """
        Obtain the label match to retrieve all managed objects.

        :param ctx: The context of the operation.
        :type ctx: :class:`~.Context`
        :rtype: :class:`dict`
        :return: A (fresh) dictionary mapping the label keys to values.
        """

        labels = ctx.base_label_match()
        labels[context.LABEL_COMPONENT] = self.component
        return labels

    def get_listeners(self) -> typing.List[context.Listener]:
        """
        Return a list of listener callbacks for this state.

        For the state to work correctly, the listeners must be invoked when
        the respective API objects change.
        """
        return []

    async def adopt_object(
            self,
            ctx: context.Context,
            obj: ResourceBody) -> None:
        """
        Update the `obj` in-place to adopt it into the given `ctx`.

        :param ctx: The context of the operation.
        :type ctx: :class:`Context`
        :param obj: The body of the resource to adopt.

        This auto-creates an owner reference for the parent custom resource
        referenced by the `ctx` and it applies all labels from :meth:`labels`.
        """

        adoption = {
            "apiVersion": ctx.parent_api_version,
            "name": ctx.parent_name,
            "kind": ctx.parent_kind,
            "uid": ctx.parent_uid,
            "blockOwnerDeletion": True,
            "controller": False,
        }
        metadata = obj.setdefault("metadata", {})
        metadata.setdefault("labels", {}).update(self.labels(ctx))
        owner_references = metadata.setdefault("ownerReferences", [])

        for ref in owner_references:
            if ref["apiVersion"] != adoption["apiVersion"]:
                continue
            if ref["kind"] != adoption["kind"]:
                continue
            ref.clear()
            ref.update(adoption)
            break
        else:
            owner_references.append(adoption)

    @abc.abstractmethod
    async def reconcile(self,
                        ctx: context.Context,
                        *,
                        dependencies: DependencyMap,
                        ) -> None:
        """
        Reconcile the state.

        :param ctx: The context of the current operation.
        :type ctx: :class:`Context`
        :param dependencies: The dependencies which are injected into this
            State.

        `dependencies` must be a dictionary mapping the state class to an
        instance of that class. It can be used to access information about
        dependent resources.
        """

    async def is_ready(self, ctx: context.Context) -> bool:
        """
        Whether the managed object is ready to use for dependents.
        """
        return True

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Mapping[
                typing.Tuple[str, str],
                typing.Collection[typing.Tuple[str, str]],
            ]) -> None:
        """
        Delete all unprotected resource orphans from the Kubernetes API server.

        :param ctx: The context to use.
        :param protect: A collection of resources to keep.

        `protect` must be a mapping which maps `(apiVersion, plural)` to a
        collection (preferably a set) of namespace-name-pairs of resources to
        keep. No resources from those sets will be deleted.
        """
        intf = self.get_resource_interface(ctx)
        selector = api_utils.LabelSelector(
            match_labels=self.labels(ctx),
            match_expressions=[api_utils.LabelExpression(
                key=context.LABEL_ORPHANED,
                operator=api_utils.SelectorOperator.EXISTS,
                values=None,
            )],
        ).as_api_selector()

        protected = protect.get((intf.api_version, intf.plural))
        if not protected:
            ctx.logger.debug(
                "cleanup_orphans: %s deleting by selector %s",
                self, selector,
            )
            await intf.delete_collection(
                ctx.namespace,
                label_selector=selector,
            )
            return

        for instance in await intf.list_(ctx.namespace,
                                         label_selector=selector):
            metadata = api_utils.extract_metadata(instance)
            key = metadata["namespace"], metadata["name"]
            if key not in protected:
                ctx.logger.debug(
                    "cleanup_orphans: %s deleting unprotected orphan %s/%s",
                    self, *key,
                )
                await intf.delete(*key)
            else:
                ctx.logger.debug(
                    "cleanup_orphans: %s NOT deleting protected orphan %s/%s",
                    self, *key,
                )


class Orphan(KubernetesResource[T]):
    """
    A state just to orphan all matching resources.
    Can be used when a state has been completely removed to clean up its
    resources.
    """
    def __init__(
            self,
            *,
            resource_interface_factory: typing.Callable[
                [kclient.ApiClient],
                interfaces.ResourceInterface[T],
            ],
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._resource_interface_factory = resource_interface_factory

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[T]:
        return self._resource_interface_factory(api_client)

    async def get_with_instanced(self, ctx: context.Context
                                 ) -> typing.Tuple[kclient.V1ObjectReference,
                                                   bool]:
        raise NotImplementedError()

    async def get_all(self,
                      ctx: context.Context) -> typing.Mapping[
                          typing.Union[str, None],
                          kclient.V1ObjectReference]:
        raise NotImplementedError()

    async def reconcile(self,
                        ctx: context.Context,
                        *,
                        dependencies: DependencyMap,
                        ) -> None:
        await self.delete(ctx, dependencies=dependencies)

    async def delete(
            self,
            ctx: context.Context,
            *,
            dependencies: DependencyMap,
    ) -> None:
        interface = self.get_resource_interface(ctx)
        await interface.delete_collection(
            ctx.namespace,
            label_selector=api_utils.LabelSelector(
                match_labels=self.labels(
                    dataclasses.replace(ctx, instance=None),
                ),
            ).as_api_selector(),
        )


class SingleObject(KubernetesResource[T], metaclass=abc.ABCMeta):
    """
    Manage the state of a single Kubernetes resource.

    :param copy_on_write: Initialises :attr:`copy_on_write`
    :param ignore_deleted_resources: Initialises
        :attr:`_ignore_deleted_resources`

    .. seealso::

        :class:`~.KubernetesResource`, :class:`~.Resource`
            for more supported arguments.

    **Public interface:**

    .. attribute:: copy_on_write

        Enable the use of a copy-on-write strategy when updating the resource.

        If :attr:`copy_on_write` is :data:`True`, the resource will be orphaned
        (and later deleted by :meth:`cleanup_orphans`) and re-created instead
        of updated in-place. It is effectively treated as completely immutable.

        This strategy has advantages for situations where the old resource may
        still be in use and switching over to the new resource is not an atomic
        process. One prominent example of this is service configuration stored
        in Secrets.

    .. autoattribute:: component

    .. automethod:: cleanup_orphans

    .. automethod:: get

    .. automethod:: get_all

    .. automethod:: get_listeners

    .. automethod:: get_resource_interface

    .. automethod:: is_ready

    .. automethod:: reconcile

    **Fields for subclasses:**

    .. attribute:: _ignore_deleted_resources

        If set to `True` then resources with a `deletionTimestamp` set will
        be ignored in
        :func:`~yaook.statemachine.resources.SingleObject._get_current`.
        If set to `False` these resources will be returned. This can be useful
        for foreground deletion.

    **Methods to be implemented by subclasses:**

    .. automethod:: _make_body

    .. automethod:: _needs_update

    **Interface for subclasses:**

    .. automethod:: _get_current

    **Internal API:**

    .. automethod:: _orphan
    """
    def __init__(self, *,
                 copy_on_write: bool = False,
                 ignore_deleted_resources: bool = True,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.copy_on_write = copy_on_write
        self._ignore_deleted_resources = ignore_deleted_resources

    def _needs_update(self, old: typing.Any, new: typing.Mapping) -> bool:
        """
        Test whether the resource must be updated.

        :param old: The old resource as returned by the Kubernetes API.
        :param new: The new resource as generated by :meth:`_make_body`.
        :return: True if the resource needs to be updated in Kubernetes, false
            otherwise.
        """
        try:
            old = api_utils.k8s_obj_to_yaml_data(old)
        except AttributeError:
            old = old

        if "metadata" in old:
            return api_utils.deep_has_changes(
                old.get("metadata", {}).get("labels", {}),
                new.get("metadata", {}).get("labels", {}),
            )

        return False

    @abc.abstractmethod
    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> ResourceBody:
        """
        Construct the complete intended body of the resource.

        :param ctx: The context of the operation.
        :param dependencies: The dependencies supplied to this resource.

        .. seealso::

            :meth:`~.Resource.reconcile`
                for details on `dependencies`.

        Return the complete body as nested dictionaries, including the
        ``apiVersion``, ``kind`` and ``metadata`` fields.
        """

    async def adopt_object(self,
                           ctx: context.Context,
                           body: ResourceBody) -> None:
        await super().adopt_object(ctx, body)
        self._add_annotations(body)

    def _add_annotations(self, body: ResourceBody) -> None:
        annotations = body.setdefault(
            "metadata", {}
        ).setdefault("annotations", {})
        annotations[context.ANNOTATION_LAST_UPDATE] = \
            api_utils.generate_update_timestamp()

    async def _get_current(self, ctx: context.Context,
                           show_deleted: bool = False) -> T:
        """
        Return the current instance of the resource, if it exists.

        :param ctx: The context of the operation.
        :param show_deleted: If True, also deactivated instances will be
            returned.
        :raises .ResourceNotPresent: If there is no active instance of the
            resource.
        :raises .AmbiguousRequest: If there are multiple active instances of
            the resource.
        :return: The resource as returned by the Kubernetes API.

        This lists all matching (see :meth:`.KubernetesResource.labels`)
        resources and filters out any orphaned or deleted resources.

        If exactly one resource remains, it is returned. Otherwise,
        :class:`~.ResourceNotPresent` or :class:`~.AmbiguousRequest` is raised
        as is appropriate./
        """
        label_match = self.labels(ctx)
        api_items = await self.get_resource_interface(ctx).list_(
            ctx.namespace,
            label_selector=label_match,
        )
        items = []
        for item in api_items:
            metadata = api_utils.extract_metadata(item)
            if metadata.get("deletionTimestamp") is not None and \
                    self._ignore_deleted_resources and not show_deleted:
                continue
            if context.LABEL_ORPHANED in metadata.get("labels", {}):
                continue
            items.append(item)
        if not items:
            raise exceptions.ResourceNotPresent(
                self.component, ctx,
                resource_version=api_items.resource_version,
            )
        if len(items) > 1:
            raise exceptions.AmbiguousRequest(self.component, ctx)
        return items[0]

    async def get_with_instanced(self, ctx: context.Context
                                 ) -> typing.Tuple[kclient.V1ObjectReference,
                                                   bool]:
        try:
            obj = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            # If we cannot find the object, we try without the instance in the
            # context. This makes sense as we might be inside some instancing
            # and this is the lookup for shared dependency.
            try:
                obj = await self._get_current(ctx.without_instance())
                reraise = False
            except (exceptions.AmbiguousRequest,
                    exceptions.ResourceNotPresent):
                reraise = True
            # this ugly flow with the reraise flag is required to not lose the
            # original context of `exc` (which should be treated as the main
            # error here)
            # if we used `raise exc` in the above `except` block, the context
            # of the exception would be set to the exception we caught there,
            # losing any original context the exception might have had.
            if reraise:
                raise
        metadata = api_utils.extract_metadata(obj)
        return (kclient.V1ObjectReference(
            name=metadata["name"],
            namespace=metadata["namespace"],
        ), "state.yaook.cloud/instance" in metadata.get("labels", {}))

    async def get_all(self, ctx: context.Context,
                      show_deleted: bool = False) -> typing.Mapping[
            typing.Union[str, None],
            kclient.V1ObjectReference]:
        items = await self.get_resource_interface(ctx).list_(
            ctx.namespace,
            label_selector=api_utils.LabelSelector(
                match_labels=self.labels(
                    dataclasses.replace(ctx, instance=None),
                ),
                match_expressions=[
                    api_utils.LabelExpression(
                        key=context.LABEL_ORPHANED,
                        operator=api_utils.SelectorOperator.NOT_EXISTS,
                        values=None,
                    ),
                ],
            ).as_api_selector(),
        )
        result = {}
        for item in items:
            metadata = api_utils.extract_metadata(item)
            if metadata.get("deletionTimestamp") is not None and \
                    not show_deleted:
                continue
            result[metadata.get("labels", {}).get(context.LABEL_INSTANCE)] = \
                kclient.V1ObjectReference(
                    name=metadata["name"],
                    namespace=metadata["namespace"],
                )
        return result

    async def _orphan(
            self,
            ctx: context.Context,
            namespace: str,
            name: str,
            ) -> None:
        """
        Orphan the current resource.

        :param ctx: The context of the operation.
        :param namespace: The namespace of the resource to orphan.
        :param name: The name of the resource to orphan.

        This will add the :data:`~.context.LABEL_ORPHANED` to the resource,
        if it is not present yet, to inform listers about the resource being
        deprecated.
        """
        intf = self.get_resource_interface(ctx)
        await intf.patch(
            namespace, name,
            [
                {
                    "op": "add",
                    "path": jsonpatch.JsonPointer.from_parts([
                        "metadata", "labels", context.LABEL_ORPHANED]).path,
                    "value": "",
                },
            ],
        )

    async def _update_resource(self,
                               ctx: context.Context,
                               current: T,
                               new_body: ResourceBody) -> bool:
        """
        Update the current resource. This is called by `reconcile` when it
        detects that the resource needs to change.

        It can be overwritten by subclasses to handle their own change or
        recreation behaviour.

        :param ctx: The context of the operation.
        :param current: The current state of the resource.
        :param new_body: The body that the resource should have
        :return: True if we should stop processing after the update.
            False if we should continue with recreating the resource
            afterwards.

        .. seealso::

            :meth:`~.SingleObject.reconcile`
                for details.
        """
        metadata = api_utils.extract_metadata(current)
        if not self.copy_on_write:
            await self.get_resource_interface(ctx).patch(
                ctx.namespace, metadata["name"],
                _encode_body(new_body),
                field_manager=ctx.field_manager,
                force=True,
            )
            return True

        await self._orphan(ctx, ctx.namespace, metadata["name"])
        return False

    async def reconcile(self,
                        ctx: context.Context,
                        *,
                        dependencies: DependencyMap,
                        ) -> None:
        """
        Reconcile the state of the resource.

        :param ctx: The context of the operation.
        :param dependencies: The dependencies for the operation.
        :return: True if the resource was updated or (re-)created, false
            otherwise.

        .. seealso::

            :meth:`~.Resource.reconcile`
                for details on `dependencies`.

        This compares a freshly generated intended body with the resource as
        is currently present in the Kubernetes API.

        If the body differs (:meth:`_needs_update`), the resource is updated
        according to the update strategy (see :attr:`copy_on_write`). If the
        resource does not exist at all, it will be created.

        If a change was made to the resource (update or (re-)creation), this
        method returns true, false otherwise.
        """
        new_body = await self._make_body(ctx, dependencies)
        await self.adopt_object(ctx, new_body)

        try:
            current = await self._get_current(ctx)
        except exceptions.ResourceNotPresent:
            ctx.logger.debug(
                "no resource present, will create new one",
            )
        else:
            if not self._needs_update(current, new_body):
                return

            ctx.logger.debug("resource exists and needs an update")
            if await self._update_resource(ctx, current, new_body):
                return
            # in the normal case fall through down to the recreation
            # Cases where we absolutely need to wait for the deletion to
            # complete would return `True` above. Two cases:
            # - Fixed resource name: The create call below will fail. See below
            #   for discussion.
            # - Generated name: The create call will succeed; later calls to
            #   _get_current will ignore both the orphan and a possibly
            #   async-deleted resource, so it does not matter either.

        # NOTE: If a resource has been ignored because of being an orphan or
        # its deletionTimestamp, this may cause a 409 Conflict. However, this
        # can only happen for until the resource is really gone and only if we
        # do not auto-generate a new name.
        #
        # This implies that `is_converged` MUST NOT return False for an
        # orphaned resource existing.
        #
        # In those cases, preventing a reconcile from proceeding is probably
        # the preferable way anyway. In addition, it does not seem to be
        # possible to set a deletion timestamp far in the future for "trivial"
        # resoucres such as ConfigMaps; they are gone right away when you call
        # the delete API endpoint even with a long grace period.
        await self.get_resource_interface(ctx).create(
            ctx.namespace,
            new_body,
            field_manager=ctx.field_manager,
        )

    async def delete(
            self,
            ctx: context.Context,
            *,
            dependencies: DependencyMap,
    ) -> None:
        try:
            ref = await self.get(ctx)
        except exceptions.ResourceNotPresent:
            return
        await self._orphan(ctx, ref.namespace, ref.name)


class DefaultTemplateParamsMixin(TemplateMixin):
    def __prepare_dependencies(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> typing.Mapping[str, DependencyTemplateWrapper]:
        wrapped_dependencies = {}
        # TODO(resource-refactor): resolve this cycle in a nicer way
        from .orchestration import Optional
        for component, resource in dependencies.items():
            if isinstance(resource, Optional):
                inner = resource.get_inner(ctx)
                if inner is None:
                    continue
                resource = inner
            if isinstance(resource, KubernetesReference):
                wrapped_dependencies[component] = DependencyTemplateWrapper(
                    ctx,
                    resource,
                )
        return wrapped_dependencies

    async def _get_template_parameters(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> TemplateParameters:
        """
        Extract the parameters fed to the template from the context and the
        dependencies.

        The resulting dictionary has the following keys:

        ``namespace``
            The namespace from the context.

        ``dependencies``
            A mapping which maps the dependency :attr:`~.State.component` to
            a wrapper object for use inside templates.

            .. seealso::

                :class:`~.DependencyTemplateWrapper`
                    for the API offered by the wrappers

        ``crd_spec``
            The full `spec` of the parent custom resource from the context.

        ``labels``
            The fully qualifying labels of the component. Note that you
            generally do not need to set the labels on the object iself as that
            is done by the state class.

        ``cluster_domain``
            The domain name used for the services in the kubernetes cluster.
            It can be specified when running kubeadm with the
            `--service-dns-domain` setting.

        ``params``
            The `params` passed to the constructor of this object.

        ``vars``
            By default, an empty hash. A subclass may override this method
            to add additional values in here.
            If the context has a instance set it its value is also included
            here.

        ``instance``
            Only set if the state is running in a instanced context.
            If yes contains the instance name/id.

        ``instance_data``
            Only set if the state is running in a instanced context and
            has instance_data.
            If yes contains the instance_data.

        ``target_release``
            The target release as specified in the spec of the custom
            resource, if present.
        """
        wrapped_dependencies = self.__prepare_dependencies(
            ctx,
            dependencies,
        )

        result = {
            "namespace": ctx.namespace,
            "dependencies": wrapped_dependencies,
            "crd_spec": ctx.parent_spec,
            "cluster_domain": api_utils.get_cluster_domain(),
            "params": self._params,
            "vars": {},
        }
        if ctx.instance is not None:
            result["instance"] = ctx.instance
        if ctx.instance_data is not None:
            result["instance_data"] = ctx.instance_data

        try:
            result["target_release"] = version_utils.get_target_release(ctx)
        except KeyError:
            pass

        # We do this nasty dance here because this class is a Mixin -- it
        # cannot fully rely on the methods available. We also cannot declare
        # an abstract `labels` method, because ABCMeta will not detect the
        # mixed-in labels and it will mess up the method resolution order.
        try:  # nosemgrep
            labels_func = getattr(self, "labels")
        except AttributeError:
            pass
        else:
            result["labels"] = labels_func(ctx)

        return result


class BodyTemplateMixin(DefaultTemplateParamsMixin):
    """
    Mixin to create the body of a resource from a Jinja2 template.

    :param template: Path to the template to render for the body.
    :type template: :class:`str`

    .. seealso::

        :class:`TemplateMixin`
            for more arguments of the constructor and which extra filters are
            available in templates.

    .. automethod:: _get_template_parameters
    """
    def __init__(
            self,
            *,
            template: str,
            versioned_dependencies: typing.List[
                versioneddependencies.VersionedDependency] = [],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._template = template
        self.versioned_dependencies = versioned_dependencies

    async def _make_body(self,
                         ctx: context.Context,
                         dependencies: DependencyMap) -> ResourceBody:
        return await self._render_template(
            self._template,
            await self._get_template_parameters(ctx, dependencies),
        )

    async def _get_template_parameters(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> TemplateParameters:
        """
        In addition to the parameters exposed by
        :meth:`~.DefaultTemplateParamsMixin._get_template_parameters`, this
        method offers the following:

        ``versioned_dependencies``
            A mapping which maps the dependency
            :attr:`~.VersionedDependency.name` to the version string
            provided by the `VersionedDependency`.
        """
        result = await super()._get_template_parameters(ctx, dependencies)

        ver_deps = {
            x.name: await x.get_versioned_url(ctx)
            for x in self.versioned_dependencies
        }

        result["versioned_dependencies"] = ver_deps

        return result
