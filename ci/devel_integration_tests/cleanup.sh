#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -euo pipefail

# Get logs
if [ -f "/tmp/claimed.kubeconfig" ]; then
    export KUBECONFIG="/tmp/claimed.kubeconfig"
    ./tools/dump-k8s.sh podlogs "$NAMESPACE" '' +pod +pvc +statefulset +deployment +job {keystone,barbican,glance,cinder,heat,nova,neutron,gnocchi}deployment novacomputenode neutron{bgpdr,dhcp,l2,l3,ovn}agent +service configmap secret roles clusterroles rolebindings clusterrolebindings cds amqpuser amqpserver mysqluser mysqlservice ovsdbservice keystoneuser keystoneendpoints cronjob || true

    for i in amqpserver amqpuser mysqlservice mysqluser ovsdbservice keystoneendpoints keystoneusers novacomputenodes neutronbgpdragents neutrondhcpagents neutronl2agents neutronl3agents neutronovnagents; do
        ./tools/strip-finalizers.sh "$i" -n "$NAMESPACE" &
    done
    echo "Waiting for finalizer cleanup to complete ..."
    wait
    echo "Finalizer cleanup done!"
fi

# Delete cluster
if [ -f "/tmp/claimed_cluster" ]; then
    CLAIMED_CLUSTER=$(cat /tmp/claimed_cluster)
    curl "https://ske.api.eu01.stackit.cloud/v1/projects/a67baa55-a893-49bd-ba60-2f92d811369f/clusters/${CLAIMED_CLUSTER}" -X DELETE -H @/tmp/curlheaders
fi
