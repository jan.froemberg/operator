#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -e

# yes we can pass a content type even though we do not send content :)
echo "Authorization: ${STACKIT_SERVICE_ACCOUNT}" > /tmp/curlheaders
echo "Content-Type: application/json" >> /tmp/curlheaders

set -x

apt-get update
# debianutils: which
apt-get install -o Apt::InstallRecommends=0 -y apt-transport-https curl jq openssl ca-certificates debianutils
cat ci/devel_integration_tests/files/packages.cloud.google.com-apt-key.gpg | apt-key add -

cat <<EOF > /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubectl jq

ske_api() {
    url=$1
    shift
    curl "https://ske.api.eu01.stackit.cloud/v1/${url}" -H @/tmp/curlheaders $@
}

ske_cluster_api() {
    url=$1
    shift
    ske_api "projects/a67baa55-a893-49bd-ba60-2f92d811369f/clusters${url}" $@
}

ske_cluster_api_put() {
    ske_cluster_api $1 -X PUT -d @-
}

# Hack: use the last 5 digits of the job id as a arbitrary and hopefully unique
# id for this cluster. This cluster has NO special relation to this CI job.
shootname="${CI_JOB_ID:(-5)}"
timestamp=$(date +%s)
flatcarversion=$(ske_api "provider-options" | jq -r '.machineImages[] | select(.name == "flatcar") | .versions[] | select(.state == "supported") | .version' | head -1)
cat "$(dirname "$0")/ske-cluster.json" | sed -e "s/replacethisname/${shootname}/" -e "s/replacethisnumber/$(($RANDOM % 3 + 1))/" -e "s/replacethisjobid/$CI_JOB_ID/" -e "s/replacethistesttime/$timestamp/" -e "s/replaceflatcarversion/$flatcarversion/" | ske_cluster_api_put "/${shootname}"

while true; do
    shootstatus=$(ske_cluster_api "/$shootname" | jq -r '.status.aggregated')
    if [ "x$shootstatus" = "xSTATE_HEALTHY" ] || [ "x$shootstatus" = "xSTATE_RECONCILING" ]; then
        echo "Shoot is now up. Can now start the run.";
        break;
    else
        echo "Shoot is still starting up. Waiting...";
        sleep 30;
    fi
done

# Save CLAIMED_CLUSTER so we can cleanup later
echo "${shootname}" > /tmp/claimed_cluster

# Build kubeconfig for the claimed cluster
export CLAIMED_CLUSTER=$shootname
ske_cluster_api "/$CLAIMED_CLUSTER/credentials" | jq -r '.kubeconfig' > /tmp/claimed.kubeconfig
export KUBECONFIG=/tmp/claimed.kubeconfig

pip install -e .
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

timeout 90m "$(dirname "$0")/run-tests.sh"
