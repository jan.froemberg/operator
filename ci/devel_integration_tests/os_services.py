#!/usr/bin/env python3
#
# Copyright (c) 2022 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Integration test release handling
#
# In order to not list services and releases in multiple places, this
# script reads the information from the actual python classes. It also
# provides some logic to find the correct releases for installation
# and upgrades.

import argparse
import collections
import importlib


class ReleaseInfo:
    def __init__(self):
        self.installables = set()
        self.upgradeables = set()

    def __repr__(self):
        return f"<{self.installables},{self.upgradeables}>"


# Openstack services which will be considered for Testing
# ModuleName : ClassName
CLASS_INFO = {
    "keystone": "KeystoneDeployment",
    "barbican": "Barbican",
    "nova": "Nova",
    "glance": "Glance",
    "horizon": "Horizon",
    "neutron": "Neutron",
    "cinder": "Cinder",
    "gnocchi": "Gnocchi",
    "ceilometer": "Ceilometer",
    "heat": "Heat",
}


# Define which services are currently installable for which release and
# if an upgrade path exists from the previous version.
def read_releases_from_classes():
    """
    Imports each service's class and reads available releases

    :returns: A dict release_name -> ReleaseInfo
    """
    releases = collections.defaultdict(ReleaseInfo)

    for mod_name, cls_name in CLASS_INFO.items():
        mod = importlib.import_module(f"yaook.op.{mod_name}")
        cls = getattr(mod, cls_name)
        for release in cls.RELEASES:
            releases[release].installables.add(mod_name)
        for release in cls.VALID_UPGRADE_TARGETS:
            releases[release].upgradeables.add(mod_name)

    return releases


RELEASES = read_releases_from_classes()
RELEASE_NAMES = list(sorted(RELEASES.keys()))


def get_installable_services(release_name):
    """
    Find the installable release for each service.

    :param release_name: The name of the release that is to be installed.
    :returns: A list of tuples (service, release)

    If a service is not available in the requested release, the next one
    down is used.
    The implementation is built on the expectation that all services are
    available in the oldest release so that there is always a fallback.
    """
    services = {}
    for release in RELEASE_NAMES:
        for service_name in RELEASES[release].installables:
            services[service_name] = release
            # we deprecated queens, rocky and stein for some services so
            # we need a workaround that the integrationtest runs with a higher
            # keystone and glance version for the deprecated versions
            if release in ["queens", "rocky", "stein"]:
                services["keystone"] = "train"
                services["glance"] = "train"
                services["barbican"] = "train"
                services["heat"] = "train"
        if release == release_name:
            break
    return services.items()


def get_upgrade_path(initial_release, target_release):
    """
    Generate a list of release names

    :param initial_release: First release in the list.
    :param target_release: Last release in the list.
    :returns: A list of release names.

    If target_release is None, or older than the initial release
    the return value will only contain the initial release.
    """
    if initial_release not in RELEASE_NAMES:
        raise KeyError(
            f"Invalid release name: {initial_release}")

    initial_index = RELEASE_NAMES.index(initial_release)
    target_index = RELEASE_NAMES.index(target_release)
    if target_index < initial_index:
        target_index = initial_index

    return RELEASE_NAMES[initial_index:target_index+1]


# Command line interface

def cmd_upgradepath(args):
    target_release = args.target or args.initial
    for release in get_upgrade_path(args.initial, target_release):
        print(release)


def cmd_installables(args):
    for installable in get_installable_services(args.release):
        if not args.include_lower_versions:
            if args.release not in installable:
                continue
        print(",".join(installable))


def cmd_upgradables(args):
    for upgradeable in RELEASES[args.release].upgradeables:
        print(upgradeable)


def main():
    parser = argparse.ArgumentParser()
    cmd_parsers = parser.add_subparsers(required=True, dest="command")

    upgradepath_parser = cmd_parsers.add_parser("upgradepath")
    upgradepath_parser.add_argument("--initial", required=True)
    upgradepath_parser.add_argument("--target")
    upgradepath_parser.set_defaults(func=cmd_upgradepath)

    installables_parser = cmd_parsers.add_parser("installables")
    installables_parser.add_argument(
        "--release", required=True, help="The Openstack Release"
    )
    installables_parser.add_argument(
        "--include-lower-versions",
        help="Include the next best version for Services "
             "where the requested release is not available",
        action="store_true",
        dest="include_lower_versions",
    )
    installables_parser.set_defaults(
        func=cmd_installables, include_lower_versions=False
    )

    upgradables_parser = cmd_parsers.add_parser("upgradables")
    upgradables_parser.add_argument("--release", required=True)
    upgradables_parser.set_defaults(func=cmd_upgradables)

    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
