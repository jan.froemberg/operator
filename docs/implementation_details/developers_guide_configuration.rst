Developers Guide: Configration generation
=========================================

In this developers guide we will cover generation and validation of configuration data inside Yaook operators.
We generally assume you already had a look at the concepts section above (especially the :doc:`../concepts/configuration` document).

Cuelang
-------

The yaook operators rely on cuelang (https://cuelang.org/) to do the heavy lifing of configuration generation and validation.
Cuelang allows us to take an arbitrary amount of configuration pieces and merge them together.
During this cuelang will make sure the individual pieces do not conflict.

Implementation
--------------

Configuration is generated into Configmaps or Secrets by using :class:`yaook.statemachine.CueConfigMapState` and :class:`yaook.statemachine.CueSecretState`.
Both of them behave in the same way.

To pass in pieces of configuration we use :class:`yaook.statemachine.CueLayer`'s.
Each of these layers holds an individual piece configuration.

Each layer can generate its piece of configuration in any possible way.
Check :mod:`yaook.statemachine` for a list of all existing layers.

Each layer has a :meth:`yaook.statemachine.CueLayer.get_layer` method to access the layer data.
For an example best take a look at an existing implementation.

Copy on write
-------------

Most services do not support hot-reloading of configuration changes.
Because of this we can not simply update a configmap or secret when the containing data should change.
Instead we generate a new configmap/secret and :meth:`yaook.statemachine.SingleObjectState._orphan` the old resource.

As now the name of the configmap/secret changed the full reconcile run of the operator will propagate the change to the users of the configmap/secret.
For deployments and statefulsets this will cause an update to them which will trigger a rolling restart.
This will also ensure that existing pods can restart and still get their old configuration.
This is especially helpful if the new configuration is invalid as it ensures the old pods will stay in the old state.

An orphaned resource means for the operator that it should delete the resource whenever it is no longer actively used.
The operator regularly checks for all used resources using :meth:`yaook.statemachine.Resource.get_used_resources`.
All resources that are orphaned and no longer used are deleted.
