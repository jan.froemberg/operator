Yaook Disruption Budget
=======================

Yaook Disruption Budgets allow you to tune the automated updating of nova and neutron agents.
Per default yaook will update/evacuate one agent at a time to roll out a given change.
However if you have a large deployment this might take a long time and you might want to parallelize this behaviour.

Assume you have the following setup:
* 200 compute nodes in providing services to the same host aggregate. To rollout changes fast up to 10% of these should update at the same time
* 10 compute nodes serving some special host aggregate. Yaook should never automatically update these (maybe because it is currently not possible to live-migrated nested-virtualization machines)
* 6 network nodes providing neutron l3 and dhcp agents. Yaook should update them one-by-one

To do this you could create the following YaookDisruptionBudgets.

.. literalinclude:: disruption_budget.yaml

The operators will write to the status field of the YaookDisruptionBudgets.
You can use this to find out the state of a rollout of changes or how many nodes are matching a given disruption budget.
