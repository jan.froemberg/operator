Nova
======

.. literalinclude:: nova.yaml

Ceph Config
-----------

.. note::
    To use ceph you need to specify the same uuid and the same secret name as
    it is in use for cinder. The usage of different keys/users is not
    supported by openstack.

Add vTPM to instance
--------------------

To add a vTPM, the following prequisites are to be fullfilled.

1. Enable the swtpm settings in the nova-compute config for the nodes which are to provide the vTPM to the instance
.. literalinclude:: nova-compute-vtpm.yaml
As we specify the same user/group within the qemu config for swtpm we highly recommend to use nova:libvirt within the novacompute config.

2. Set the image or flavor specs to provide a vTPM if an instance is created with either of those.
`openstack flavor set $FLAVOR -property hw:tpm_version=2.0 --property hw:tpm_model=tpm-crb`
or
`openstack image set $IMAGE -property hw_tpm_version=2.0 --property hw_tpm_model=tpm-crb`
(reference https://docs.openstack.org/nova/latest/admin/emulated-tpm.html)

3. Have barbican as secret manager present
4. Nova (and compute) of atleast Victoria Release
5. Make sure the novacompute keystone user has permissions to create(store) barbican secrets (and read them).


NovaHostAggregate
=================

Availability-Zone and Properties are both optional.
A computenode can only reside in a single Availability-Zone.
If there is a mapping for aggregates with conflicting Availability-Zone values, no mapping will take place.
(there will be an Exception for that)

Example of a NovaHostAggregate
------------------------------

.. literalinclude:: novahostaggregate.yaml
