Welcome to Yaook's documentation!
=================================

Welcome to the documentation of Yaook.

Yaook is providing a fully automated OpenStack lifecycle management.
It thereby not only focusses on OpenStack itself, but also on all other relevant aspects (e.g. hardware inventory, monitoring).
Generic day 2 operation tasks are also included in Yaook's scope.

A general overview about the Yaook project is given in :doc:`concepts/overview`.

The source code is made available under Apache License and is located at: https://gitlab.com/yaook

A step-by-step guide on how to build your first Yaook cluster is given in :doc:`getting_started/dev_setup`.

If you want to start developing for Yaook you can take a look at the :doc:`implementation_details/developers_guide`.

.. toctree::
   :maxdepth: 2
   :caption: Getting Started
   :hidden:

   getting_started/dev_setup
   getting_started/common-problems
   getting_started/devpod
   getting_started/migrating-to-helm

.. toctree::
   :maxdepth: 2
   :caption: Concepts
   :hidden:

   concepts/overview
   concepts/operators
   concepts/configuration
   concepts/credentials
   concepts/updates
   concepts/scheduling
   concepts/safe-eviction
   concepts/releases
   concepts/deprecation_policy

.. toctree::
   :maxdepth: 2
   :caption: User Manual
   :hidden:

   handbook/custom-resource-options
   handbook/operator
   handbook/custom-resource-status
   handbook/finalizer-cleanup
   handbook/bgp-setup

.. toctree::
   :maxdepth: 2
   :caption: Requirements
   :hidden:

   requirements/os
   requirements/k8s-api
   requirements/k8s-cluster

.. toctree::
   :maxdepth: 2
   :caption: Deployment
   :hidden:

   deployment/downloads
   deployment/operator_helm_chart_values.rst

.. toctree::
   :maxdepth: 2
   :caption: Operators
   :hidden:

   operators/configureddaemonset
   operators/operator-structure

.. toctree::
   :maxdepth: 2
   :caption: Implementation Details
   :hidden:

   implementation_details/developers_guide
   implementation_details/developers_guide_configuration
   implementation_details/containers
   implementation_details/openstack_upgrades
   implementation_details/nova/index
   implementation_details/helm-charts

.. toctree::
   :maxdepth: 2
   :caption: Example Kubernetes Resources
   :hidden:

   examples/glance
   examples/keystone
   examples/barbican
   examples/neutron
   examples/nova
   examples/cinder
   examples/horizon
   examples/rook-cluster
   examples/ceilometer
   examples/gnocchi
   examples/heat
   examples/tempest
   examples/mysqlservice
   examples/memcachedservice
   examples/disruption_budget
   examples/infra_ironic

.. toctree::
   :caption: Internal API Reference
   :maxdepth: 2

   reference/yaook.statemachine.rst
   reference/yaook.op.rst

.. toctree::
   :caption: vGPU Support
   :maxdepth: 2

   examples/vgpu_support.rst
   examples/vgpu_types.rst
