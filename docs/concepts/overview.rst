Overview
========

Yaook is a lifecycle management tool for OpenStack which renders it possible to automatically transfer a set of bare metal machines to an operational OpenStack cluster (soon, SecuStack will be supported as well).

For short, Yaook installs and configures operating systems on bare metal machines, provisions a Kubernetes cluster on top of this base installation and leverages the provided Kubernetes cluster to deploy OpenStack.

Thus, Yaook is divided into three layers:

Yaook Bare Metal
    Deploys and manages server hardware ("Metal-as-a-Service").
    This includes defining and building the OS running on the servers as well as their network and disk configuration.
    It allows to automatically redeploy nodes which is Yaook's primary mechanism for software updates.

Yaook Kubernetes
    Deploys and manages the Kubernetes below OpenStack.
    It handles Kubernetes upgrades transparently and provides a full-featured Kubernetes cluster with LoadBalancers, Ingresses, StorageClasses, Certificates and other important APIs.

Yaook Operator
    Deploys and manages OpenStack on top of Kubernetes
    It talks to the standard Kubernetes APIs to create and manage resources in the cluster.
    It handles OpenStack upgrades and maintenance (such as evacuating nodes) and provides an interface for the user to insert configuration.

.. image:: figures/overview.drawio.svg
