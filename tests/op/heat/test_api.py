#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import pprint
import os
import json

import ddt

import yaook.op.heat as heat
import yaook.op.common as op_common
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces
import yaook.statemachine as sm

from tests import testutils

NAMESPACE = "test-namespace"
NAME = "heat"
CONFIG_FILE_NAME = "heat.conf"
CONFIG_PATH = "/etc/heat"
POLICY_FILE_NAME = "policy.json"
POLICY_RULE_KEY = "stacks:create"
POLICY_RULE_VALUE = "rule:deny_stack_user"
HEAT_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}
CONFIG_SECRET_NAME = "config-secret"
CONFIG_SECRET_KEY = "mysecretkey"
CONFIG_SECRET_VALUE = "mysecretvalue"


def _get_heat_deployment_yaml(keystone_name):
    return {
        'metadata': {
            'name': NAME,
            'namespace': NAMESPACE
        },
        'spec': {
            'keystoneRef': {
                'name': keystone_name,
                "kind": "KeystoneDeployment",
            },
            'database': {
                'replicas': 3,
                'storageSize': '8Gi',
                'proxy': {
                    'replicas': 5,
                    "resources": testutils.generate_db_proxy_resources(),
                },
                'backup': {
                    'schedule': '0 * * * *'
                },
                "resources": testutils.generate_db_resources(),
            },
            'messageQueue': {
                'replicas': 1,
                'storageSize': '5Gi',
                "resources": testutils.generate_amqp_resources(),
            },
            'api': {
                'replicas': 3,
                'ingress': {
                    'fqdn': f"heat.{NAMESPACE}.cloud",
                    'port': 32443,
                    'ingressClassName': 'nginx',
                },
                "resources": testutils.generate_resources_dict(
                    "api.heat-api",
                    "api.ssl-terminator",
                    "api.ssl-terminator-external",
                    "api.service-reload",
                    "api.service-reload-external",
                ),
            },
            'apiCfn': {
                'replicas': 3,
                'ingress': {
                    'fqdn': f"heat-cfn.{NAMESPACE}.cloud",
                    'port': 32443,
                    'ingressClassName': 'nginx',
                },
                "resources": testutils.generate_resources_dict(
                    "apiCfn.heat-api-cfn",
                    "apiCfn.ssl-terminator",
                    "apiCfn.ssl-terminator-external",
                    "apiCfn.service-reload",
                    "apiCfn.service-reload-external",
                ),
            },
            'engine': {
                'replicas': 3,
                "resources": testutils.generate_resources_dict(
                    "engine.heat-engine",
                ),
            },
            'heatConfig': {
                'DEFAULT': {
                    'debug': True
                },
                'trustee': {
                    'auth-url': 'https://keystone:5000/v3'
                }
            },
            "heatSecrets": [
                {
                    "secretName": CONFIG_SECRET_NAME,
                    "items": [{
                        "key": CONFIG_SECRET_KEY,
                        "path": "/DEFAULT/mytestsecret",
                    }],
                },
            ],
            "memcached": {
                "replicas": 2,
                "memory": "512",
                "connections": "2000",
                "resources": testutils.generate_memcached_resources(),
            },
            "issuerRef": {
                "name": "issuername"
            },
            'region': {
                'name': 'MyRegion',
                'parent': 'ParentRegionName'
            },
            'targetRelease': 'train',
            "jobResources": testutils.generate_resources_dict(
                "job.heat-db-sync-job",
            ),
        }
    }


@ddt.ddt
class TestHeatDeployments(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin,
        testutils.MessageQueueTestMixin,
        testutils.MemcachedTestMixin):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            heat.Heat,
            _get_heat_deployment_yaml(self._keystone_name),
        )
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        CONFIG_SECRET_KEY: CONFIG_SECRET_VALUE,
                    }),
                },
            )

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(endpoint["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_is_created(self):
        deployment_yaml = _get_heat_deployment_yaml(self._keystone_name)
        self._configure_cr(heat.Heat, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 1)

    async def test_keystone_endpoint_is_not_created(self):
        deployment_yaml = _get_heat_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["api"]["publishEndpoint"] = False
        self._configure_cr(heat.Heat, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 0)

    async def test_cfn_keystone_endpoint_is_created(self):
        deployment_yaml = _get_heat_deployment_yaml(self._keystone_name)
        self._configure_cr(heat.Heat, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_cfn_endpoint"},
        )

        self.assertEqual(len(endpoints), 1)

    async def test_cfn_keystone_endpoint_is_not_created(self):
        deployment_yaml = _get_heat_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["apiCfn"]["publishEndpoint"] = False
        self._configure_cr(heat.Heat, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_cfn_endpoint"},
        )

        self.assertEqual(len(endpoints), 0)

    async def test_keystone_endpoint_matches_region(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["region"]["name"],
                         "MyRegion")
        self.assertEqual(endpoint["spec"]["region"]["parent"],
                         "ParentRegionName")

    async def test_created_config_matches_keystone_credentials(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config_api"},
        )

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        user_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: user["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        user_credentials = sm.api_utils.decode_secret_data(
            user_credentials_secret.data
        )
        cfg = testutils._parse_config(
            config.data[CONFIG_FILE_NAME], decode=True)

        self.assertEqual(cfg.get("keystone_authtoken", "username"),
                         user_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("keystone_authtoken", "password"),
                         user_credentials["OS_PASSWORD"])

    async def test_certificate_contains_api_service_name(self):
        await self._certificate_contains_service_name('api')

    async def test_certificate_contains_api_cfn_service_name(self):
        await self._certificate_contains_service_name('api_cfn')

    async def test_certificate_contains_api_ingress_fqdn(self):
        await self._certificate_contains_api_ingress_fqdn(
            "api", f"heat.{NAMESPACE}.cloud"
        )

    async def test__certificate_contains_issuer_name(self):
        await self._certificate_contains_issuer_name("api")

    async def test_certificate_contains_api_cfn_ingress_fqdn(self):
        await self._certificate_contains_api_ingress_fqdn(
            "api_cfn", f"heat-cfn.{NAMESPACE}.cloud"
        )

    async def _get_certificate(self, component_name):
        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"certificate_{component_name}",
            },
        )

        return certificate

    async def _certificate_contains_service_name(self, component_name):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"{component_name}_service",
            },
        )

        certificate = await self._get_certificate(component_name)

        self.assertIn(
            f"{service.metadata.name}.{NAMESPACE}.svc",
            certificate["spec"]["dnsNames"],
        )

    async def _certificate_contains_issuer_name(self, component_name):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificate = await self._get_certificate(component_name)

        self.assertEqual(
            "issuername",
            certificate["spec"]["issuerRef"]["name"],
        )

    async def _certificate_contains_api_ingress_fqdn(
            self, component_name, fqdn):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificate = await self._get_certificate(component_name)

        self.assertIn(
            fqdn,
            certificate["spec"]["dnsNames"],
        )

    async def test_creates_db_sync_job_and_halts(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        self._make_all_memcached_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        db_sync, = all_jobs

        self.assertEqual(db_sync.metadata.labels[context.LABEL_COMPONENT],
                         "db_sync")

    async def _helper_creates_deployment_with_replicas_when_all_jobs_succeed(
            self, component_label, replicas):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: component_label}
        )
        self.assertEqual(deployment.spec.replicas, replicas)

    async def test_creates_api_deployment_with_replicas_when_all_jobs_succeed(
            self):
        await self._helper_creates_deployment_with_replicas_when_all_jobs_succeed(  # noqa: E501
            'api_deployment', 3
        )

    async def test_creates_api_cfn_deployment_with_replicas_when_all_jobs_succeed(  # noqa: E501
            self):
        await self._helper_creates_deployment_with_replicas_when_all_jobs_succeed(  # noqa: E501
            'api_cfn_deployment', 3
        )

    async def test_creates_engine_statefulset_with_replicas_when_all_jobs_succeed(  # noqa: E501
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        statefulset_interface = \
            interfaces.stateful_set_interface(self.api_client)
        statefulset, = await statefulset_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: 'engine'}
        )
        self.assertEqual(statefulset.spec.replicas, 3)

    # Tests for database
    async def test_creates_database(self):
        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        db, = await dbs.list_(NAMESPACE)

        self.assertEqual(db["spec"]["replicas"], 3)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 5)
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def _helper_creates_database_credentials(self, user):
        self._make_all_databases_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(NAMESPACE)
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"db_{user}_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"db_{user}_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )

    async def test_creates_database_api_credentials(self):
        await self._helper_creates_database_credentials("api")

    async def test_creates_database_api_cfn_credentials(self):
        await self._helper_creates_database_credentials("api_cfn")

    async def test_creates_database_engine_credentials(self):
        await self._helper_creates_database_credentials("engine")

    async def _helper_creates_config_with_database_uri(self, user):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"config_{user}",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"db_{user}_user_password",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"db_{user}_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )
        db_name = db["spec"]["database"]
        heat_conf = config.data[CONFIG_FILE_NAME]
        cfg = testutils._parse_config(heat_conf, decode=True)

        self.assertEqual(
            cfg.get("DEFAULT", "sql_connection"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_database_frontendIssuer_name(self):
        self._make_all_databases_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        mysqlservices = interfaces.mysqlservice_interface(self.api_client)

        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )

        self.assertEqual(
            "issuername",
            db["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_creates_config_with_database_uri_api(self):
        await self._helper_creates_config_with_database_uri('api')

    async def test_creates_config_with_database_uri_api_cfn(self):
        await self._helper_creates_config_with_database_uri('api_cfn')

    async def test_creates_config_with_database_uri_engine(self):
        await self._helper_creates_config_with_database_uri('engine')

    async def _helper_database_uri_refers_to_mounted_ca_bundle(
            self, component_resource, component_name):
        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"config_{component_name}",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "heatdeployments",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            component_resource.spec.template.spec,
            testutils.find_configmap_volume(
                component_resource.spec.template.spec,
                ca_certs.metadata.name,
            ),
            f"heat-{component_name}".replace("_", "-"),
        )

        heat_conf = config.data["heat.conf"]
        cfg = testutils._parse_config(heat_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("DEFAULT", "sql_connection"),
        )

    async def test_database_uri_refers_to_mounted_ca_bundle_api(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)

        component_resource, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_deployment",
            },
        )

        await self._helper_database_uri_refers_to_mounted_ca_bundle(
            component_resource, "api"
        )

    async def test_database_uri_refers_to_mounted_ca_bundle_api_cfn(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)

        component_resource, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_cfn_deployment",
            },
        )

        await self._helper_database_uri_refers_to_mounted_ca_bundle(
            component_resource, "api_cfn"
        )

    async def test_database_uri_refers_to_mounted_ca_bundle_engine(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulset_interface = \
            interfaces.stateful_set_interface(self.api_client)

        component_resource, = await statefulset_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: 'engine'}
        )

        await self._helper_database_uri_refers_to_mounted_ca_bundle(
            component_resource, "engine"
        )

    # Tests for message queue
    async def test_creates_message_queue_and_user(self):
        await self.cr.sm.ensure(self.ctx)

        mqs = interfaces.amqpserver_interface(self.api_client)

        mq, = await mqs.list_(NAMESPACE)

        self.assertEqual(mq["spec"]["replicas"], 1)
        self.assertEqual(mq["spec"]["storageSize"], "5Gi")

    async def _helper_creates_message_queue_credentials(self, component):
        self._make_all_mqs_succeed_immediately()

        await self.cr.sm.ensure(self.ctx)

        mqs = interfaces.amqpserver_interface(self.api_client)
        mqusers = interfaces.amqpuser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        mq, = await mqs.list_(NAMESPACE)
        mq_user, = await mqusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"mq_{component}_user",
            }
        )
        mq_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"mq_{component}_user_password",
            }
        )

        self.assertEqual(mq_user["spec"]["serverRef"]["name"],
                         mq["metadata"]["name"])
        self.assertEqual(
            mq_user["spec"]["passwordSecretKeyRef"]["name"],
            mq_user_password.metadata.name,
        )

    async def test_creates_message_queue_credentials_api(self):
        await self._helper_creates_message_queue_credentials("api")

    async def test_creates_message_queue_credentials_api_cfn(self):
        await self._helper_creates_message_queue_credentials("api_cfn")

    async def test_creates_message_queue_credentials_engine(self):
        await self._helper_creates_message_queue_credentials("engine")

    async def _helper_creates_config_with_transport_url(self, component):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"config_{component}",
            },
        )
        mq_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: f"mq_{component}_user_password",
            },
        )
        mq_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "amqpservers",
            },
        )
        mq_user_password = await sm.extract_password(
            self.ctx,
            mq_user_password_secret.metadata.name,
        )
        heat_conf = config.data[CONFIG_FILE_NAME]
        cfg = testutils._parse_config(heat_conf, decode=True)

        user_name = component.replace("_", "-")

        self.assertEqual(
            cfg.get("DEFAULT", "transport_url"),
            f"rabbit://{user_name}:{mq_user_password}@"
            f"{mq_service.metadata.name}.{mq_service.metadata.namespace}:5671/"
        )

    async def test_creates_config_with_transport_url_api(self):
        await self._helper_creates_config_with_transport_url("api")

    async def test_creates_config_with_transport_url_api_cfn(self):
        await self._helper_creates_config_with_transport_url("api_cfn")

    async def test_creates_config_with_transport_engine(self):
        await self._helper_creates_config_with_transport_url("engine")

    async def test_creates_memcached(self):
        await self.cr.sm.ensure(self.ctx)

        memcacheds = interfaces.memcachedservice_interface(self.api_client)
        memcached, = await memcacheds.list_(NAMESPACE)

        self.assertEqual(
            memcached["spec"]["memory"],
            "512"
        )
        self.assertEqual(
            memcached["spec"]["connections"],
            "2000"
        )

    # Tests for services, endpoints and ingresses
    async def test_creates_service_api(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "heat-api")

        self.assertIsNotNone(service)

    async def test_creates_service_api_cfn(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "heat-api-cfn")

        self.assertIsNotNone(service)

    async def _helper_creates_endpoint_with_ingress(
            self, component, public_url, admin_and_internal_url):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE,
                                        f"heat-{component}-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["public"],
            public_url,
        )

        self.assertEqual(
            endpoint["spec"]["endpoints"]["admin"],
            admin_and_internal_url,
        )

        self.assertEqual(
            endpoint["spec"]["endpoints"]["internal"],
            admin_and_internal_url,
        )

    async def test_creates_endpoint_with_ingress_api(self):
        await self._helper_creates_endpoint_with_ingress(
            "api",
            f"https://heat.{NAMESPACE}.cloud:32443/v1/%(tenant_id)s",
            f"https://heat-api.{NAMESPACE}.svc:8004/v1/%(tenant_id)s"
        )

    async def test_creates_endpoint_with_ingress_api_cfn(self):
        await self._helper_creates_endpoint_with_ingress(
            "api-cfn",
            f"https://heat-cfn.{NAMESPACE}.cloud:32443/v1",
            f"https://heat-api-cfn.{NAMESPACE}.svc:8000/v1"
        )

    async def _helper_creates_ingress(self, name):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, name)

        self.assertEqual(
            ingress.spec.rules[0].host,
            f"{name}.{NAMESPACE}.cloud",
        )

    async def test_creates_ingress_api(self):
        await self._helper_creates_ingress("heat")

    async def test_creates_ingress_api_cfn(self):
        await self._helper_creates_ingress("heat-cfn")

    async def _helper_ingress_matches_service(self, ingress_name, component):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, ingress_name)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, f"{ingress_name}-{component}")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )

    async def _helper_ingress_matches_service_api(self):
        await self._helper_ingress_matches_service("heat", "api")

    async def _helper_ingress_matches_service_api_cfn(self):
        await self._helper_ingress_matches_service("heat-cfn", "api-cfn")

    async def _helper_service_matches_deployment_pods(self, component):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: f"{component}_service"}
        )
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: f"{component}_deployment"}
        )

        pod_labels = deployment.spec.template.metadata.labels
        service_labels = service.spec.selector

        self.assertTrue(
            sm.matches_labels(pod_labels, service_labels),
            f"pods: {pprint.pformat(pod_labels)}\n"
            f"service: {pprint.pformat(service_labels)}\n",
        )

    async def test_service_matches_deployment_pods_api(self):
        await self._helper_service_matches_deployment_pods("api")

    async def test_service_matches_deployment_pods_api_cfn(self):
        await self._helper_service_matches_deployment_pods("api_cfn")

    # Tests for scheduling keys
    async def _helper_applies_scheduling_key_to_api_deployment(self, component,
                                                               scheduling_key):
        self._make_all_dependencies_complete_immediately()
        blocking = await self.cr.sm.ensure(self.ctx)
        self.assertEquals(len(blocking), 0, blocking)
        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: f"{component}_deployment"}
        )

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": scheduling_key,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.ANY_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": scheduling_key,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.ANY_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_api_deployment(self):
        await self._helper_applies_scheduling_key_to_api_deployment(
            "api", op_common.SchedulingKey.HEAT_API.value
        )

    async def test_applies_scheduling_key_to_api_cfn_deployment(self):
        await self._helper_applies_scheduling_key_to_api_deployment(
            "api_cfn",
            op_common.SchedulingKey.HEAT_API_CFN.value
        )

    async def test_applies_scheduling_key_to_engine_statefulset(self):
        self._make_all_dependencies_complete_immediately()
        blocking = await self.cr.sm.ensure(self.ctx)
        self.assertEquals(len(blocking), 0, blocking)
        statefulset_interface = interfaces.stateful_set_interface(
            self.api_client
        )
        statefulset, = await statefulset_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: 'engine'}
        )

        self.assertEqual(
            statefulset.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key":
                                    op_common.SchedulingKey.HEAT_ENGINE.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            statefulset.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.HEAT_ENGINE.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)

        self.assertEquals(len(jobs), 1)

        for job in jobs:
            self.assertEqual(
                job.spec.template.spec.affinity.node_affinity.
                required_during_scheduling_ignored_during_execution.to_dict(),
                {
                    "node_selector_terms": [
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_HEAT.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_ANY.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                    ],
                },
            )

            self.assertCountEqual(
                job.spec.template.spec.to_dict()["tolerations"],
                [
                    {
                        "key": op_common.SchedulingKey.OPERATOR_HEAT.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                    {
                        "key": op_common.SchedulingKey.OPERATOR_ANY.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                ],
            )

    # Test correct volumes and volume mounts
    async def _helper_creates_deployment_with_heat_container_volume_mounts(
            self, component):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT:
                            f"{component}_deployment"},
        )

        heat_container_spec = \
            api_deployment.spec.template.spec.containers[0]

        # Assert that the right container was picked by id
        self.assertEqual(heat_container_spec.name,
                         f"heat-{component}".replace("_", "-"))

        # Assert that the container has the correct number of volume mounts
        self.assertEqual(
            len(heat_container_spec.volume_mounts),
            3
        )

        # Build list with names of volumes and volume mounts of the container
        # in the Deployment
        observed_volumes = list(map(lambda v: v.name,
                                    api_deployment.spec.template.spec.volumes))
        observed_volume_mounts = list(map(lambda v: v.name,
                                          heat_container_spec.volume_mounts))

        # Assert container mounts config file correctly
        config_volume_name = "heat-config-volume"
        self.assertIn(config_volume_name, observed_volumes)
        self.assertIn(config_volume_name, observed_volume_mounts)

        observed_config_volume_mount = \
            heat_container_spec.volume_mounts[
                observed_volume_mounts.index(config_volume_name)
            ]
        self.assertEqual(
            observed_config_volume_mount.mount_path,
            os.path.join(CONFIG_PATH, CONFIG_FILE_NAME),
        )
        self.assertEqual(
            observed_config_volume_mount.sub_path,
            CONFIG_FILE_NAME,
        )

        # Assert container mounts policy file correctly
        # Expected to appear right after the config file volume mount
        # (index + 1)
        observed_policy_volume_mount = \
            heat_container_spec.volume_mounts[
                observed_volume_mounts.index(config_volume_name) + 1
                ]
        self.assertEqual(
            observed_policy_volume_mount.mount_path,
            os.path.join(CONFIG_PATH, POLICY_FILE_NAME),
        )
        self.assertEqual(
            observed_policy_volume_mount.sub_path,
            POLICY_FILE_NAME,
        )

        # Assert container mounting certs correctly
        certs_volume_name = "ca-certs"
        self.assertIn(certs_volume_name, observed_volumes)
        self.assertIn(certs_volume_name, observed_volume_mounts)

        observed_certs_volume_mount = \
            heat_container_spec.volume_mounts[
                observed_volume_mounts.index(certs_volume_name)
            ]
        self.assertEqual(
            observed_certs_volume_mount.mount_path,
            "/etc/pki/tls/certs",
        )
        self.assertEqual(
            observed_certs_volume_mount.sub_path,
            None,
        )

    async def test_creates_api_deployment_with_heat_container_volume_mounts(
            self):
        await self._helper_creates_deployment_with_heat_container_volume_mounts(  # noqa: E501
            "api"
        )

    async def test_creates_api_cfn_deployment_with_heat_container_volume_mounts(  # noqa: E501
            self):
        await self._helper_creates_deployment_with_heat_container_volume_mounts(  # noqa: E501
            "api_cfn"
        )

    async def _helper_creates_api_deployment_with_term_containers_volume_mounts(  # noqa: E501
            self, component, container_index, name_suffix=""):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT:
                            f"{component}_deployment"},
        )

        ssl_terminator_container_spec = \
            api_deployment.spec.template.spec.containers[container_index]

        # Assert container has correct number of volume mounts
        self.assertEqual(
            len(ssl_terminator_container_spec.volume_mounts),
            3
        )

        # Build list with names of volumes and volume mounts of the container
        # in the Deployment
        observed_volumes = list(map(lambda v: v.name,
                                    api_deployment.spec.template.spec.volumes))
        observed_volume_mounts = list(map(
            lambda v: v.name, ssl_terminator_container_spec.volume_mounts
        ))

        # Assert container mounting certs correctly
        cert_volume_name = "ca-certs"
        self.assertIn(cert_volume_name, observed_volumes)
        self.assertIn(cert_volume_name, observed_volume_mounts)

        observed_certs_volume_mount = \
            ssl_terminator_container_spec.volume_mounts[
                observed_volume_mounts.index(cert_volume_name)
            ]
        self.assertEqual(
            observed_certs_volume_mount.mount_path,
            "/etc/ssl/certs/ca-certificates.crt",
        )
        self.assertEqual(
            observed_certs_volume_mount.sub_path,
            "ca-bundle.crt",
        )

    async def test_creates_api_deployment_with_term_containers_volume_mounts(self):  # noqa: E501
        await self._helper_creates_api_deployment_with_term_containers_volume_mounts(  # noqa: E501
            "api", 1
        )

    async def test_creates_api_deployment_with_ext_term_containers_volume_mounts(self):  # noqa: E501
        await self._helper_creates_api_deployment_with_term_containers_volume_mounts(  # noqa: E501
            "api", 2, "-external"
        )

    async def test_creates_api_cfn_deployment_with_term_containers_volume_mounts(self):  # noqa: E501
        await self._helper_creates_api_deployment_with_term_containers_volume_mounts(  # noqa: E501
            "api_cfn", 1
        )

    async def test_creates_api_cfn_deployment_with_ext_term_containers_volume_mounts(self):  # noqa: E501
        await self._helper_creates_api_deployment_with_term_containers_volume_mounts(  # noqa: E501
            "api_cfn", 2, "-external"
        )

    async def _helper_creates_deployment_with_reload_containers_volume_mounts(
            self, component, container_index, name_suffix=""):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT:
                            f"{component}_deployment"},
        )

        reload_container_spec = \
            api_deployment.spec.template.spec.containers[container_index]

        # Assert that the right container was picked by id
        self.assertEqual(reload_container_spec.name,
                         f"service-reload{name_suffix}")

        # Assert container has correct number of volume mounts
        self.assertEqual(
            len(reload_container_spec.volume_mounts),
            2
        )

        # Build list with names of volumes and volume mounts of the container
        # in the Deployment
        observed_volumes = list(map(lambda v: v.name,
                                    api_deployment.spec.template.spec.volumes))
        observed_volume_mounts = list(map(lambda v: v.name,
                                          reload_container_spec.volume_mounts))

        # Assert container mounting tls-secret correctly
        tls_volume_name = f"tls-secret{name_suffix}"
        self.assertIn(tls_volume_name, observed_volumes)
        self.assertIn(tls_volume_name, observed_volume_mounts)

        observed_tls_secret_volume_mount = \
            reload_container_spec.volume_mounts[
                observed_volume_mounts.index(tls_volume_name)
            ]
        self.assertEqual(
            observed_tls_secret_volume_mount.mount_path,
            "/data",
        )
        self.assertEqual(
            observed_tls_secret_volume_mount.sub_path,
            None,
        )

    async def test_creates_api_deployment_with_reload_containers_volume_mounts(  # noqa: E501
            self):
        await self._helper_creates_deployment_with_reload_containers_volume_mounts(  # noqa: E501
            "api", 3
        )

    async def test_creates_api_deployment_with_ext_reload_containers_volume_mounts(  # noqa: E501
            self):
        await self._helper_creates_deployment_with_reload_containers_volume_mounts(  # noqa: E501
            "api", 4, "-external"
        )

    async def test_creates_api_cfn_deployment_with_reload_containers_volume_mounts(  # noqa: E501
            self):
        await self._helper_creates_deployment_with_reload_containers_volume_mounts(  # noqa: E501
            "api_cfn", 3
        )

    async def test_creates_api_cfn_deployment_with_ext_reload_containers_volume_mounts(  # noqa: E501
            self):
        await self._helper_creates_deployment_with_reload_containers_volume_mounts(  # noqa: E501
            "api_cfn", 4, "-external"
        )

    async def test_creates_db_sync_job_volume_mounts(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        job_interface = interfaces.job_interface(self.api_client)
        db_sync_job, = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"},
        )

        container_spec = \
            db_sync_job.spec.template.spec.containers[0]

        # Assert that the right container was picked by id
        self.assertEqual(container_spec.name, "heat-setup")

        # Assert container has correct number of volume mounts
        self.assertEqual(
            len(container_spec.volume_mounts),
            2
        )

        # Build list with names of volumes and volume mounts of the
        # container in the Deployment
        observed_volumes = list(map(
            lambda v: v.name, db_sync_job.spec.template.spec.volumes
        ))
        observed_volume_mounts = list(map(
            lambda v: v.name, container_spec.volume_mounts
        ))

        # Assert container mounting config volume correctly
        config_volume_name = "heat-config-volume"
        self.assertIn(config_volume_name, observed_volumes)
        self.assertIn(config_volume_name, observed_volume_mounts)

        observed_config_volume_mount = \
            container_spec.volume_mounts[
                observed_volume_mounts.index(config_volume_name)
            ]
        self.assertEqual(
            observed_config_volume_mount.mount_path,
            os.path.join(CONFIG_PATH, CONFIG_FILE_NAME),
        )
        self.assertEqual(
            observed_config_volume_mount.sub_path,
            CONFIG_FILE_NAME,
        )

        # Assert container mounting certs correctly
        cert_volume_name = "ca-certs"
        self.assertIn(cert_volume_name, observed_volumes)
        self.assertIn(cert_volume_name, observed_volume_mounts)

        observed_certs_volume_mount = \
            container_spec.volume_mounts[
                observed_volume_mounts.index(cert_volume_name)
            ]
        self.assertEqual(
            observed_certs_volume_mount.mount_path,
            "/etc/pki/tls/certs",
        )
        self.assertEqual(
            observed_certs_volume_mount.sub_path,
            None,
        )

    # Test correct network ports
    async def _helper_creates_deployment_with_api_container_port(
            self, component):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT:
                            f"{component}_deployment"},
        )

        container_spec = deployment.spec.template.spec.containers[0]
        self.assertEqual(len(container_spec.env), 2)
        local_port_env_var = container_spec.env[0]
        self.assertEqual(local_port_env_var.name, "LOCAL_PORT")
        self.assertEqual(local_port_env_var.value, "8080")

    async def test_creates_api_deployment_with_api_container_port(self):
        await self._helper_creates_deployment_with_api_container_port("api")

    async def test_creates_api_cfn_deployment_with_api_container_port(self):
        await self._helper_creates_deployment_with_api_container_port(
            "api_cfn"
        )

    async def _helper_creates_api_deployment_with_ssl_term_container_port(
            self, component, container_index, container_name, service_port,
            metrics_port):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT:
                            f"{component}_deployment"},
        )

        ssl_term_container_spec = \
            deployment.spec.template.spec.containers[container_index]

        # Assert that the right container was picked by id
        self.assertEqual(ssl_term_container_spec.name, container_name)

        self.assertEqual(len(ssl_term_container_spec.env), 4)
        service_port_env_var = ssl_term_container_spec.env[0]
        self.assertEqual(service_port_env_var.name, "SERVICE_PORT")
        self.assertEqual(service_port_env_var.value, service_port)
        local_port_env_var = ssl_term_container_spec.env[1]
        self.assertEqual(local_port_env_var.name, "LOCAL_PORT")
        self.assertEqual(local_port_env_var.value, "8080")
        local_port_env_var = ssl_term_container_spec.env[2]
        self.assertEqual(local_port_env_var.name, "METRICS_PORT")
        self.assertEqual(local_port_env_var.value, metrics_port)

    async def test_create_api_deploy_verify_ssl_term_container_port(self):
        await self._helper_creates_api_deployment_with_ssl_term_container_port(
            "api", 1, "ssl-terminator", "8004", "9090"
        )

    async def test_create_api_deploy_verify_ext_ssl_term_container_port(self):
        await self._helper_creates_api_deployment_with_ssl_term_container_port(
            "api", 2, "ssl-terminator-external", "8005", "9091"
        )

    async def test_create_api_cfn_deploy_verify_ssl_term_container_port(self):
        await self._helper_creates_api_deployment_with_ssl_term_container_port(
            "api_cfn", 1, "ssl-terminator", "8000", "9090"
        )

    async def test_create_api_cfn_deploy_verify_ext_ssl_term_container_port(
            self):
        await self._helper_creates_api_deployment_with_ssl_term_container_port(
            "api_cfn", 2, "ssl-terminator-external", "8001", "9091"
        )

    async def test_creates_containers_with_resources(self):

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        statefulsets = interfaces.stateful_set_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        cfn_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_cfn_deployment"},
        )
        engine_sts, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "engine"},
        )

        db_sync_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"}
        )

        self.assertEqual(
            testutils.container_resources(api_deployment, 0),
            testutils.unique_resources("api.heat-api")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 1),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 3),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 4),
            testutils.unique_resources("api.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(cfn_deployment, 0),
            testutils.unique_resources("apiCfn.heat-api-cfn")
        )
        self.assertEqual(
            testutils.container_resources(cfn_deployment, 1),
            testutils.unique_resources("apiCfn.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(cfn_deployment, 2),
            testutils.unique_resources("apiCfn.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(cfn_deployment, 3),
            testutils.unique_resources("apiCfn.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(cfn_deployment, 4),
            testutils.unique_resources("apiCfn.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(engine_sts, 0),
            testutils.unique_resources("engine.heat-engine")
        )
        self.assertEqual(
            testutils.container_resources(db_sync_job, 0),
            testutils.unique_resources("job.heat-db-sync-job")
        )

    # Tests for policy
    async def _helper_creates_config_with_policy_file(self, policy, component):
        heat_deployment_yaml = _get_heat_deployment_yaml(self._keystone_name)
        heat_deployment_yaml["spec"].update(policy)
        self._configure_cr(heat.Heat,
                           heat_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        expected_policy_file = os.path.join(CONFIG_PATH, POLICY_FILE_NAME)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: f"config_{component}"},
        )
        heat_conf_content = testutils._parse_config(
            config_secret.data[CONFIG_FILE_NAME],
            decode=True
        )

        observed_policy_file = \
            heat_conf_content.get("oslo_policy", "policy_file")

        self.assertEqual(observed_policy_file, expected_policy_file)

    @ddt.data({}, HEAT_POLICY)
    async def test_creates_api_config_with_policy_file(self, policy):
        await self._helper_creates_config_with_policy_file(policy, "api")

    @ddt.data({}, HEAT_POLICY)
    async def test_creates_api_cfn_config_with_policy_file(self, policy):
        await self._helper_creates_config_with_policy_file(policy, "api_cfn")

    @ddt.data({}, HEAT_POLICY)
    async def test_creates_engine_config_with_policy_file(self, policy):
        await self._helper_creates_config_with_policy_file(policy, "engine")

    async def _helper_creates_resource_with_projected_volume(
            self, policy, component_resource,
            component_name, expected_num_of_volumes):
        heat_deployment_yaml = _get_heat_deployment_yaml(self._keystone_name)
        heat_deployment_yaml["spec"].update(policy)
        self._configure_cr(heat.Heat,
                           heat_deployment_yaml)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)

        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT:
                            f"config_{component_name}"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "heat_policy"},
        )

        # Assert number of volumes
        self.assertEqual(len(component_resource.spec.template.spec.volumes),
                         expected_num_of_volumes)

        # Assert correct volume
        config_volume_name = "heat-config-volume"
        observed_volumes = list(map(
            lambda v: v.name, component_resource.spec.template.spec.volumes
        ))
        self.assertIn(config_volume_name, observed_volumes)

        # Assert correct container was picked
        container_spec = component_resource.spec.template.spec.containers[0]
        self.assertEqual(container_spec.name,
                         f"heat-{component_name}".replace("_", "-"))

        # Assert correct volume mount
        observed_volume_mounts = list(map(lambda v: v.name,
                                          container_spec.volume_mounts))
        self.assertIn(config_volume_name, observed_volume_mounts)

        observed_projected_volume_mount = container_spec.volume_mounts[
            observed_volume_mounts.index(config_volume_name)
        ]
        self.assertEqual(
            observed_projected_volume_mount.mount_path,
            os.path.join(CONFIG_PATH, CONFIG_FILE_NAME)
        )

        # Assert correct protected volume
        observed_projected_volume = \
            component_resource.spec.template.spec.volumes[
                observed_volumes.index(config_volume_name)
            ]

        observed_projected_volume_sources = \
            observed_projected_volume.projected.sources
        self.assertEqual(len(observed_projected_volume_sources), 2)

        # Assert config secret source in projected volume
        conf_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.secret
        )
        self.assertEqual(
            conf_vol_source.secret.name,
            config_secret.metadata.name,
        )
        self.assertEqual(len(conf_vol_source.secret.items), 1)
        self.assertEqual(
            conf_vol_source.secret.items[0].key,
            CONFIG_FILE_NAME,
        )
        self.assertEqual(
            conf_vol_source.secret.items[0].path,
            CONFIG_FILE_NAME,
        )

        # Assert policy config map source in projected volume
        policy_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.config_map
        )
        self.assertEqual(
            policy_vol_source.config_map.name,
            policy_config_map.metadata.name,
        )
        self.assertEqual(len(policy_vol_source.config_map.items), 1)
        self.assertEqual(
            policy_vol_source.config_map.items[0].key,
            POLICY_FILE_NAME,
        )
        self.assertEqual(
            policy_vol_source.config_map.items[0].path,
            POLICY_FILE_NAME,
        )

    @ddt.data({}, HEAT_POLICY)
    async def test_creates_api_deployment_with_projected_volume(self, policy):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        await self._helper_creates_resource_with_projected_volume(
            policy, api_deployment, "api", 6
        )

    @ddt.data({}, HEAT_POLICY)
    async def test_creates_api_cfn_deployment_with_projected_volume(self,
                                                                    policy):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)

        api_cfn_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_cfn_deployment"},
        )

        await self._helper_creates_resource_with_projected_volume(
            policy, api_cfn_deployment, "api_cfn", 6
        )

    @ddt.data({}, HEAT_POLICY)
    async def test_creates_engine_statefulset_with_projected_volume(self,
                                                                    policy):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = interfaces.stateful_set_interface(self.api_client)

        engine_statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "engine"},
        )

        await self._helper_creates_resource_with_projected_volume(
            policy, engine_statefulset, "engine", 2
        )

    @ddt.data({}, HEAT_POLICY)
    async def test_creates_policy_configmap(self, policy):
        heat_deployment_yaml = _get_heat_deployment_yaml(self._keystone_name)
        heat_deployment_yaml["spec"].update(policy)
        self._configure_cr(heat.Heat,
                           heat_deployment_yaml)

        self._make_all_jobs_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        config_maps = interfaces.config_map_interface(self.api_client)
        heat_policy, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "heat_policy"},
        )

        self.assertEqual(heat_policy.kind, "ConfigMap")
        self.assertTrue(
            heat_policy.metadata.name.startswith("heat-policy"))

        heat_policy_decoded = json.loads(
            heat_policy.data[POLICY_FILE_NAME])

        for rule, value in policy.get("policy", {}).items():
            self.assertEqual(
                heat_policy_decoded[rule],
                value
            )

    async def test_injects_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        for component in ["config_api", "config_api_cfn", "config_engine"]:
            secret, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: component},
            )
            decoded = sm.api_utils.decode_secret_data(secret.data)
            lines = decoded["heat.conf"].splitlines()

            self.assertIn(f"mytestsecret = {CONFIG_SECRET_VALUE}", lines)


@ddt.ddt
class TestHeatDeploymentsWithInternalIngress(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin,
        testutils.MessageQueueTestMixin):

    def _get_heat_deployment_yaml(self, keystone_name):
        return {
            'metadata': {
                'name': NAME,
                'namespace': NAMESPACE
            },
            'spec': {
                'keystoneRef': {
                    'name': keystone_name,
                    "kind": "KeystoneDeployment",
                },
                'database': {
                    'replicas': 3,
                    'storageSize': '8Gi',
                    'proxy': {
                        'replicas': 5,
                        "resources": testutils.generate_db_proxy_resources(),
                    },
                    'backup': {
                        'schedule': '0 * * * *'
                    },
                    "resources": testutils.generate_db_resources(),
                },
                'messageQueue': {
                    'replicas': 1,
                    'storageSize': '5Gi',
                    "resources": testutils.generate_amqp_resources(),
                },
                'api': {
                    "internal": {
                        'ingress': {
                            'fqdn': f"heat-internal.{NAMESPACE}.cloud",
                            'port': 32443,
                            'ingressClassName': 'nginx',
                        },
                    },
                    'replicas': 3,
                    'ingress': {
                        'fqdn': f"heat.{NAMESPACE}.cloud",
                        'port': 32443,
                        'ingressClassName': 'nginx',
                    },
                    "resources": testutils.generate_resources_dict(
                        "api.heat-api",
                        "api.ssl-terminator",
                        "api.ssl-terminator-external",
                        "api.service-reload",
                        "api.service-reload-external",
                    ),
                },
                'apiCfn': {
                    'internal': {
                        'ingress': {
                            'fqdn': f"heat-cfn-internal.{NAMESPACE}.cloud",
                            'port': 32443,
                            'ingressClassName': 'nginx',
                        },
                    },
                    'replicas': 3,
                    'ingress': {
                        'fqdn': f"heat-cfn.{NAMESPACE}.cloud",
                        'port': 32443,
                        'ingressClassName': 'nginx',
                    },
                    "resources": testutils.generate_resources_dict(
                        "apiCfn.heat-api-cfn",
                        "apiCfn.ssl-terminator",
                        "apiCfn.ssl-terminator-external",
                        "apiCfn.service-reload",
                        "apiCfn.service-reload-external",
                    ),
                },
                'engine': {
                    'replicas': 3,
                    "resources": testutils.generate_resources_dict(
                        "engine.heat-engine",
                    ),
                },
                'heatConfig': {
                    'DEFAULT': {
                        'debug': True
                    },
                    'trustee': {
                        'auth-url': 'https://keystone:5000/v3'
                    }
                },
                "heatSecrets": [
                    {
                        "secretName": CONFIG_SECRET_NAME,
                        "items": [{
                            "key": CONFIG_SECRET_KEY,
                            "path": "/DEFAULT/mytestsecret",
                        }],
                    },
                ],
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "2000",
                    "resources": testutils.generate_memcached_resources(),
                },
                "issuerRef": {
                    "name": "issuername"
                },
                'region': {
                    'name': 'MyRegion',
                    'parent': 'ParentRegionName'
                },
                'targetRelease': 'train',
                "jobResources": testutils.generate_resources_dict(
                    "job.heat-db-sync-job",
                ),
            }
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            heat.Heat,
            self._get_heat_deployment_yaml(self._keystone_name),
        )
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        CONFIG_SECRET_KEY: CONFIG_SECRET_VALUE,
                    }),
                },
            )

    async def _helper_creates_endpoint_with_ingress(
            self, component, public_url, admin_url, internal_url):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE,
                                        f"heat-{component}-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["public"],
            public_url,
        )

        self.assertEqual(
            endpoint["spec"]["endpoints"]["admin"],
            admin_url,
        )

        self.assertEqual(
            endpoint["spec"]["endpoints"]["internal"],
            internal_url,
        )

    async def test_creates_endpoint_with_ingress_api(self):
        await self._helper_creates_endpoint_with_ingress(
            "api",
            f"https://heat.{NAMESPACE}.cloud:32443/v1/%(tenant_id)s",
            f"https://heat-api.{NAMESPACE}.svc:8004/v1/%(tenant_id)s",
            f"https://heat-internal.{NAMESPACE}.cloud:32443/v1/%(tenant_id)s",
        )

    async def test_creates_endpoint_with_ingress_api_cfn(self):
        await self._helper_creates_endpoint_with_ingress(
            "api-cfn",
            f"https://heat-cfn.{NAMESPACE}.cloud:32443/v1",
            f"https://heat-api-cfn.{NAMESPACE}.svc:8000/v1",
            f"https://heat-cfn-internal.{NAMESPACE}.cloud:32443/v1",
        )

    async def _helper_creates_ingress(self, name):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, name)

        self.assertEqual(
            ingress.spec.rules[0].host,
            f"{name}.{NAMESPACE}.cloud",
        )

    async def test_creates_ingress_api(self):
        await self._helper_creates_ingress("heat-internal")

    async def test_creates_ingress_api_cfn(self):
        await self._helper_creates_ingress("heat-cfn-internal")

    async def test_ingress_matches_service_api(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "heat")
        internal_ingress = await ingresses.read(NAMESPACE, "heat-internal")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "heat-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].
            backend.service.port.number,
            ([x.port for x in service.spec.ports
                if x.name == "internal-ingress"][0]),
        )

    async def test_ingress_matches_service_api_cfn(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "heat-cfn")
        internal_ingress = await ingresses.read(NAMESPACE, "heat-cfn-internal")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "heat-api-cfn")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].
            backend.service.port.number,
            ([x.port for x in service.spec.ports
                if x.name == "internal-ingress"][0]),
        )
