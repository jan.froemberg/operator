#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import copy
import json
import pprint
import os
import unittest
from unittest.mock import sentinel

import ddt

import yaook.op.common as op_common
import yaook.op.glance as glance
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from tests import testutils


NAMESPACE = "test-namespace"
NAME = "glance"
CONFIG_FILE_NAME = "glance.conf"
CONFIG_PATH = "/etc/glance"
POLICY_FILE_NAME = "policy.json"
POLICY_RULE_KEY = "add_image"
POLICY_RULE_VALUE = "role:admin"
GLANCE_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}


@ddt.ddt()
class TestGlanceDeployments(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin,
        testutils.MemcachedTestMixin):
    def _get_glance_deployment_yaml(self):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneRef": {
                    "name": self._keystone_name,
                    "kind": "KeystoneDeployment",
                },
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "api": {
                    "ingress": {
                        "fqdn": "glance-ingress",
                        "port": 8080,
                        "ingressClassName": "nginx",
                    },
                    "replicas": 2,
                    "resources": testutils.generate_resources_dict(
                        "api.glance-api",
                        "api.ssl-terminator",
                        "api.ssl-terminator-external",
                        "api.service-reload",
                        "api.service-reload-external",
                    ),
                },
                "database": {
                    "replicas": 2,
                    "storageSize": "8Gi",
                    "storageClassName": "foo-class",
                    "proxy": {
                        "replicas": 1,
                        "resources": testutils.generate_db_proxy_resources(),
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                    "resources": testutils.generate_db_resources(),
                },
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "2000",
                    "resources": testutils.generate_memcached_resources(),
                },
                "issuerRef": {
                    "name": "issuername"
                },
                "glanceConfig": {},
                "backends": {
                },
                "targetRelease": "train",
                "jobResources": testutils.generate_resources_dict(
                    "job.glance-db-load-metadefs-job",
                    "job.glance-db-sync-job",
                    "job.glance-db-upgrade-pre-job",
                    "job.glance-db-upgrade-post-job",
                ),
            },
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            glance.Glance,
            self._get_glance_deployment_yaml(),
        )

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(endpoint["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_is_created(self):
        deployment_yaml = self._get_glance_deployment_yaml()
        self._configure_cr(glance.Glance, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 1)

    async def test_keystone_endpoint_is_not_created(self):
        deployment_yaml = self._get_glance_deployment_yaml()
        deployment_yaml["spec"]["api"]["publishEndpoint"] = False
        self._configure_cr(glance.Glance, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 0)

    async def test_keystone_endpoint_matches_region(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["region"]["name"],
                         "regionname")
        self.assertEqual(endpoint["spec"]["region"]["parent"],
                         "parentregionname")

    async def test_created_config_matches_keystone_user(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        user_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: user["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        user_credentials = sm.api_utils.decode_secret_data(
            user_credentials_secret.data
        )
        cfg = testutils._parse_config(config.data["glance.conf"], decode=True)

        self.assertEqual(cfg.get("keystone_authtoken", "username"),
                         user_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("keystone_authtoken", "password"),
                         user_credentials["OS_PASSWORD"])

    async def test_creates_certificate_and_halts(self):
        await self.cr.sm.ensure(self.ctx)
        self.api_client.client_side_validation = False
        cert_interface = interfaces.certificates_interface(self.api_client)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

        await self.cr.sm.ensure(self.ctx)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

    async def test_creates_db_sync_job_and_halts(self):
        self._make_all_databases_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        self._make_all_memcached_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        db_sync, = all_jobs

        self.assertEqual(db_sync.metadata.labels[context.LABEL_COMPONENT],
                         "db_sync")

        await self.cr.sm.ensure(self.ctx)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        db_sync, = all_jobs

        self.assertEqual(db_sync.metadata.labels[context.LABEL_COMPONENT],
                         "db_sync")

    async def test_creates_deployment_with_replica_spec_when_all_jobs_succeed(
            self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        deployment_interface = interfaces.deployment_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        deployment, = await deployment_interface.list_(NAMESPACE)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        self.assertEqual(deployment.spec.replicas, 2)

        self.assertEqual(
            deployment.spec.template.spec.volumes[0].projected.sources[0]
                                                    .secret.name,
            config_secret.metadata.name,
        )

    async def test_creates_deployment_with_max_upload_size(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(NAMESPACE)

        for container in deployment.spec.template.spec.containers:
            if not container.name.startswith("ssl-terminator"):
                continue

            envmap = {
                item.name: item.value
                for item in container.env
            }

            self.assertEqual(
                envmap["MAX_BODY_SIZE_MB"],
                "0"
            )

    async def test_creates_containers_with_correct_resource_constraints(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        db_sync_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"}
        )
        db_load_metadefs_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_load_metadefs"}
        )

        self.assertEqual(
            testutils.container_resources(api_deployment, 0),
            testutils.unique_resources("api.glance-api")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 1),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 3),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 4),
            testutils.unique_resources("api.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(db_sync_job, 0),
            testutils.unique_resources("job.glance-db-sync-job")
        )
        self.assertEqual(
            testutils.container_resources(db_load_metadefs_job, 0),
            testutils.unique_resources("job.glance-db-load-metadefs-job")
        )

    async def test_jobs_use_config_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        jobs = interfaces.job_interface(self.api_client)
        for job in await jobs.list_(NAMESPACE):
            self.assertEqual(
                job.spec.template.spec.volumes[0].secret.secret_name,
                config_secret.metadata.name,
                str(job.metadata),
            )

    async def test_creates_database_and_user(self):
        self._make_all_databases_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(NAMESPACE)
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 2)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 1)
        self.assertEqual(db["spec"]["storageClassName"],
                         "foo-class")
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def test_creates_config_with_database_uri(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )
        db_name = db["spec"]["database"]
        glance_conf = config.data["glance.conf"]
        cfg = testutils._parse_config(glance_conf, decode=True)

        self.assertEqual(
            cfg.get("database", "connection"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_database_frontendIssuer_name(self):
        self._make_all_databases_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        mysqlservices = interfaces.mysqlservice_interface(self.api_client)

        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )

        self.assertEqual(
            "issuername",
            db["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_database_uri_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "glancedeployments",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_deployment",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_configmap_volume(
                api.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "glance-api",
        )

        glance_conf = config.data["glance.conf"]
        cfg = testutils._parse_config(glance_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("database", "connection"),
        )

    async def test_creates_memcached(self):
        await self.cr.sm.ensure(self.ctx)

        memcacheds = interfaces.memcachedservice_interface(self.api_client)
        memcached, = await memcacheds.list_(NAMESPACE)

        self.assertEqual(
            memcached["spec"]["memory"],
            "512"
        )
        self.assertEqual(
            memcached["spec"]["connections"],
            "2000"
        )

    async def test_creates_config_without_ceph(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},)
        keystone_conf = config.data["glance.conf"]
        self.assertNotIn("ceph.conf", config.data)

        cfg = testutils._parse_config(keystone_conf, decode=True)
        self.assertNotIn("glance_store", cfg.keys())

    @ddt.data({}, GLANCE_POLICY)
    async def test_creates_config_with_policy_file(self, policy):
        glance_deployment_yaml = self._get_glance_deployment_yaml()
        glance_deployment_yaml["spec"].update(policy)
        self._configure_cr(glance.Glance, glance_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        expected_policy_file = os.path.join(CONFIG_PATH, POLICY_FILE_NAME)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        glance_conf_content = testutils._parse_config(
            config_secret.data[CONFIG_FILE_NAME],
            decode=True
        )

        observed_policy_file = \
            glance_conf_content.get("oslo_policy", "policy_file")

        self.assertEqual(observed_policy_file, expected_policy_file)

    @ddt.data({}, GLANCE_POLICY)
    async def test_creates_api_deployment_with_projected_volume(self, policy):
        glance_deployment_yaml = self._get_glance_deployment_yaml()
        glance_deployment_yaml["spec"].update(policy)
        self._configure_cr(glance.Glance, glance_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "glance_policy"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 6)
        observed_projected_volume = \
            api_deployment.spec.template.spec.volumes[0]
        observed_projected_volume_mount = \
            api_deployment.spec.template.spec.containers[0].volume_mounts[0]
        self.assertEqual(
            observed_projected_volume_mount.name,
            observed_projected_volume.name,
        )
        self.assertEqual(
            observed_projected_volume_mount.mount_path,
            CONFIG_PATH,
        )

        observed_projected_volume_sources = \
            observed_projected_volume.projected.sources
        self.assertEqual(len(observed_projected_volume_sources), 2)

        conf_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.secret
        )
        self.assertEqual(
            conf_vol_source.secret.name,
            config_secret.metadata.name,
        )
        self.assertEqual(len(conf_vol_source.secret.items), 1)
        self.assertEqual(
            conf_vol_source.secret.items[0].key,
            CONFIG_FILE_NAME,
        )
        self.assertEqual(
            conf_vol_source.secret.items[0].path,
            CONFIG_FILE_NAME,
        )

        policy_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.config_map
        )
        self.assertEqual(
            policy_vol_source.config_map.name,
            policy_config_map.metadata.name,
        )
        self.assertEqual(len(policy_vol_source.config_map.items), 1)
        self.assertEqual(
            policy_vol_source.config_map.items[0].key,
            POLICY_FILE_NAME,
        )
        self.assertEqual(
            policy_vol_source.config_map.items[0].path,
            POLICY_FILE_NAME,
        )

    @ddt.data({}, GLANCE_POLICY)
    async def test_creates_policy_configmap(self, policy):
        glance_deployment_yaml = self._get_glance_deployment_yaml()
        glance_deployment_yaml["spec"].update(policy)
        self._configure_cr(glance.Glance, glance_deployment_yaml)

        self._make_all_jobs_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        config_maps = interfaces.config_map_interface(self.api_client)
        glance_policy, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "glance_policy"},
        )

        self.assertEqual(glance_policy.kind, "ConfigMap")
        self.assertTrue(
            glance_policy.metadata.name.startswith("glance-policy"))

        glance_policy_decoded = json.loads(
            glance_policy.data[POLICY_FILE_NAME])

        for rule, value in policy.get("policy", {}).items():
            self.assertEqual(
                glance_policy_decoded[rule],
                value
            )

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "glance-api")

        self.assertIsNotNone(service)

    async def test_creates_endpoint_with_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "glance-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["public"],
            "https://glance-ingress:8080",
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "glance")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "glance-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "glance")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "glance-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )

    async def test_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_PARENT_PLURAL: "glancedeployments",
            }
        )
        deployment, = await deployments.list_(NAMESPACE)

        pod_labels = deployment.spec.template.metadata.labels
        service_labels = service.spec.selector

        self.assertTrue(
            sm.matches_labels(pod_labels, service_labels),
            f"pods: {pprint.pformat(pod_labels)}\n"
            f"service: {pprint.pformat(service_labels)}\n",
        )

    async def test_applies_scheduling_key_to_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(NAMESPACE)

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.IMAGE_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.ANY_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.IMAGE_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.ANY_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)

        for job in jobs:
            self.assertEqual(
                job.spec.template.spec.affinity.node_affinity.
                required_during_scheduling_ignored_during_execution.to_dict(),
                {
                    "node_selector_terms": [
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_GLANCE.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_ANY.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                    ],
                },
            )

            self.assertCountEqual(
                job.spec.template.spec.to_dict()["tolerations"],
                [
                    {
                        "key": op_common.SchedulingKey.OPERATOR_GLANCE.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                    {
                        "key": op_common.SchedulingKey.OPERATOR_ANY.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                ],
            )

    async def test_api_depends_on_db_syncs_or_upgrade(self):
        self.assertIn(self.cr.db_sync, self.cr.api_deployment._dependencies)
        self.assertIn(self.cr.db_upgrade_pre,
                      self.cr.api_deployment._dependencies)

    async def test_upgrade_post_depends_on_api(self):
        self.assertIn(self.cr.api_deployment,
                      self.cr.db_upgrade_post._dependencies)

    async def test_not_creates_db_upgrade_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_upgrade_pre"},
        )

        self.assertEqual(0, len(jobs))

        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_upgrade_post"},
        )

        self.assertEqual(0, len(jobs))


class TestGlanceDeploymentsUpgrade(
        testutils.ReleaseAwareCustomResourceTestCase):
    def _get_glance_deployment_yaml(self):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneRef": {
                    "name": self._keystone_name,
                    "kind": "KeystoneDeployment",
                },
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "api": {
                    "ingress": {
                        "fqdn": "glance-ingress",
                        "port": 8080,
                        "ingressClassName": "nginx",
                    },
                    "replicas": 2,
                },
                "database": {
                    "replicas": 2,
                    "storageSize": "8Gi",
                    "storageClassName": "foo-class",
                    "proxy": {
                        "replicas": 1,
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                },
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "2000",
                    "resources": testutils.generate_memcached_resources(),
                },
                "issuerRef": {
                    "name": "issuername"
                },
                "glanceConfig": {},
                "backends": {
                },
                "targetRelease": "train",
                "maxUploadSize": 42,
            },
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            glance.Glance,
            self._get_glance_deployment_yaml(),
        )
        self._make_all_databases_ready_immediately()
        hooks = copy.deepcopy(self.client_mock._hooks)
        self._make_all_dependencies_complete_immediately()
        await self.cr.reconcile(self.ctx, 0)
        self.client_mock._hooks = hooks

        kst = self.client_mock.get_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            NAMESPACE, NAME,
        )
        kst["spec"]["targetRelease"] = "ussuri"
        self.client_mock.put_object(
            self.cr.API_GROUP, self.cr.API_GROUP_VERSION, self.cr.PLURAL,
            NAMESPACE, NAME, kst,
        )
        self.ctx = self._make_context()  # needed to set the updated spec

    async def test_imagepullsecret_in_podspec_for_jobs(self):
        # We need to remove the jobs from the initial run
        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)
        for job in jobs:
            await job_interface.delete(job.metadata.namespace,
                                       job.metadata.name)
        return await super().test_imagepullsecret_in_podspec_for_jobs()

    async def test_sets_deployedopenstackversion_on_success(self):
        raise unittest.SkipTest("not relevant")

    async def test_does_upgrade(self):
        self._make_all_dependencies_complete_immediately()
        glance = await self.ctx.parent_intf.read(NAMESPACE, NAME)
        self.assertEqual("train", glance["status"]["installedRelease"])

        await self.cr.reconcile(self.ctx, 0)
        glance = await self.ctx.parent_intf.read(NAMESPACE, NAME)
        self.assertEqual("ussuri", glance["status"]["installedRelease"])

    async def test_creates_db_upgrade_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_upgrade_pre"},
        )

        self.assertEqual(1, len(jobs))

        jobs = await job_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_upgrade_post"},
        )

        self.assertEqual(1, len(jobs))

    async def test_not_creates_db_sync_jobs(self):
        self._make_all_dependencies_complete_immediately()

        # We need to remove the job from the initial queens run
        job_interface = interfaces.job_interface(self.api_client)
        for jobname in ["db_sync", "db_load_metadefs"]:
            await job_interface.delete_collection(
                NAMESPACE,
                label_selector=sm.api_utils.LabelSelector(
                    match_labels={context.LABEL_COMPONENT: jobname},
                ).as_api_selector())

        await self.cr.sm.ensure(self.ctx)

        for jobname in ["db_sync", "db_load_metadefs"]:
            jobs = await job_interface.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: jobname},
            )
            self.assertEqual(0, len(jobs))


class TestGlanceDeploymentsWithSecrets(
        testutils.ReleaseAwareCustomResourceTestCase):

    CONFIG_SECRET_NAME = "config-secret"
    CONFIG_SECRET_KEY = "mysecretkey"
    CONFIG_SECRET_VALUE = "mysecretvalue"

    def _get_glance_deployment_yaml(self):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneRef": {
                    "name": self._keystone_name,
                    "kind": "KeystoneDeployment",
                },
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "issuerRef": {
                    "name": "issuername",
                },
                "api": {
                    "ingress": {
                        "fqdn": "glance-ingress",
                        "port": 8080,
                        "ingressClassName": "nginx",
                    },
                    "replicas": 2,
                },
                "database": {
                    "replicas": 2,
                    "storageSize": "8Gi",
                    "storageClassName": "foo-class",
                    "proxy": {
                        "replicas": 1,
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                },
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "2000",
                    "resources": testutils.generate_memcached_resources(),
                },
                "glanceConfig": {},
                "glanceSecrets": [
                    {
                        "secretName": self.CONFIG_SECRET_NAME,
                        "items": [{
                            "key": self.CONFIG_SECRET_KEY,
                            "path": "/DEFAULT/transport_url",
                        }],
                    },
                ],
                "backends": {
                },
                "targetRelease": "train",
            },
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            glance.Glance,
            self._get_glance_deployment_yaml(),
        )
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, self.CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": self.CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        self.CONFIG_SECRET_KEY: self.CONFIG_SECRET_VALUE,
                    }),
                },
            )

    async def test_injects_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config"},
        )
        decoded = sm.api_utils.decode_secret_data(secret.data)
        lines = decoded["glance.conf"].splitlines()

        self.assertIn(f"transport_url = {self.CONFIG_SECRET_VALUE}", lines)

    async def test_certificate_contains_issuer_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertEqual(
            "issuername",
            certificate["spec"]["issuerRef"]["name"],
        )


class TestGlanceDeploymentsCeph(testutils.ReleaseAwareCustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            glance.Glance,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "region": {
                        "name": "regionname",
                        "parent": "parentregionname",
                    },
                    "issuerRef": {
                        "name": "issuername"
                    },
                    "api": {
                        "ingress": {
                            "fqdn": "glance-ingress",
                            "port": 8080,
                            "ingressClassName": "nginx",
                        },
                        "replicas": 2,
                    },
                    "glanceConfig": {
                        "glance_store": {
                            "default_store": "rbd",
                        }
                    },
                    "database": {
                        "replicas": 2,
                        "storageSize": "8Gi",
                        "proxy": {
                            "replicas": 1,
                        },
                        "backup": {
                            "schedule": "0 * * * *"
                        },
                    },
                    "memcached": {
                        "replicas": 2,
                        "memory": "512",
                        "connections": "2000",
                        "resources": testutils.generate_memcached_resources(),
                    },
                    "backends": {
                        "ceph": {
                            "keyringReference": "glance-client-key",
                            "keyringUsername": "glance",
                            "keyringPoolname": "glance-pool",
                            "cephConfig": {
                                "global": {
                                    "mon_host": "rook-ceph-mon-a:6789"
                                },
                            },
                        },
                    },
                    "targetRelease": "train",
                },
            },
        )

    async def test_creates_config_with_ceph(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},)
        keystone_conf = config.data["glance.conf"]
        ceph_cfg = testutils._parse_config(config.data["ceph.conf"],
                                           decode=True)
        self.assertIn("rook", ceph_cfg.get("global", "mon_host"))

        cfg = testutils._parse_config(keystone_conf, decode=True)
        self.assertIn("rbd", cfg.get("glance_store", "stores"))
        self.assertEqual(
            "glance-pool",
            cfg.get("glance_store", "rbd_store_pool")
        )

    async def test_mon_host_in_ceph_config(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},)
        ceph_cfg = testutils._parse_config(config.data["ceph.conf"],
                                           decode=True)
        self.assertEqual(
            "rook-ceph-mon-a:6789",
            ceph_cfg.get("global", "mon_host")
        )

    async def test_deployment_template(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(NAMESPACE)
        volume_mounts = deployment.spec.template.spec.containers[0].\
            volume_mounts
        mount_paths = [x.mount_path for x in volume_mounts]

        self.assertIn("/etc/ceph/ceph.conf", mount_paths)

    async def test_deployment_template_keyfile(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(NAMESPACE)
        volumes = deployment.spec.template.spec.volumes
        key = [x.secret.secret_name for x in volumes if
               x.name == "glance-ceph-keyfile-volume"]

        self.assertEqual("glance-client-key", key[0])


class TestGlanceDeploymentsFile(testutils.ReleaseAwareCustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            glance.Glance,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "region": {
                        "name": "regionname",
                        "parent": "parentregionname",
                    },
                    "issuerRef": {
                        "name": "issuername"
                    },
                    "api": {
                        "ingress": {
                            "fqdn": "glance-ingress",
                            "port": 8080,
                            "ingressClassName": "nginx",
                        },
                        "replicas": 2,
                    },
                    "glanceConfig": {
                        "glance_store": {
                            "default_store": "rbd",
                        }
                    },
                    "database": {
                        "replicas": 2,
                        "storageSize": "8Gi",
                        "proxy": {
                            "replicas": 1,
                        },
                        "backup": {
                            "schedule": "0 * * * *"
                        },
                    },
                    "memcached": {
                        "replicas": 2,
                        "memory": "512",
                        "connections": "2000",
                        "resources": testutils.generate_memcached_resources(),
                    },
                    "backends": {
                        "file": {
                            "storageClassName": str(sentinel.storageclass),
                            "storageSize": str(sentinel.storageSize),
                        }
                    },
                    "targetRelease": "train",
                },
            },
        )

    async def test_creates_config_with_file(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},)
        keystone_conf = config.data["glance.conf"]

        self.assertNotIn("ceph.conf", config.data)

        cfg = testutils._parse_config(keystone_conf, decode=True)
        self.assertIn("file", cfg.get("glance_store", "stores"))

    async def test_creates_pvc(self):
        self._make_all_databases_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        pvcs = interfaces.persistent_volume_claim_interface(self.api_client)
        image_volume, = await pvcs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "image_volume"},)

        self.assertEqual(["ReadWriteMany"], image_volume.spec.access_modes)
        self.assertEqual(str(sentinel.storageclass),
                         image_volume.spec.storage_class_name)
        self.assertEqual(str(sentinel.storageSize),
                         image_volume.spec.resources.requests["storage"])

    async def test_deployment_template(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(NAMESPACE)
        volume_mounts = deployment.spec.template.spec.containers[0].\
            volume_mounts
        mount_paths = [x.mount_path for x in volume_mounts]

        self.assertIn("/var/lib/glance/images/", mount_paths)


@ddt.ddt()
class TestGlanceDeploymentsWithInternalIngress(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin):
    def _get_glance_deployment_yaml(self):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneRef": {
                    "name": self._keystone_name,
                    "kind": "KeystoneDeployment",
                },
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "api": {
                    "internal": {
                        "ingress": {
                            "fqdn": "internal-glance-ingress",
                            "port": 8081,
                            "ingressClassName": "nginx",
                        },
                    },
                    "ingress": {
                        "fqdn": "glance-ingress",
                        "port": 8080,
                        "ingressClassName": "nginx",
                    },
                    "replicas": 2,
                    "resources": testutils.generate_resources_dict(
                        "api.glance-api",
                        "api.ssl-terminator",
                        "api.ssl-terminator-external",
                        "api.service-reload",
                        "api.service-reload-external",
                    ),
                },
                "database": {
                    "replicas": 2,
                    "storageSize": "8Gi",
                    "storageClassName": "foo-class",
                    "proxy": {
                        "replicas": 1,
                        "resources": testutils.generate_db_proxy_resources(),
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                    "resources": testutils.generate_db_resources(),
                },
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "2000",
                    "resources": testutils.generate_memcached_resources(),
                },
                "issuerRef": {
                    "name": "issuername"
                },
                "glanceConfig": {},
                "backends": {
                },
                "targetRelease": "train",
                "jobResources": testutils.generate_resources_dict(
                    "job.glance-db-load-metadefs-job",
                    "job.glance-db-sync-job",
                    "job.glance-db-upgrade-pre-job",
                    "job.glance-db-upgrade-post-job",
                ),
            },
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            glance.Glance,
            self._get_glance_deployment_yaml(),
        )

    async def test_certificate_contains_internal_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            "internal-glance-ingress",
            certificate["spec"]["dnsNames"],
        )

    async def test_creates_endpoint_with_internal_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "glance-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["internal"],
            "https://internal-glance-ingress:8081",
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "glance-internal")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "internal-glance-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "glance")
        internal_ingress = await ingresses.read(NAMESPACE, "glance-internal")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "glance-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].
            backend.service.port.number,
            ([x.port for x in service.spec.ports
                if x.name == "internal-ingress"][0]),
        )
