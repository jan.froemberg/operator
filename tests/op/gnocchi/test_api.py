#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import os
import pprint
import unittest
from unittest.mock import sentinel

import ddt

import yaook.op.common as op_common
import yaook.op.gnocchi as gnocchi
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from tests import testutils


NAMESPACE = "test-namespace"
NAME = "gnocchi"
CONFIG_FILE_NAME = "gnocchi.conf"
CONFIG_PATH = "/etc/gnocchi"
POLICY_FILE_NAME = "policy.json"
POLICY_RULE_KEY = "get status"
POLICY_RULE_VALUE = "role:admin"
GNOCCHI_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}


def _get_gnocchi_deployment_yaml(keystone_name):
    return {
        "metadata": {
            "name": NAME,
            "namespace": NAMESPACE,
        },
        "spec": {
            "keystoneRef": {
                "name": keystone_name,
                "kind": "KeystoneDeployment",
            },
            "region": {
                "name": "regionname",
                "parent": "parentregionname",
            },
            "issuerRef": {
                "name": "issuername"
            },
            "api": {
                "ingress": {
                    "fqdn": "gnocchi-ingress",
                    "port": 8080,
                    "ingressClassName": "nginx",
                },
                "replicas": 2,
                "resources": testutils.generate_resources_dict(
                    "api.gnocchi-api",
                    "api.ssl-terminator",
                    "api.ssl-terminator-external",
                    "api.service-reload",
                    "api.service-reload-external",
                ),
            },
            "metricd": {
                "replicas": 3,
                "resources": testutils.generate_resources_dict(
                    "metricd.gnocchi-metricd",
                ),
            },
            "database": {
                "replicas": 2,
                "storageSize": "8Gi",
                "proxy": {
                    "replicas": 1,
                    "resources": testutils.generate_db_proxy_resources(),
                },
                "backup": {
                    "schedule": "0 * * * *"
                },
                "resources": testutils.generate_db_resources(),
            },
            "memcached": {
                "replicas": 2,
                "memory": "512",
                "connections": "2000",
                "resources": testutils.generate_memcached_resources(),
            },
            "gnocchiConfig": {},
            "backends": {
            },
            "jobResources": testutils.generate_resources_dict(
                "job.gnocchi-upgrade-job",
            ),
        },
    }


@ddt.ddt
class TestGnocchiDeployments(
        testutils.CustomResourceTestCase,
        testutils.DatabaseTestMixin,
        testutils.MemcachedTestMixin):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            gnocchi.Gnocchi,
            _get_gnocchi_deployment_yaml(self._keystone_name),
        )

    def setUp(self):
        super().setUp()
        self.__patches = [
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="queens")
            ),
        ]
        for p in self.__patches:
            p.start()

    def tearDown(self):
        for p in self.__patches:
            p.stop()
        super().tearDown()

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(endpoint["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_is_created(self):
        deployment_yaml = _get_gnocchi_deployment_yaml(self._keystone_name)
        self._configure_cr(gnocchi.Gnocchi, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 1)

    async def test_keystone_endpoint_is_not_created(self):
        deployment_yaml = _get_gnocchi_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["api"]["publishEndpoint"] = False
        self._configure_cr(gnocchi.Gnocchi, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 0)

    async def test_keystone_endpoint_matches_region(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["region"]["name"],
                         "regionname")
        self.assertEqual(endpoint["spec"]["region"]["parent"],
                         "parentregionname")

    async def test_created_config_matches_keystone_user(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        user_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: user["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        user_credentials = sm.api_utils.decode_secret_data(
            user_credentials_secret.data
        )
        cfg = testutils._parse_config(config.data["gnocchi.conf"], decode=True)

        self.assertEqual(cfg.get("keystone_authtoken", "username"),
                         user_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("keystone_authtoken", "password"),
                         user_credentials["OS_PASSWORD"])

    async def test_creates_api_deployment_with_replicas_when_all_jobs_succeed(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        self.assertEqual(deployment.spec.replicas, 2)

    async def test_creates_database_and_user(self):
        self._make_all_databases_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(NAMESPACE)
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 2)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 1)

    async def test_database_frontendIssuer_name(self):
        self._make_all_databases_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        mysqlservices = interfaces.mysqlservice_interface(self.api_client)

        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )

        self.assertEqual(
            "issuername",
            db["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_creates_config_with_database_uri(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )
        db_name = db["spec"]["database"]
        gnocchi_conf = config.data["gnocchi.conf"]
        cfg = testutils._parse_config(gnocchi_conf, decode=True)

        self.assertEqual(
            cfg.get("indexer", "url"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_database_uri_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "gnocchideployments",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_deployment",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_configmap_volume(
                api.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "gnocchi-api",
        )

        gnocchi_conf = config.data["gnocchi.conf"]
        cfg = testutils._parse_config(gnocchi_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("indexer", "url"),
        )

    async def test_creates_config_without_ceph(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},)
        keystone_conf = config.data["gnocchi.conf"]
        self.assertNotIn("ceph.conf", config.data)

        cfg = testutils._parse_config(keystone_conf, decode=True)
        self.assertNotIn("storage", cfg.keys())

    async def test_creates_memcached(self):
        await self.cr.sm.ensure(self.ctx)

        memcacheds = interfaces.memcachedservice_interface(self.api_client)
        memcached, = await memcacheds.list_(NAMESPACE)

        self.assertEqual(
            memcached["spec"]["memory"],
            "512"
        )
        self.assertEqual(
            memcached["spec"]["connections"],
            "2000"
        )

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "gnocchi-api")

        self.assertIsNotNone(service)

    async def test_creates_endpoint_with_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "gnocchi-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["public"],
            "https://gnocchi-ingress:8080",
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "gnocchi")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "gnocchi-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "gnocchi")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "gnocchi-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )

    async def test_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_PARENT_PLURAL: "gnocchideployments"}
        )
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )

        pod_labels = deployment.spec.template.metadata.labels
        service_labels = service.spec.selector

        self.assertTrue(
            sm.matches_labels(pod_labels, service_labels),
            f"pods: {pprint.pformat(pod_labels)}\n"
            f"service: {pprint.pformat(service_labels)}\n",
        )

    async def test_applies_scheduling_key_to_api_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                GNOCCHI_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                ANY_API.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.GNOCCHI_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": op_common.SchedulingKey.ANY_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_metricd_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "metricd_deployment"}
        )

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": op_common.SchedulingKey.
                                GNOCCHI_METRICD.value,
                                "operator": "Exists",
                                "values": [],
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": op_common.SchedulingKey.GNOCCHI_METRICD.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)

        assert jobs, "No jobs were found"
        for job in jobs:
            self.assertEqual(
                job.spec.template.spec.affinity.node_affinity.
                required_during_scheduling_ignored_during_execution.to_dict(),
                {
                    "node_selector_terms": [
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_GNOCCHI.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        op_common.SchedulingKey.
                                        OPERATOR_ANY.value,
                                    "operator": "Exists",
                                    "values": [],
                                },
                            ],
                            "match_fields": None,
                        },
                    ],
                },
            )

            self.assertCountEqual(
                job.spec.template.spec.to_dict()["tolerations"],
                [
                    {
                        "key": op_common.SchedulingKey.OPERATOR_GNOCCHI.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                    {
                        "key": op_common.SchedulingKey.OPERATOR_ANY.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                ],
            )

    async def test_creates_metricd_deployment_with_replicas(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "metricd_deployment"}
        )
        self.assertEqual(deployment.spec.replicas, 3)

    async def test_creates_certificate_and_halts(self):
        await self.cr.sm.ensure(self.ctx)
        self.api_client.client_side_validation = False
        cert_interface = interfaces.certificates_interface(self.api_client)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

        await self.cr.sm.ensure(self.ctx)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

    async def test_creates_upgrade_job_and_halts(self):
        self._make_all_databases_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        self._make_all_memcached_succeed_immediately()

        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        upgrade, = all_jobs

        self.assertEqual(upgrade.metadata.labels[context.LABEL_COMPONENT],
                         "upgrade")

        await self.cr.sm.ensure(self.ctx)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 1)
        upgrade, = all_jobs

        self.assertEqual(upgrade.metadata.labels[context.LABEL_COMPONENT],
                         "upgrade")

    async def test_certificate_contains_service_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_service",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            f"{service.metadata.name}.{NAMESPACE}.svc",
            certificate["spec"]["dnsNames"],
        )

    async def test_certificate_contains_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            "gnocchi-ingress",
            certificate["spec"]["dnsNames"],
        )

    @ddt.data({}, GNOCCHI_POLICY)
    async def test_creates_config_with_policy_file(self, policy):
        self._keystone_name = self._provide_keystone(NAMESPACE)
        gnocchi_deployment_yaml = \
            _get_gnocchi_deployment_yaml(self._keystone_name)
        gnocchi_deployment_yaml["spec"].update(policy)
        self._configure_cr(gnocchi.Gnocchi, gnocchi_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        expected_policy_file = os.path.join(CONFIG_PATH, POLICY_FILE_NAME)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        gnocchi_conf_content = testutils._parse_config(
            config_secret.data[CONFIG_FILE_NAME],
            decode=True
        )

        observed_policy_file = \
            gnocchi_conf_content.get("oslo_policy", "policy_file")

        self.assertEqual(observed_policy_file, expected_policy_file)

    @ddt.data({}, GNOCCHI_POLICY)
    async def test_creates_api_deployment_with_projected_volume(self, policy):
        self._keystone_name = self._provide_keystone(NAMESPACE)
        gnocchi_deployment_yaml = \
            _get_gnocchi_deployment_yaml(self._keystone_name)
        gnocchi_deployment_yaml["spec"].update(policy)
        self._configure_cr(gnocchi.Gnocchi, gnocchi_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "gnocchi_policy"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 6)
        observed_projected_volume = \
            api_deployment.spec.template.spec.volumes[0]
        observed_projected_volume_mount = \
            api_deployment.spec.template.spec.containers[0].volume_mounts[0]
        self.assertEqual(
            observed_projected_volume_mount.name,
            observed_projected_volume.name,
        )
        self.assertEqual(
            observed_projected_volume_mount.mount_path,
            CONFIG_PATH,
        )

        observed_projected_volume_sources = \
            observed_projected_volume.projected.sources
        self.assertEqual(len(observed_projected_volume_sources), 2)

        conf_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.secret
        )
        self.assertEqual(
            conf_vol_source.secret.name,
            config_secret.metadata.name,
        )
        self.assertEqual(len(conf_vol_source.secret.items), 1)
        self.assertEqual(
            conf_vol_source.secret.items[0].key,
            CONFIG_FILE_NAME,
        )
        self.assertEqual(
            conf_vol_source.secret.items[0].path,
            CONFIG_FILE_NAME,
        )

        policy_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.config_map
        )
        self.assertEqual(
            policy_vol_source.config_map.name,
            policy_config_map.metadata.name,
        )
        self.assertEqual(len(policy_vol_source.config_map.items), 1)
        self.assertEqual(
            policy_vol_source.config_map.items[0].key,
            POLICY_FILE_NAME,
        )
        self.assertEqual(
            policy_vol_source.config_map.items[0].path,
            POLICY_FILE_NAME,
        )

    async def test_creates_containers_with_correct_resource_constraints(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        metricd_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "metricd_deployment"}
        )
        upgrade_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "upgrade"}
        )

        self.assertEqual(
            testutils.container_resources(api_deployment, 0),
            testutils.unique_resources("api.gnocchi-api")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 1),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 3),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 4),
            testutils.unique_resources("api.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(metricd_deployment, 0),
            testutils.unique_resources("metricd.gnocchi-metricd")
        )
        self.assertEqual(
            testutils.container_resources(upgrade_job, 0),
            testutils.unique_resources("job.gnocchi-upgrade-job")
        )

    @ddt.data({}, GNOCCHI_POLICY)
    async def test_creates_policy_configmap(self, policy):
        self._keystone_name = self._provide_keystone(NAMESPACE)
        gnocchi_deployment_yaml = \
            _get_gnocchi_deployment_yaml(self._keystone_name)
        gnocchi_deployment_yaml["spec"].update(policy)
        self._configure_cr(gnocchi.Gnocchi, gnocchi_deployment_yaml)

        self._make_all_jobs_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        config_maps = interfaces.config_map_interface(self.api_client)
        gnocchi_policy, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "gnocchi_policy"},
        )

        self.assertEqual(gnocchi_policy.kind, "ConfigMap")
        self.assertTrue(
            gnocchi_policy.metadata.name.startswith("gnocchi-policy"))

        gnocchi_policy_decoded = json.loads(
            gnocchi_policy.data[POLICY_FILE_NAME])

        for rule, value in policy.get("policy", {}).items():
            self.assertEqual(
                gnocchi_policy_decoded[rule],
                value
            )

    async def test_certificate_contains_issuer_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertEqual(
            "issuername",
            certificate["spec"]["issuerRef"]["name"],
        )


class TestGnocchiDeploymentsCeph(testutils.CustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            gnocchi.Gnocchi,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "region": {
                        "name": "regionname",
                        "parent": "parentregionname",
                    },
                    "issuerRef": {
                        "name": "ca-issuer"
                    },
                    "api": {
                        "ingress": {
                            "fqdn": "gnocchi-ingress",
                            "port": 8080,
                            "ingressClassName": "nginx",
                        },
                        "replicas": 2,
                    },
                    "metricd": {
                        "replicas": 3
                    },
                    "database": {
                        "replicas": 2,
                        "storageSize": "8Gi",
                        "proxy": {
                            "replicas": 1,
                        },
                        "backup": {
                            "schedule": "0 * * * *"
                        },
                    },
                    "memcached": {
                        "replicas": 2,
                        "memory": "512",
                        "connections": "2000",
                        "resources": testutils.generate_memcached_resources(),
                    },
                    "gnocchiConfig": {},
                    "backends": {
                        "ceph": {
                            "keyringReference": "gnocchi-client-key",
                            "keyringUsername": "gnocchi",
                            "cephConfig": {
                                "global": {
                                    "mon_host": "rook-ceph-mon-a:6789"
                                },
                            },
                        }
                    },
                },
            },
        )

    def setUp(self):
        super().setUp()
        self.__patches = [
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="queens")
            ),
        ]
        for p in self.__patches:
            p.start()

    def tearDown(self):
        for p in self.__patches:
            p.stop()
        super().tearDown()

    async def test_creates_config_with_ceph(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},)
        keystone_conf = config.data["gnocchi.conf"]
        ceph_cfg = testutils._parse_config(config.data["ceph.conf"],
                                           decode=True)
        self.assertIn("rook", ceph_cfg.get("global", "mon_host"))

        cfg = testutils._parse_config(keystone_conf, decode=True)
        self.assertEqual("ceph", cfg.get("storage", "driver"))

    async def test_mon_host_in_ceph_config(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},)
        ceph_cfg = testutils._parse_config(config.data["ceph.conf"],
                                           decode=True)
        self.assertEqual(
            "rook-ceph-mon-a:6789",
            ceph_cfg.get("global", "mon_host")
        )


class TestGnocchiDeploymentsS3(testutils.CustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            gnocchi.Gnocchi,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "region": {
                        "name": "regionname",
                        "parent": "parentregionname",
                    },
                    "issuerRef": {
                        "name": "ca-issuer"
                    },
                    "api": {
                        "ingress": {
                            "fqdn": "gnocchi-ingress",
                            "port": 8080,
                            "ingressClassName": "nginx",
                        },
                        "replicas": 2,
                    },
                    "metricd": {
                        "replicas": 3
                    },
                    "database": {
                        "replicas": 2,
                        "storageSize": "8Gi",
                        "proxy": {
                            "replicas": 1,
                        },
                        "backup": {
                            "schedule": "0 * * * *"
                        },
                    },
                    "memcached": {
                        "replicas": 2,
                        "memory": "512",
                        "connections": "2000",
                        "resources": testutils.generate_memcached_resources(),
                    },
                    "gnocchiConfig": {},
                    "backends": {
                        "s3": {
                            "endpoint": str(sentinel.s3_endpoint),
                            "bucketPrefix": str(sentinel.bucket_prefix),
                            "credentialRef": {
                                "name": "gnocchi-s3-password",
                            },
                        },
                    },
                },
            },
        )

        self.access_key = str(sentinel.access_key)
        self.secret_key = str(sentinel.secret_key)
        self.client_mock.put_object(
            "", "v1", "secrets",
            NAMESPACE, "gnocchi-s3-password",
            {
                "apiVersion": "v1",
                "kind": "Secret",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "gnocchi-s3-password",
                },
                "data": sm.api_utils.encode_secret_data({
                    "access": self.access_key,
                    "secret": self.secret_key,
                }),
            },
        )

    def setUp(self):
        super().setUp()
        self.__patches = [
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="queens")
            ),
        ]
        for p in self.__patches:
            p.start()

    def tearDown(self):
        for p in self.__patches:
            p.stop()
        super().tearDown()

    async def test_creates_config_with_s3(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},)

        self.assertNotIn("ceph.conf", config.data)

        gnocchi_conf = config.data["gnocchi.conf"]
        cfg = testutils._parse_config(gnocchi_conf, decode=True)
        self.assertEqual("s3", cfg.get("storage", "driver"))
        self.assertEqual(str(sentinel.s3_endpoint),
                         cfg.get("storage", "s3_endpoint_url"))
        self.assertEqual(str(sentinel.bucket_prefix),
                         cfg.get("storage", "s3_bucket_prefix"))
        self.assertEqual(self.access_key,
                         cfg.get("storage", "s3_access_key_id"))
        self.assertEqual(self.secret_key,
                         cfg.get("storage", "s3_secret_access_key"))


@ddt.ddt
class TestGnocchiDeploymentsWithInternalIngress(
        testutils.CustomResourceTestCase,
        testutils.DatabaseTestMixin):
    def _get_gnocchi_deployment_yaml(self, keystone_name):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneRef": {
                    "name": keystone_name,
                    "kind": "KeystoneDeployment",
                },
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "issuerRef": {
                    "name": "issuername"
                },
                "api": {
                    "internal": {
                        "ingress": {
                            "fqdn": "internal-gnocchi-ingress",
                            "port": 8081,
                            "ingressClassName": "nginx",
                        },
                    },
                    "ingress": {
                        "fqdn": "gnocchi-ingress",
                        "port": 8080,
                        "ingressClassName": "nginx",
                    },
                    "replicas": 2,
                    "resources": testutils.generate_resources_dict(
                        "api.gnocchi-api",
                        "api.ssl-terminator",
                        "api.ssl-terminator-external",
                        "api.ssl-terminator-internal",
                        "api.service-reload",
                        "api.service-reload-external",
                        "api.service-reload-internal",
                    ),
                },
                "metricd": {
                    "replicas": 3,
                    "resources": testutils.generate_resources_dict(
                        "metricd.gnocchi-metricd",
                    ),
                },
                "database": {
                    "replicas": 2,
                    "storageSize": "8Gi",
                    "proxy": {
                        "replicas": 1,
                        "resources": testutils.generate_db_proxy_resources(),
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                    "resources": testutils.generate_db_resources(),
                },
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "2000",
                    "resources": testutils.generate_memcached_resources(),
                },
                "gnocchiConfig": {},
                "backends": {
                },
                "jobResources": testutils.generate_resources_dict(
                    "job.gnocchi-upgrade-job",
                ),
            },
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            gnocchi.Gnocchi,
            self._get_gnocchi_deployment_yaml(self._keystone_name),
        )

    def setUp(self):
        super().setUp()
        self.__patches = [
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="queens")
            ),
        ]
        for p in self.__patches:
            p.start()

    def tearDown(self):
        for p in self.__patches:
            p.stop()
        super().tearDown()

    async def test_creates_containers_with_correct_resource_constraints(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)
        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        metricd_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "metricd_deployment"}
        )
        upgrade_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "upgrade"}
        )

        self.assertEqual(
            testutils.container_resources(api_deployment, 0),
            testutils.unique_resources("api.gnocchi-api")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 1),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 3),
            testutils.unique_resources("api.ssl-terminator-internal")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 4),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 5),
            testutils.unique_resources("api.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 6),
            testutils.unique_resources("api.service-reload-internal")
        )
        self.assertEqual(
            testutils.container_resources(metricd_deployment, 0),
            testutils.unique_resources("metricd.gnocchi-metricd")
        )
        self.assertEqual(
            testutils.container_resources(upgrade_job, 0),
            testutils.unique_resources("job.gnocchi-upgrade-job")
        )

    async def test_certificate_contains_internal_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            "internal-gnocchi-ingress",
            certificate["spec"]["dnsNames"],
        )

    async def test_creates_endpoint_with_internal_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "gnocchi-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["internal"],
            "https://internal-gnocchi-ingress:8081",
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "gnocchi-internal")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "internal-gnocchi-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "gnocchi")
        internal_ingress = await ingresses.read(NAMESPACE, "gnocchi-internal")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "gnocchi-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].
            backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].
            backend.service.port.number,
            ([x.port for x in service.spec.ports
                if x.name == "internal-ingress"][0]),
        )
