#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest
import unittest.mock
import uuid
from unittest.mock import sentinel
from datetime import datetime

import kubernetes_asyncio.client

import yaook.common.config
import yaook.statemachine.context as context
import yaook.statemachine.cue as cue
# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
import yaook.op.neutron_bgp.resources as resources
import yaook.op.neutron_bgp.cr as cr


class TestBGPConfigState(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.password_state = unittest.mock.sentinel.password_state
        self.cs = cr.BGPConfigState(
            metadata=sentinel.metadata_provider,
        )

    def test_is_cue_secret_state(self):
        self.assertIsInstance(self.cs, cue.CueSecret)

    def test_extract_interface_mapping_with_ip(self):
        node_info = unittest.mock.Mock([])
        node_info.metadata = unittest.mock.Mock([])
        node_info.metadata.annotations = {
            context.ANNOTATION_BGP_INTERFACE_IP + 'test': '10.1.4.42/24',
            context.ANNOTATION_BGP_INTERFACE_IP + 'anothertest':
                '10.2.4.42/24',
        }
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "bgpInterfaceMapping": {"bridgeName": "brfoo"},
            "configKey": "test",
        }
        uid1 = str(uuid.uuid4())
        ctx.parent_uid = uid1
        result1 = self.cs._extract_interface_mappings(ctx, node_info)
        ctx.parent_spec = {
            "bgpInterfaceMapping": {"bridgeName": "brbaz"},
            "configKey": "anothertest",
        }
        uid2 = str(uuid.uuid4())
        ctx.parent_uid = uid2
        result2 = self.cs._extract_interface_mappings(ctx, node_info)

        self.assertEqual(
            result1,
            cr.BGPInterfaceMapping(
                bridge_name="brfoo",
                interface_name="bgp-" + uid1[:11],
                interface_ip='10.1.4.42/24',
            )
        )

        self.assertEqual(
            result2,
            cr.BGPInterfaceMapping(
                bridge_name="brbaz",
                interface_name="bgp-" + uid2[:11],
                interface_ip='10.2.4.42/24',
            )
        )

    def test_extract_interface_mappings_raise_if_ip_invalid(self):
        node_info = unittest.mock.Mock([])
        node_info.metadata = unittest.mock.Mock([])
        node_info.metadata.annotations = {
            context.ANNOTATION_BGP_INTERFACE_IP + 'test': '10.1.4.42/64',
            context.ANNOTATION_BGP_INTERFACE_IP + 'anothertest': '10.2.4.500',
            context.ANNOTATION_BGP_INTERFACE_IP + 'failingtest': 'foobar',
        }
        with self.assertRaises(Exception):
            self.cs._extract_interface_mappings(
                [
                    {
                        "bridgeName": "brfoo",
                        "uplinkDevice": "test-bgp",
                    }
                ],
                node_info
            )
        with self.assertRaises(Exception):
            self.cs._extract_interface_mappings(
                [
                    {
                        "bridgeName": "brbar",
                        "uplinkDevice": "anothertest-bgp",
                    },
                ],
                node_info
            )
        with self.assertRaises(Exception):
            self.cs._extract_interface_mappings(
                [
                    {
                        "bridgeName": "brbaz",
                        "uplinkDevice": "failingtest-bgp",
                    }
                ],
                node_info
            )

    async def test_injects_interface_mappings_with_ip_in_rendered_config(self):
        ctx = unittest.mock.Mock(['api_client'])
        uid = str(uuid.uuid4())
        ctx.parent_uid = uid
        ctx.parent_spec = {
            "bgpInterfaceMapping": sentinel.interfacemapping,
            "configKey": "test",
            "hostname": str(uuid.uuid4())
        }

        with contextlib.ExitStack() as stack:
            super_render_config = stack.enter_context(
                unittest.mock.patch.object(
                    cue.CueSecret, "_render_cue_config",
                    new=unittest.mock.AsyncMock()
                )
            )
            super_render_config.return_value = {
                "some": "unrelated",
                "keys": "from",
                "the-actual": "rendering",
            }
            node_info = stack.enter_context(
                unittest.mock.patch.object(
                    kubernetes_asyncio.client.CoreV1Api, "read_node",
                    new=unittest.mock.AsyncMock()
                )
            )

            extract_interface_mappings = stack.enter_context(
                unittest.mock.patch.object(
                    self.cs, "_extract_interface_mappings",
                )
            )
            extract_interface_mappings.return_value = cr.BGPInterfaceMapping(
                bridge_name="brfoo",
                interface_name="bgp-" + uid[:11],
                interface_ip='10.1.4.42/24',
            )

            result = await self.cs._render_cue_config(
                ctx,
            )

        super_render_config.assert_called_once_with(ctx)
        extract_interface_mappings.assert_called_once_with(
            ctx,
            node_info.return_value,
        )

        self.assertEqual(
            result,
            {
                "some": "unrelated",
                "keys": "from",
                "the-actual": "rendering",
                "bgp_interface_mapping": "BRIDGE=brfoo\nBGP_INTERFACE=bgp-" + uid[:11] + "\nBGP_IP=10.1.4.42/24"  # noqa: E501
            }
        )

    async def test__render_cue_config_raise_if_debug_and_json(self):
        ctx = unittest.mock.Mock(['api_client'])
        uid = str(uuid.uuid4())
        ctx.parent_uid = uid
        ctx.parent_spec = {
            "bgpInterfaceMapping": sentinel.interfacemapping,
            "configKey": "test",
            "hostname": str(uuid.uuid4())
        }

        with contextlib.ExitStack() as stack:
            super_render_config = stack.enter_context(
                unittest.mock.patch.object(
                    cue.CueSecret, "_render_cue_config",
                    new=unittest.mock.AsyncMock()
                )
            )
            super_render_config.return_value = {
                "neutron_bgp_dragent.conf": "[DEFAULT]\noptiona = 1\ndebug = true\nsome_other = option\nuse_json = true\n",  # noqa: E501
            }

            with self.assertRaises(Exception):
                await self.cs._render_cue_config(
                    ctx,
                )


# BIG TODO: generalize this whole test class
# we basically copy pasted and ctrl+f and replaced stuff
class TestSpecSequenceLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ssl = cr.BGPSpecSequenceLayer()
        self.__patches = [
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="queens")
            ),
        ]
        for p in self.__patches:
            p.start()

    def tearDown(self):
        for p in self.__patches:
            p.stop()
        super().tearDown()

    async def test_get_layer_extracts_configs_from_spec(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "neutronConfig": [sentinel.cfg1, sentinel.cfg2],
            "neutronBGPDRAgentConfig": [sentinel.cfg2, sentinel.cfg3]
        }
        ctx.parent_name = "node1"
        result = await self.ssl.get_layer(ctx)
        self.assertEqual(
            result,
            {
                "neutron_bgp_dragent": yaook.common.config.OSLO_CONFIG.declare(
                    [sentinel.cfg1, sentinel.cfg2, sentinel.cfg2,
                     sentinel.cfg3] +
                    [{'DEFAULT': {'host': 'node1'}, 'bgp': {
                      'bgp_speaker_driver': "neutron_dynamic_routing."
                                            "services.bgp.agent.driver."
                                            "ryu.driver.RyuBgpDriver"}}],
                ),
            }
        )


class TestBGPStateResource(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.csr = resources.BGPStateResource(
            scheduling_keys=["foo", "bar"],
            endpoint_config=sentinel.endpoint_config,
            credentials_secret=sentinel.credentials_secret,
            job_endpoint_config=sentinel.job_endpoint_config,
            finalizer="test.yaook.cloud",
            eviction_job_template="some-job-template.yaml",
        )

        self.node_name = str(uuid.uuid4()) + '.examplebgplong_name'

        self.core_v1 = unittest.mock.Mock([])
        self.core_v1.read_node = unittest.mock.AsyncMock()
        self.core_v1.read_node.return_value = kubernetes_asyncio.client.V1Node(
            metadata=kubernetes_asyncio.client.V1ObjectMeta(
                name=self.node_name,
            ),
            spec=kubernetes_asyncio.client.V1NodeSpec(
                taints=[
                    kubernetes_asyncio.client.V1Taint(
                        key=unittest.mock.sentinel.foo,
                        effect="NoSchedule",
                    ),
                    kubernetes_asyncio.client.V1Taint(
                        key=unittest.mock.sentinel.bar,
                        effect="NoExecute",
                    )
                ]
            ),
            status=kubernetes_asyncio.client.V1NodeStatus(),
        )
        self.CoreV1Api = unittest.mock.Mock([])
        self.CoreV1Api.return_value = self.core_v1

        self.background_job = unittest.mock.Mock([])
        self.background_job.is_ready = unittest.mock.AsyncMock()
        self.background_job.is_ready.return_value = True

        self.get_node_name = unittest.mock.Mock([])
        self.get_node_name.return_value = self.node_name

        self.__state_patches = [
            unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
                new=self.CoreV1Api,
            ),
            unittest.mock.patch.object(
                self.csr, "_background_job",
                new=self.background_job,
            ),
            unittest.mock.patch.object(
                self.csr, "_get_node_name",
                new=self.get_node_name,
            ),
        ]
        self._stack_ref = contextlib.ExitStack()
        self.stack = self._stack_ref.__enter__()

    def tearDown(self):
        self._stack_ref.__exit__(None, None, None)

    def _setup_state_patches(self):
        for p in self.__state_patches:
            self.stack.enter_context(p)

    def _make_state_context(self):
        ctx = unittest.mock.Mock(["api_client"])
        ctx.parent = {
            "metadata": {
                "name": self.node_name,
                "creationTimestamp": "2021-12-14T11:00:00Z",
            },
            "spec": {
                "state": "Enabled",
            },
            "status": {},
        }
        ctx.parent_name = self.node_name
        ctx.logger = unittest.mock.Mock()
        ctx.creation_timestamp = datetime.strptime(
            ctx.parent["metadata"]["creationTimestamp"],
            "%Y-%m-%dT%H:%M:%SZ")
        return ctx

    @unittest.mock.patch("openstack.connect")
    async def test__get_status_returns_non_if_not_exists(self, connect):
        agents = unittest.mock.Mock()
        network = unittest.mock.Mock(["serivces"])
        network.agents = agents
        connection = unittest.mock.Mock(["network"])
        connection.network = network
        connect.return_value = connection

        agents.return_value = []

        self.assertEqual(
            None,
            self.csr._get_status(self._make_state_context(), {}),
        )

        connect.assert_called_once_with()
        agents.assert_called_once_with(
            binary="neutron-bgp-dragent",
            host=self.node_name)

    @unittest.mock.patch("openstack.connect")
    async def test__get_status_returns_correct_agent(self, connect):
        agents = unittest.mock.Mock()
        network = unittest.mock.Mock(["agents"])
        network.agents = agents
        connection = unittest.mock.Mock(["network"])
        connection.network = network
        connect.return_value = connection

        agent1 = unittest.mock.Mock()
        agent1.binary = "neutron-bgp-dragent"
        agent1.host = self.node_name
        agent1.last_heartbeat_at = "2022-01-03 11:22:33"
        agent1.is_alive = True
        agent1.is_admin_state_up = True
        agent2 = unittest.mock.Mock()
        agent2.binary = "neutron-bgp-dragent"
        agent2.host = "someothernode"
        agent2.last_heartbeat_at = "2022-01-03 11:22:33"
        agent2.is_alive = False
        agent2.is_admin_state_up = False
        agents.return_value = [agent1, agent2]

        self.assertEqual(
            resource.ResourceStatus(
                True, True, None),
            self.csr._get_status(self._make_state_context(), {}),
        )

        connect.assert_called_once_with()
        agents.assert_called_once_with(
            binary="neutron-bgp-dragent",
            host=self.node_name)

    @unittest.mock.patch("openstack.connect")
    async def test__get_status_ignores_agent_if_too_old(self, connect):
        agents = unittest.mock.Mock()
        network = unittest.mock.Mock(["agents"])
        network.agents = agents
        connection = unittest.mock.Mock(["network"])
        connection.network = network
        connect.return_value = connection

        agent1 = unittest.mock.Mock()
        agent1.binary = "neutron-bgp-dragent"
        agent1.host = self.node_name
        agent1.last_heartbeat_at = "2021-01-01 00:00:00"
        agent1.is_alive = True
        agent1.is_admin_state_up = True
        agents.return_value = [agent1]

        self.assertEqual(
            None,
            self.csr._get_status(self._make_state_context(), {}),
        )

        connect.assert_called_once_with()
        agents.assert_called_once_with(
            binary="neutron-bgp-dragent",
            host=self.node_name)

    async def test_delete_removes_annotation_for_bgp(self):
        ctx = self._make_state_context()
        ctx.parent_kind = "NeutronBGPDRAgent"
        ctx.parent_spec = {
            "hostname": "node1",
            "configkey": "agent1",
            "lockName": "bgp-agent1"
        }

        with contextlib.ExitStack() as stack:
            update = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.APIStateResource.update",  # noqa: E501
                new=unittest.mock.AsyncMock([])
            ))
            update.return_value = None
            is_ready = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.openstack.BackgroundJob.is_ready",  # noqa: E501
                new=unittest.mock.AsyncMock([])
            ))
            is_ready.return_value = True
            patch_node = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.patch_node",
                new=unittest.mock.AsyncMock([])
            ))
            await self.csr.delete(ctx, dependencies={})

        patch = {
            "op": "remove",
            # In the openstackresource we are using a jsonpatch and since we
            # don't want this here, '~1' is converted from a '/'
            "path": "/metadata/annotations/l2-lock.maintenance.yaook.cloud~1bgp-" +  # noqa: E501
            ctx.parent_spec["configkey"],
            "value": "",
        }
        patch_node.assert_called_once_with(ctx.parent_spec["hostname"],
                                           [patch])


class TestL2Lock(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.lock = resources.BGPL2Lock()
        self.ctx = unittest.mock.Mock([])
        self.ctx.api_client = unittest.mock.Mock([])
        self.ctx.field_manager = unittest.mock.Mock([])
        self.ctx.parent_kind = "NeutronBGPDRAgent"
        self.ctx.parent_name = "bgp-node1"
        self.ctx.parent_spec = {
            "hostname": "node1",
            "configkey": "agent1",
            "lockName": "bgp-agent1"
        }
        self.node = unittest.mock.Mock(['V1Node'])
        self.node.metadata = unittest.mock.Mock([])
        self.node.metadata.annotations = {}
        self.node.metadata.resource_version = 42

    async def test_reconcile_sets_l2_migration_lock(self):
        with contextlib.ExitStack() as stack:
            read_node = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.read_node",
                new=unittest.mock.AsyncMock([])
            ))
            self.node.metadata.labels = {
                context.LABEL_L2_REQUIRE_MIGRATION: 'False'
            }
            read_node.return_value = self.node
            patch_node = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.patch_node",
                new=unittest.mock.AsyncMock([])
            ))
            await self.lock.reconcile(self.ctx, dependencies={})

        patch = [{
            "op": "add",
            # In the openstackresource we are using a jsonpatch and since we
            # don't want this here, the slash gets converted to a '~1
            "path": "/metadata/annotations/l2-lock.maintenance.yaook.cloud~1bgp-agent1",  # noqa: E501
            "value": "",
        }, {
            'op': 'add', 'path': '/metadata/resourceVersion', 'value':
                self.node.metadata.resource_version
        }]
        patch_node.assert_called_once_with(
            self.ctx.parent_spec["hostname"],
            patch,
            field_manager=self.ctx.field_manager
        )
