#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest
import unittest.mock
import uuid
from unittest.mock import sentinel
from datetime import datetime

import openstack

import yaook.common.config
import yaook.op.neutron_l2.resources as l2_resources
import yaook.op.neutron_l2.cr as cr
import yaook.statemachine as sm
import yaook.statemachine.resources.openstack as resource


class TestL2ConfigState(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.password_state = unittest.mock.sentinel.password_state
        self.cs = cr.L2ConfigState(
            metadata=sentinel.metadata_provider,
        )

    def test_is_cue_secret_state(self):
        self.assertIsInstance(self.cs, sm.CueSecret)

    def test_extract_bridge_mappings(self):
        result1 = self.cs._extract_bridge_mappings(
            [
                {
                    "bridgeName": "brfoo",
                    "uplinkDevice": "eth1",
                }
            ]
        )
        result2 = self.cs._extract_bridge_mappings(
            [
                {
                    "bridgeName": "brfoo",
                    "uplinkDevice": "eth1",
                },
                {
                    "bridgeName": "brbaz",
                    "uplinkDevice": "eth3",
                }
            ]
        )

        self.assertEqual(
            result1,
            [
                cr.BridgeMapping(
                    bridge_name="brfoo",
                    uplink_device="eth1",
                ),
            ]
        )

        self.assertEqual(
            result2,
            [
                cr.BridgeMapping(
                    bridge_name="brfoo",
                    uplink_device="eth1",
                ),
                cr.BridgeMapping(
                    bridge_name="brbaz",
                    uplink_device="eth3",
                ),
            ]
        )

    async def test_injects_bridge_mappings_in_rendered_config(self):
        ctx = unittest.mock.Mock([])
        ctx.parent_spec = {
            "bridgeConfig": unittest.mock.sentinel.bridgeconfig,
        }

        with contextlib.ExitStack() as stack:
            super_render_config = stack.enter_context(
                unittest.mock.patch.object(
                    sm.CueSecret, "_render_cue_config",
                    new=unittest.mock.AsyncMock()
                )
            )
            super_render_config.return_value = {
                "some": "unrelated",
                "keys": "from",
                "the-actual": "rendering",
            }

            extract_bridge_mappings = stack.enter_context(
                unittest.mock.patch.object(
                    self.cs, "_extract_bridge_mappings",
                )
            )
            extract_bridge_mappings.return_value = [
                cr.BridgeMapping(
                    bridge_name="brfoo",
                    uplink_device="eth1",
                ),
                cr.BridgeMapping(
                    bridge_name="brbar",
                    uplink_device="eth2",
                ),
                cr.BridgeMapping(
                    bridge_name="brbaz",
                    uplink_device="eth3",
                ),
            ]

            result = await self.cs._render_cue_config(
                ctx,
            )

        super_render_config.assert_called_once_with(ctx)
        extract_bridge_mappings.assert_called_once_with(
            unittest.mock.sentinel.bridgeconfig,
        )

        self.assertEqual(
            result,
            {
                "some": "unrelated",
                "keys": "from",
                "the-actual": "rendering",
                "bridge_mappings": "brfoo;eth1\nbrbar;eth2\nbrbaz;eth3\n"
            }
        )


class TestSpecSequenceLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ssl = cr.L2SpecSequenceLayer()

    async def test_get_layer_extracts_configs_from_spec(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "neutronConfig": [sentinel.cfg1, sentinel.cfg2],
            "neutronOpenvSwitchAgentConfig": [sentinel.cfg2, sentinel.cfg3],
        }
        result = await self.ssl.get_layer(ctx)
        self.assertEqual(
            result,
            {
                "neutron_openvswitch_agent":
                yaook.common.config.OSLO_CONFIG.declare(
                    [sentinel.cfg1, sentinel.cfg2, sentinel.cfg2,
                     sentinel.cfg3],
                ),
            }
        )


class TestL2StateResource(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.csr = l2_resources.L2StateResource(
            scheduling_keys=["foo", "bar"],
            endpoint_config=sentinel.endpoint_config,
            credentials_secret=sentinel.credentials_secret,
            finalizer="test.yaook.cloud",
        )

        self.node_name = str(uuid.uuid4())

        self.agent = unittest.mock.Mock()
        self.agent.binary = "neutron-openvswitch-agent"
        self.agent.host = self.node_name
        self.agent.is_alive = True
        self.agent.is_admin_state_up = True
        self.agent.id = str(uuid.uuid4())
        self.agent.last_heartbeat_at = "2022-01-03 11:22:33"

        self.mock_get_agent = unittest.mock.Mock()
        self.mock_get_agent.return_value = self.agent

        self.__os_patches = [
            unittest.mock.patch(
                "yaook.op.neutron_l2.resources.L2StateResource._get_agent",
                self.mock_get_agent,
            ),
        ]

        self._stack_ref = contextlib.ExitStack()
        self.stack = self._stack_ref.__enter__()

    def tearDown(self):
        self._stack_ref.__exit__(None, None, None)

    def _setup_os_patches(self):
        for p in self.__os_patches:
            self.stack.enter_context(p)

    def _make_state_context(self):
        ctx = unittest.mock.Mock(["api_client"])
        ctx.parent = {
            "metadata": {
                "name": self.node_name,
                "creationTimestamp": "2021-12-14T11:00:00Z",
            },
            "spec": {
                "state": "Enabled",
            },
            "status": {},
        }
        ctx.parent_name = self.node_name
        ctx.logger = unittest.mock.Mock()
        ctx.namespace = "testnamespace"
        ctx.creation_timestamp = datetime.strptime(
            ctx.parent["metadata"]["creationTimestamp"],
            "%Y-%m-%dT%H:%M:%SZ")
        return ctx

    @unittest.mock.patch("openstack.connect")
    async def test__check_agent_returns_non_if_not_exists(self, connect):
        agents = unittest.mock.Mock()
        network = unittest.mock.Mock(["serivces"])
        network.agents = agents
        connection = unittest.mock.Mock(["network"])
        connection.network = network
        connect.return_value = connection

        agents.return_value = []

        self.assertEqual(
            None,
            self.csr._check_agent(self._make_state_context(),
                                  openstack.connect()),
        )

        connect.assert_called_once_with()
        agents.assert_called_once_with(
            binary="neutron-openvswitch-agent",
            host=self.node_name)

    @unittest.mock.patch("openstack.connect")
    async def test__check_agent_returns_correct_agent(self, connect):
        agents = unittest.mock.Mock()
        network = unittest.mock.Mock(["agents"])
        network.agents = agents
        connection = unittest.mock.Mock(["network"])
        connection.network = network
        connect.return_value = connection

        agent2 = unittest.mock.Mock()
        agent2.binary = "neutron-openvswitch-agent"
        agent2.host = "someothernode"
        agent2.last_heartbeat_at = "2022-01-03 11:22:33"
        agent2.is_alive = False
        agent2.is_admin_state_up = False
        agents.return_value = [self.agent, agent2]

        self.assertEqual(
            self.agent,
            self.csr._check_agent(self._make_state_context(),
                                  openstack.connect()),
        )

        connect.assert_called_once_with()
        agents.assert_called_once_with(
            binary="neutron-openvswitch-agent",
            host=self.node_name)

    @unittest.mock.patch("openstack.connect")
    async def test__check_agent_ignores_agent_if_too_old(self, connect):
        agents = unittest.mock.Mock()
        network = unittest.mock.Mock(["agents"])
        network.agents = agents
        connection = unittest.mock.Mock(["network"])
        connection.network = network
        connect.return_value = connection

        self.agent.last_heartbeat_at = "2021-01-01 00:00:00"
        agents.return_value = [self.agent]

        self.assertEqual(
            None,
            self.csr._check_agent(self._make_state_context(),
                                  openstack.connect()),
        )

        connect.assert_called_once_with()
        agents.assert_called_once_with(
            binary="neutron-openvswitch-agent",
            host=self.node_name)

    @unittest.mock.patch("openstack.connect")
    def test__update_status_with_same_state(self, connect):
        self._setup_os_patches()
        connection = unittest.mock.Mock(["network"])
        connect.return_value = connection

        status = self.csr._update_status(self._make_state_context(), {}, True)
        connection.network.update_agent.assert_not_called()
        self.assertEqual(
            status,
            resource.ResourceStatus(
                up=True,
                enabled=True,
                disable_reason=None,
            )
        )

    @unittest.mock.patch("openstack.connect")
    def test__update_status_changes_state(self, connect):
        self._setup_os_patches()
        self.agent.is_admin_state_up = False
        agent_list = [self.agent]
        agent2 = unittest.mock.Mock()
        agent2.is_admin_state_up = True
        agent2.is_alive = True
        agent_list.append(agent2)
        self.mock_get_agent.side_effect = agent_list
        connection = unittest.mock.Mock(["network"])
        connect.return_value = connection

        status = self.csr._update_status(self._make_state_context(), {}, True)
        connection.network.update_agent.assert_called_once_with(
            agent=self.agent.id,
            admin_state_up=True,
        )
        self.assertEqual(
            status,
            resource.ResourceStatus(
                up=True,
                enabled=True,
                disable_reason=None,
            )
        )

    @unittest.mock.patch("openstack.connect")
    def test__update_status_agent_is_none(self, connect):
        self._setup_os_patches()
        self.mock_get_agent.return_value = None
        connection = unittest.mock.Mock(["network"])
        connect.return_value = connection

        status = self.csr._update_status(self._make_state_context(), {}, True)
        connection.network.update_agent.assert_not_called()
        self.assertIsNone(status)
