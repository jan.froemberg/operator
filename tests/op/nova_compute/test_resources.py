#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest
import unittest.mock
import uuid
from unittest.mock import sentinel
from datetime import datetime

import ddt
import kubernetes_asyncio.client

import yaook.common.config
# TODO(resource-refactor): clean up imports
import yaook.statemachine.resources.openstack as resource
import yaook.statemachine.watcher as watcher
import yaook.op.nova_compute.resources as nc_resources


class TestSpecSequenceLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ssl = nc_resources.SpecSequenceLayer()

    async def test_get_layer_extracts_configs_from_spec(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "novaConfig": [sentinel.cfg1, sentinel.cfg2],
        }
        result = await self.ssl.get_layer(ctx)
        self.assertEqual(
            result,
            {
                "novacompute": yaook.common.config.OSLO_CONFIG.declare(
                    [sentinel.cfg1, sentinel.cfg2],
                ),
            }
        )


class TestCephConfigLayer(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.parent_spec = {"cephBackend": {
            "enabled": True,
            "user": "test-user",
            "uuid": "some-uuid",
            "keyringSecretName": "secret-name",
            "cephConfig": unittest.mock.sentinel.extra_config,
        }}
        self.ctx = unittest.mock.Mock([])
        self.ctx.parent_name = str(uuid.uuid4())
        self.ctx.parent_spec = self.parent_spec
        self.ccl = nc_resources.CephConfigLayer()

    async def test_get_layer_returns_no_config_if_ceph_is_disabled(self):
        self.parent_spec["cephBackend"]["enabled"] = False
        self.assertEqual(
            await self.ccl.get_layer(self.ctx),
            {},
        )

    async def test_get_layer_defaults_extra_config(self):
        del self.parent_spec["cephBackend"]["cephConfig"]
        self.assertEqual(
            await self.ccl.get_layer(self.ctx),
            {
                "ceph": yaook.common.config.CEPH_CONFIG.declare([
                    {},
                    {
                        "client.test-user": {
                            "keyfile": "/etc/ceph/keyfile",
                        },
                    },
                ]),
            },
        )

    async def test__get_cue_inputs_injects_ceph_config(self):
        self.assertEqual(
            await self.ccl.get_layer(self.ctx),
            {
                "ceph": yaook.common.config.CEPH_CONFIG.declare([
                    unittest.mock.sentinel.extra_config,
                    {
                        "client.test-user": {
                            "keyfile": "/etc/ceph/keyfile",
                        },
                    },
                ]),
            },
        )


class TestCephConfigResource(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.parent_spec = {"cephBackend": {
            "enabled": True,
            "user": "test-user",
            "uuid": "some-uuid",
            "keyringSecretName": "secret-name",
            "cephConfig": unittest.mock.sentinel.extra_config,
        }}
        self.ctx = unittest.mock.Mock([])
        self.ctx.parent_name = str(uuid.uuid4())
        self.ctx.parent_spec = self.parent_spec
        self.ccr = nc_resources.CephConfigResource(
            metadata=sentinel.metadata_provider,
        )

    async def test__render_cue_config_leaves_result_unchanged_if_ceph_disabled(self):  # noqa
        self.parent_spec["cephBackend"]["enabled"] = False
        with contextlib.ExitStack() as stack:
            super_render_cue_config = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.CueSecret._render_cue_config",
            ))
            super_render_cue_config.return_value = sentinel.foo

            self.assertEqual(
                await self.ccr._render_cue_config(self.ctx),
                sentinel.foo,
            )

    async def test__render_cue_config_injects_with_ceph(self):
        with contextlib.ExitStack() as stack:
            super_render_cue_config = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.CueSecret._render_cue_config",
            ))
            super_render_cue_config.return_value = {
                sentinel.k: sentinel.v
            }

            result = await self.ccr._render_cue_config(self.ctx)

        self.assertEqual(
            result["idmap"],
            "some-uuid;/etc/ceph/keyfile",
        )

        self.assertEqual(result[sentinel.k], sentinel.v)


@ddt.ddt()
class TestComputeStateResource(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.csr = nc_resources.ComputeStateResource(
            scheduling_keys=["foo", "bar"],
            endpoint_config=sentinel.endpoint_config,
            credentials_secret=sentinel.credentials_secret,
            job_endpoint_config=sentinel.job_endpoint_config,
            finalizer="test.yaook.cloud",
            eviction_job_template="some-job-template.yaml",
        )

        self.node_name = str(uuid.uuid4())

        self.core_v1 = unittest.mock.Mock([])
        self.core_v1.read_node = unittest.mock.AsyncMock()
        self.core_v1.read_node.return_value = kubernetes_asyncio.client.V1Node(
            metadata=kubernetes_asyncio.client.V1ObjectMeta(
                name=self.node_name,
            ),
            spec=kubernetes_asyncio.client.V1NodeSpec(
                taints=[
                    kubernetes_asyncio.client.V1Taint(
                        key=unittest.mock.sentinel.foo,
                        effect="NoSchedule",
                    ),
                    kubernetes_asyncio.client.V1Taint(
                        key=unittest.mock.sentinel.bar,
                        effect="NoExecute",
                    )
                ]
            ),
            status=kubernetes_asyncio.client.V1NodeStatus(),
        )
        self.CoreV1Api = unittest.mock.Mock([])
        self.CoreV1Api.return_value = self.core_v1

        self.background_job = unittest.mock.Mock([])
        self.background_job.is_ready = unittest.mock.AsyncMock()
        self.background_job.is_ready.return_value = True

        self.__state_patches = [
            unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
                new=self.CoreV1Api,
            ),
            unittest.mock.patch.object(
                self.csr, "_background_job",
                new=self.background_job,
            ),
        ]
        self._stack_ref = contextlib.ExitStack()
        self.stack = self._stack_ref.__enter__()

    def tearDown(self):
        self._stack_ref.__exit__(None, None, None)

    def _setup_state_patches(self):
        for p in self.__state_patches:
            self.stack.enter_context(p)

    def _make_state_context(self):
        ctx = unittest.mock.Mock(["api_client"])
        ctx.parent = {
            "metadata": {
                "name": self.node_name,
                "creationTimestamp": "2021-12-14T11:00:00Z",
            },
            "spec": {
                "state": "Enabled",
            },
            "status": {},
        }
        ctx.parent_name = self.node_name
        ctx.logger = unittest.mock.Mock()
        ctx.creation_timestamp = datetime.strptime(
            ctx.parent["metadata"]["creationTimestamp"],
            "%Y-%m-%dT%H:%M:%SZ")
        return ctx

    @unittest.mock.patch("openstack.connect")
    async def test__get_status_returns_non_if_not_exists(self, connect):
        services = unittest.mock.Mock()
        compute = unittest.mock.Mock(["serivces"])
        compute.services = services
        connection = unittest.mock.Mock(["compute"])
        connection.compute = compute
        connect.return_value = connection

        services.return_value = []

        self.assertEqual(
            None,
            self.csr._get_status(self._make_state_context(), {}),
        )

        connect.assert_called_once_with()
        services.assert_called_once_with()

    @unittest.mock.patch("openstack.connect")
    async def test__get_status_returns_correct_service(self, connect):
        services = unittest.mock.Mock()
        compute = unittest.mock.Mock(["serivces"])
        compute.services = services
        connection = unittest.mock.Mock(["compute"])
        connection.compute = compute
        connect.return_value = connection

        service1 = unittest.mock.Mock()
        service1.binary = "nova-compute"
        service1.host = self.node_name
        service1.updated_at = "2021-12-14T11:22:33.123456"
        service1.state = "up"
        service1.status = "enabled"
        service1.disabled_reason = ""
        service2 = unittest.mock.Mock()
        service2.binary = "nova-compute"
        service2.host = "someothernode"
        service2.updated_at = "2021-12-14T11:22:33.123456"
        service2.state = "down"
        service2.status = "disabled"
        service2.disabled_reason = ""
        services.return_value = [service1, service2]

        self.assertEqual(
            resource.ResourceStatus(
                up=True, enabled=True, disable_reason=None),
            self.csr._get_status(self._make_state_context(), {}),
        )

        connect.assert_called_once_with()
        services.assert_called_once_with()

    @unittest.mock.patch("openstack.connect")
    async def test__get_status_ignores_service_if_too_old(self, connect):
        services = unittest.mock.Mock()
        compute = unittest.mock.Mock(["serivces"])
        compute.services = services
        connection = unittest.mock.Mock(["compute"])
        connection.compute = compute
        connect.return_value = connection

        service1 = unittest.mock.Mock()
        service1.binary = "nova-compute"
        service1.host = self.node_name
        service1.updated_at = "2021-01-01T00:00:00.000000"
        service1.state = "up"
        service1.status = "enabled"
        service1.disabled_reason = ""
        services.return_value = [service1]

        self.assertEqual(
            None,
            self.csr._get_status(self._make_state_context(), {}),
        )

        connect.assert_called_once_with()
        services.assert_called_once_with()

    @unittest.mock.patch("openstack.connect")
    async def test__update_status_sets_watcher_info(self, connect):
        self.assertIsNone(
            self.csr.novaComputeWatch.connection_parameters)
        self.assertIsNone(
            self.csr.novaComputeWatch.namespace)

        ctx = unittest.mock.Mock()
        connection_info = {"foo": "bar"}
        self.csr._update_status(ctx, connection_info, True)
        self.assertEqual(
            connection_info,
            self.csr.novaComputeWatch.connection_parameters)
        self.assertEqual(
            ctx.namespace,
            self.csr.novaComputeWatch.namespace)

    @ddt.data(
        watcher.EventType.ADDED,
        watcher.EventType.DELETED,
    )
    def test__handle_event_triggers_on_added_or_deleted(self, event_type):
        event = watcher.StatefulWatchEvent(
            event_type,
            None,
            None,
            None,
            None,
            sentinel.old_object,
        )
        self.assertTrue(self.csr._handle_nova_event(None, event))

    @ddt.data(
        # old_state, old_status, new_state, new_status, expected
        (sentinel.state, sentinel.status, sentinel.state, sentinel.status, False),  # noqa: E501
        (sentinel.old_state, sentinel.status, sentinel.state, sentinel.status, True),  # noqa: E501
        (sentinel.state, sentinel.old_status, sentinel.state, sentinel.status, True),  # noqa: E501
        (sentinel.old_state, sentinel.old_status, sentinel.state, sentinel.status, True),  # noqa: E501
    )
    def test__handle_event_state_or_status(self, data):
        old_state, old_status, new_state, new_status, expected = data
        old_obj = unittest.mock.Mock(["state", "status", "updated_at"])
        old_obj.state = old_state
        old_obj.status = old_status
        old_obj.updated_at = "2021-12-14T11:00:00.000000"
        new_obj = unittest.mock.Mock(["state", "status", "updated_at"])
        new_obj.state = new_state
        new_obj.status = new_status
        new_obj.updated_at = "2021-12-14T11:00:00.000000"
        event = watcher.StatefulWatchEvent(
            watcher.EventType.MODIFIED,
            None,
            None,
            new_obj,
            None,
            old_obj,
        )
        self.assertEqual(
            expected,
            self.csr._handle_nova_event(self._make_state_context(), event)
        )


class TestNovaComputeWatcher(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self.connection_parameters = {
            "key": sentinel.connection_parameters
        }
        self.watcher = nc_resources.NovaComputeWatcher()
        self.watcher.connection_parameters = self.connection_parameters
        self.watcher.namespace = "testnamespace"

    def test_to_kubernetes_reference(self):
        self.assertEqual(
            self.watcher.to_kubernetes_reference("testname"),
            kubernetes_asyncio.client.V1ObjectReference(
                api_version="compute.yaook.cloud/v1",
                kind="NovaComputeNode",
                name="testname",
                namespace="testnamespace"
            )
        )

    @unittest.mock.patch("openstack.connect")
    def test__get_state(self, connect):
        agent1 = unittest.mock.Mock(["host"])
        agent1.host = "host1"
        agent2 = unittest.mock.Mock(["host"])
        agent2.host = "host2"
        client = unittest.mock.Mock(["compute"])
        client.compute = unittest.mock.Mock(["services"])
        client.compute.services.return_value = [
            agent1,
            agent2,
        ]
        connect.return_value = client
        ret = self.watcher._get_state()

        self.assertEqual(
            {"host1": agent1, "host2": agent2},
            ret,
        )
        connect.assert_called_once_with(
            **self.connection_parameters,
        )
        client.compute.services.assert_called_once_with(
            binary="nova-compute"
        )

    @unittest.mock.patch("openstack.connect")
    def test__get_state_does_nothing_if_not_init(self, connect):
        self.watcher.connection_parameters = None

        ret = self.watcher._get_state()

        self.assertEqual(
            {},
            ret,
        )
        connect.assert_not_called()
