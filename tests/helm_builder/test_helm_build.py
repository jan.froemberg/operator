#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import pathlib
import unittest
import unittest.mock
import os
import tempfile
import shutil

from yaook.helm_builder import cli
from yaook.helm_builder import helmutils


class TestHelmChartBuild(unittest.TestCase):
    @unittest.mock.patch.dict(os.environ,
                              {"CI_PROJECT_ID": "147258369"},
                              clear=True)
    def setUp(self) -> None:
        self.chartyaml = 'Chart.yaml'
        self.valuesyaml = 'values.yaml'
        self.templatesdir = 'templates'
        self.version = '0.2.4.5.6-delta'
        self.token = '123456789'
        self.gitlab_project = "147258369"
        self.repourl = "https://gitlab.com/api/v4/projects/" \
                       f"{self.gitlab_project}/packages/" \
                       "helm/api/stable/charts"

        self.tmpdir = tempfile.TemporaryDirectory()
        self.tmpdir.__enter__()
        etcd_backup_dir = pathlib.Path(self.tmpdir.name) / "etcd-backup"
        shutil.copytree(
            (helmutils.get_chart_root() / "etcd-backup"), etcd_backup_dir)

        self.directory_list = [etcd_backup_dir]
        cli.build_charts(
            self.directory_list,
            False,
        )

    def tearDown(self) -> None:
        if self.tmpdir:
            self.tmpdir.__exit__(None, None, None)

    def test_chartyaml_present(self):

        for dir in self.directory_list:
            self.assertTrue(pathlib.Path(dir /
                                         self.chartyaml).resolve().is_file())

    def test_valuesyaml_present(self):

        for dir in self.directory_list:
            self.assertTrue(pathlib.Path(dir /
                                         self.valuesyaml).resolve().is_file())

    def test_templatesdir_present(self):

        for dir in self.directory_list:
            self.assertTrue(pathlib.Path(dir /
                                         self.templatesdir).resolve().is_dir())

    @unittest.mock.patch('requests.post')
    def test_http_body_file_content(self, mock_post):
        mock_post.return_value.status_code = 201
        for dir in self.directory_list:
            chartname = dir.name

            file_info = helmutils.create_helm_archive(dir,
                                                      chartname,
                                                      self.version)

            tarball_path = dir / f"{chartname}-{self.version}.tgz"
            self.assertEqual(file_info[1], tarball_path.name)

            helmutils.upload_helm_chart(file_info[0],
                                        file_info[1],
                                        self.repourl,
                                        self.token)

            self.assertEqual(mock_post.call_args.kwargs['files']['chart'][0],
                             file_info[1])
