import unittest

import yaook.statemachine.resources.k8s_authn as k8s_authn

import kubernetes_asyncio.client as kclient

from .utils import ResourceTestMixin


class TestServiceAccount(unittest.TestCase):
    class ServiceAccountTest(ResourceTestMixin, k8s_authn.ServiceAccount):
        pass

    def test_needs_update_compares_name(self):
        m = unittest.mock.MagicMock()
        sa = self.ServiceAccountTest()

        for r in [True, False]:
            m.metadata.name.__ne__.reset_mock()
            m.metadata.name.__ne__.return_value = r

            result = sa._needs_update(
                m,
                {"metadata": {"name": unittest.mock.sentinel.name}},
            )

            m.metadata.name.__ne__.assert_called_once_with(
                unittest.mock.sentinel.name
            )

            self.assertEqual(result, r)

    def test__needs_update_returns_true_on_label_change(self):
        sa = self.ServiceAccountTest()
        v1 = kclient.V1ServiceAccount(
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )
        v2 = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "bar"
                }
            }
        }
        self.assertTrue(sa._needs_update(v1, v2))
