import unittest
import unittest.mock
from unittest.mock import sentinel

import ddt

import kubernetes_asyncio.client as kclient

import yaook.statemachine.resources.k8s_policy as k8s_policy


@ddt.ddt()
class TestQuorumPodDisruptionBudget(unittest.IsolatedAsyncioTestCase):

    @unittest.mock.patch(
        "yaook.statemachine.resources.Resource._declare_dependencies")
    def test___init__declares_dependency(self, _declare_dependencies):
        k8s_policy.QuorumPodDisruptionBudget(
            None, sentinel.replicated)

        _declare_dependencies.assert_called_with(sentinel.replicated)

    @ddt.data(
        # replicas, quorum value
        (0, 0),
        (1, 0),
        (2, 2),
        (3, 2),
        (4, 3),
        (5, 3),
        (1234567, 617284),
    )
    def test__get_min_available(self, data):
        replicas = {"spec": {"replicas": data[0]}}
        expected = data[1]

        qpdbs = k8s_policy.QuorumPodDisruptionBudget(None, None)

        self.assertEqual(expected, qpdbs._get_min_available(replicas))

    async def test__make_body_deployment(self):
        deployment = kclient.V1Deployment(
            spec=kclient.V1DeploymentSpec(
                replicas=3,
                selector=kclient.V1LabelSelector(),
                template=kclient.V1PodTemplate(
                    metadata=kclient.V1ObjectMeta(
                        labels={
                            "somelabel": "somevalue"
                        }
                    )
                ),
            ),
        )

        ri = unittest.mock.Mock(["read"])
        ri.read = unittest.mock.AsyncMock()
        ri.read.return_value = deployment

        deploymentstate = unittest.mock.Mock(["get", "get_resource_interface"])
        deploymentstate.get = unittest.mock.AsyncMock()
        deploymentstate.get.return_value = kclient.V1ObjectReference(
            namespace="mynamespace", name="deploymentname"
        )
        deploymentstate.get_resource_interface = unittest.mock.Mock()
        deploymentstate.get_resource_interface.return_value = ri

        qpdbs = k8s_policy.QuorumPodDisruptionBudget(
            "myresourcename",
            deploymentstate,
        )

        body = await qpdbs._make_body(None, None)

        expectedBody = {
            "apiVersion": "policy/v1beta1",
            "kind": "PodDisruptionBudget",
            "metadata": {
                "name": "myresourcename"
            },
            "spec": {
                "minAvailable": 2,
                "selector": {
                    "matchLabels": {"somelabel": "somevalue"},
                },
            },
        }

        self.assertEqual(expectedBody, body)
        deploymentstate.get.assert_called_once_with(None)
        ri.read.assert_called_once_with("mynamespace", "deploymentname")

    async def test__make_body_statefulset(self):
        statefulset = kclient.V1StatefulSet(
            spec=kclient.V1StatefulSetSpec(
                replicas=3,
                service_name="someservice",
                selector=kclient.V1LabelSelector(),
                template=kclient.V1PodTemplate(
                    metadata=kclient.V1ObjectMeta(
                        labels={
                            "somelabel": "somevalue"
                        }
                    )
                ),
            ),
        )

        ri = unittest.mock.Mock(["read"])
        ri.read = unittest.mock.AsyncMock()
        ri.read.return_value = statefulset

        statefulsetstate = unittest.mock.Mock([
            "get", "get_resource_interface"])
        statefulsetstate.get = unittest.mock.AsyncMock()
        statefulsetstate.get.return_value = kclient.V1ObjectReference(
            namespace="mynamespace", name="statefulsetname"
        )
        statefulsetstate.get_resource_interface = unittest.mock.Mock()
        statefulsetstate.get_resource_interface.return_value = ri

        qpdbs = k8s_policy.QuorumPodDisruptionBudget(
            "myresourcename",
            statefulsetstate,
        )

        body = await qpdbs._make_body(None, None)

        expectedBody = {
            "apiVersion": "policy/v1beta1",
            "kind": "PodDisruptionBudget",
            "metadata": {
                "name": "myresourcename"
            },
            "spec": {
                "minAvailable": 2,
                "selector": {
                    "matchLabels": {"somelabel": "somevalue"},
                },
            },
        }

        self.assertEqual(expectedBody, body)
        statefulsetstate.get.assert_called_once_with(None)
        ri.read.assert_called_once_with("mynamespace", "statefulsetname")


class TestDisallowPodDisruptionBudget(unittest.IsolatedAsyncioTestCase):

    @unittest.mock.patch(
        "yaook.statemachine.resources.Resource._declare_dependencies")
    def test___init__declares_dependency(self, _declare_dependencies):
        k8s_policy.DisallowedPodDisruptionBudget(
            None, sentinel.replicated)

        _declare_dependencies.assert_called_with(sentinel.replicated)

    async def test__make_body_statefulset(self):
        statefulset = kclient.V1StatefulSet(
            spec=kclient.V1StatefulSetSpec(
                replicas=1,
                service_name="someservice",
                selector=kclient.V1LabelSelector(),
                template=kclient.V1PodTemplate(
                    metadata=kclient.V1ObjectMeta(
                        labels={
                            "somelabel": "somevalue"
                        }
                    )
                ),
            ),
        )

        statefulset_ref = unittest.mock.Mock(["namespace", "name"])
        statefulsetstate = unittest.mock.Mock(
            ["get", "get_resource_interface"])
        statefulsetstate.get = unittest.mock.AsyncMock()
        statefulsetstate.get.return_value = statefulset_ref

        ri = unittest.mock.Mock(["read"])
        ri.read = unittest.mock.AsyncMock()
        ri.read.return_value = statefulset
        statefulsetstate.get_resource_interface = unittest.mock.Mock()
        statefulsetstate.get_resource_interface.return_value = ri

        qpdbs = k8s_policy.DisallowedPodDisruptionBudget(
            "myresourcename",
            statefulsetstate,
        )

        body = await qpdbs._make_body(None, None)

        expectedBody = {
            "apiVersion": "policy/v1beta1",
            "kind": "PodDisruptionBudget",
            "metadata": {
                "name": "myresourcename"
            },
            "spec": {
                "minAvailable": "100%",
                "selector": {
                    "matchLabels": {"somelabel": "somevalue"},
                },
            },
        }

        self.assertEqual(expectedBody, body)

    async def test__make_body_cds(self):
        cds = {
            "spec": {
                "template": {
                    "metadata": {
                        "labels": {
                            "somelabel": "somevalue"
                        }
                    }
                }
            }
        }

        cds_ref = unittest.mock.Mock(["namespace", "name"])
        cdsstate = unittest.mock.Mock(
            ["get", "get_resource_interface"])
        cdsstate.get = unittest.mock.AsyncMock()
        cdsstate.get.return_value = cds_ref

        ri = unittest.mock.Mock(["read"])
        ri.read = unittest.mock.AsyncMock()
        ri.read.return_value = cds
        cdsstate.get_resource_interface = unittest.mock.Mock()
        cdsstate.get_resource_interface.return_value = ri

        qpdbs = k8s_policy.DisallowedPodDisruptionBudget(
            "myresourcename",
            cdsstate,
        )

        body = await qpdbs._make_body(None, None)

        expectedBody = {
            "apiVersion": "policy/v1beta1",
            "kind": "PodDisruptionBudget",
            "metadata": {
                "name": "myresourcename"
            },
            "spec": {
                "minAvailable": "100%",
                "selector": {
                    "matchLabels": {"somelabel": "somevalue"},
                },
            },
        }

        self.assertEqual(expectedBody, body)
